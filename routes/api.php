<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/testRegisterMember' , 'Auth\RegisterController@testRegisterMember'); //大量創建會員
Route::post('/testNotice' , 'testController@testNotice'); //測試大量通知
//-------------------------發表的註冊流程------------------------
Route::post('/testRegister' , 'Auth\RegisterController@testRegister');
//-------------------------------------------------------------
// Route::get('/user/verify' , 'Auth\RegisterController@verifyUser');
Route::get('/user/verify/{token}' , 'Auth\RegisterController@verifyUser');
// Route::get('verify/{token}', function ($token) {
//     return $token;
//         $user = User::where('api_token', $token)->first();
//         if (isset($verifyUser)) {
//             if (!$user) {
//                 $user->send_email_check = 1;
//                 $user->save();
//                 $status = "Your e-mail is verified. You can now login.";
//             } else {
//                 $status = "Your e-mail is already verified. You can now login.";
//             }
//         } else {
//             return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
//         }

//         return redirect('/login')->with('status', $status);
// });

Route::post('/RegisterMember' , 'Auth\RegisterController@RegisterMember');
// Route::get('/testmail' , 'UserController@testmail');
Route::get('/cityArea','publicrequestController@cityArea');//測試
Route::post('/register' , 'Auth\RegisterController@member'); //個人註冊*
Route::post('/login' , 'Auth\LoginController@login');  //登入*
Route::post('/verification' , 'Auth\RegisterController@verification'); //註冊認證碼*
Route::get('/publicrequest','publicrequestController@publicrequest'); //回傳值*
Route::get('/county' , 'publicrequestController@county'); //縣市*
Route::get('/street', 'publicrequestController@street'); //街道*
Route::get('/licenceCategory' , 'publicrequestController@licenceCategory'); //證照類別
Route::get('/licenceName' , 'publicrequestController@licenceName'); //證照名稱
Route::post('/filterCase','publicrequestController@filterCase'); //case進階篩選*
Route::post('/filterPerson' , 'publicrequestController@filterPerson'); //人才進階篩選*
Route::get('/searchcase', 'SearchController@searchcase');//搜尋專案*
Route::get('/searchperson' , 'SearchController@searchperson'); //搜尋人才*
Route::get('/unLoginShowCase' , 'CasesController@unLoginShowCase'); //顯示所有case
Route::get('/unLoginShowPerson' , 'UserController@unLoginShowPerson'); //顯示所有人才
Route::post('/unLoginDetail','publicrequestController@unLoginDetail'); //case詳細資料
Route::get('/hotCase' , 'publicrequestController@hotCase'); //熱門旅團

Route::post('/test' ,'testController@test'); //顯示訊息數量

//----------------------證照----------------------
Route::post('/updateCode' , 'LicenseController@updateCode'); //新增證照項目

Route::group(['middleware' =>['check:api']], function () {
    // Route::post('/test','testController@test'); 
    Route::get('/tenday' , 'MarginController@tenday'); 
    
    Route::post('/ShowOwnCase' , 'publicrequestController@ShowOwnCase'); //發案管理\審核人才\付保證金、媒合完成
    //-------------------------會員--------------------------------------
    Route::post('/profile', 'UserController@setprofile'); //人才資料填寫*
    Route::post('/company', 'UserController@companyprofile'); //公司資料填寫*
    Route::post('/openprofile', 'UserController@openpofile'); //共用資料填寫*
    
    Route::post('/PwdReset' , 'UserController@PwdReset'); //密碼重置
    Route::get('/showmember','UserController@showmember'); //回傳個人資料*
    Route::get('/users', 'UserController@showprofile'); //顯示登入者資料?
    Route::get('/showCompany' , 'UserController@showCompany'); //顯示公司資料

    Route::post('/updateprofile','UserController@updateprofile'); //編輯個人資料*
    Route::post('/updateCompanyProfile' , 'UserController@updateCompanyProfile'); //編輯企業資料
    Route::post('/changepwd','UserController@changepwd'); //修改密碼*
    Route::post('/savePerson' , 'UserController@savePerson'); //收藏人才
    Route::get('/showSavePerson', 'publicrequestController@showSavePerson'); //顯示收藏人才
    Route::get('/detailPerson', 'publicrequestController@detailPerson'); //顯示人才詳細資料
    Route::get('/showperson', 'UserController@showperson'); //顯示所有人才
    Route::post('/addValue' , 'UserController@addValue'); //加值
    Route::get('/addValueRecord' , 'UserController@addValueRecord'); //加值紀錄
    Route::get('/transaction' , 'publicrequestController@transaction');  //顯示交易明細
    Route::get('/applicationRecord' ,'UserController@applicationRecord'); //應徵紀錄
    Route::get('/logout', 'UserController@logout');  //登出*
    Route::post('/authority','UserController@authority'); //權限設定*
    
    Route::get('/countNotice' ,'UserController@countNotice'); //顯示通知數量
    Route::post('/showDetailNotice' , 'UserController@showDetailNotice'); //顯示通知內容
    Route::post('/readNotice' , 'UserController@readNotice'); //已讀
    //---------------------信用卡----------------------------
    Route::get('/showCard' , 'UserController@showCard'); //顯示卡片
    Route::post('/addCard' , 'UserController@addCard'); // 新增信用卡\VISA卡
    Route::post('/removeCard' , 'UserController@removeCard'); //刪除卡片
    //-------------------------履歷-------------------------------------
    Route::post('/resume', 'ResumeController@addResume');  //履歷新增*
    Route::post('/updateresume','ResumeController@updateresume'); //履歷修改*
    Route::post('/deleteresume','ResumeController@deleteresume');  //履歷刪除*
    Route::get('/showresume','ResumeController@showresume'); //回傳履歷*
    Route::post('/PresetResume' , 'ResumeController@PresetResume'); //預設履歷
    // Route::get('/showperson','UserController@showperson'); //顯示人才資料*
    Route::post('/defaultResume' , 'ResumeController@defaultResume'); //預設履歷
    //-------------------------專案----------------------------------------
    Route::get('/showcase', 'CasesController@showcase'); //顯示所有專案
    Route::post('/addcase', 'CasesController@addcase'); //新增專案*
    Route::post('/cacheCase' , 'CasesController@cacheCase'); //暫存專案
    Route::post('/issueCase' , 'CasesController@issueCase'); //發布暫存專案
    Route::post('/savecase','CasesController@savecase'); // 收藏專案*
    Route::post('/updatecase','CasesController@updatecase'); //編輯專案
    Route::post('/detail','publicrequestController@detail'); //case詳細資料
    Route::get('/showSaveCase' , 'publicrequestController@showSaveCase'); //顯示已收藏case
    Route::post('/editPosition' , 'CasesController@editPosition'); //編輯職位需求
    Route::post('/addPosition' , 'CasesController@addPosition'); //增加職位需求
    Route::post('/removePosition' , 'CasesController@removePosition'); //刪除職位需求
    Route::post('/copyCase' , 'CasesController@copyCase'); //複製case
    //-----------------------------發案管理-------------------------------
    Route::get('/pushcase', 'CasesController@pushcase'); // 發案管理\已發佈\進行中(顯示case)
    Route::post('/showComleteCase' , 'CasesController@showComleteCase'); // 發案管理\已發佈\完成
    Route::post('/showCaseTaker' , 'CasesController@showCaseTaker'); // 發案管理\已發佈\進行中\來應試的人
    route::post('/showCaseInvite' , 'CasesController@showCaseInvite'); // 發案管理\已發佈\進行中\已邀請的人
    Route::post('/showCaseRefuse' , 'CasesController@showCaseRefuse'); //發案管理\已發佈\進行中\已拒絕的人
    Route::post('/showAllStatus' , 'CasesController@showAllStatus'); //發案管理\已發佈\進行中\所有狀態
    Route::post('/showRefuse' , 'CasesController@showRefuse'); //發案管理\已發佈\進行中\已拒絕
    Route::get('/showDefault' , 'CasesController@showDefault'); //發案管理\已發佈\人才概況\顯示違約細項
    Route::post('/checkPerson' , 'publicrequestController@checkPerson'); // 發案管理\審核人才\待審核
    Route::post('/showCacheCase' , 'publicrequestController@showCacheCase'); //發案管理\未發佈
    Route::post('/cancelTaker' , 'CasesController@cancelTaker'); //發案管理\審核人才\等待中 取消按鈕
    // Route::get('/showCancelReason' , 'publicrequestController@showCancelReason'); //顯示取消接案原因
    Route::post('/alternate' , 'CasesController@alternate'); //發案管理\已發佈\備取人才
    Route::post('/complete' , 'CasesController@complete'); //結案
    Route::post('/removeCase' , 'CasesController@removeCase'); //刪除旅團
    Route::post('/ShowHistoryRecordTaker' , 'CasesController@ShowHistoryRecordTaker'); //歷史應徵
    // Route::post('/showCheckInvite' , 'publicrequestController@showCheckInvite'); //發案管理\確認邀請
    //-----------------------------接案管理-------------------------------
    Route::post('/showBeInvite' , 'publicrequestController@showBeInvite'); //接案管理\等待中\被邀請
    Route::post('/ShowTakeCase' , 'publicrequestController@ShowTakeCase'); //接案管理\顯示應徵的case
    Route::get('/showHistoryRecordCarryOut' , 'publicrequestController@showHistoryRecordCarryOut'); //接案管裡\歷史紀錄\進行中
    Route::get('/showHistoryRecordComplete' , 'publicrequestController@showHistoryRecordComplete');//接案管裡\歷史紀錄\完成
    Route::get('/getCase' , 'publicrequestController@getCase');
    //-------------------------接案-----------------------------------
    Route::post('/undertake', 'TakeCaseController@undertake'); //接案*
    Route::post('/cancelTake' , 'TakeCaseController@cancelTake'); //取消投履歷
    Route::post('/doubletake' , 'TakeCaseController@doubletake'); //重複接案
    Route::post('/accept','TakeCaseController@accept');  //接受*
    Route::post('/promisemoney' , 'TakeCaseController@promisemoney'); //付保證金
    Route::post('/refusePromisemoney' , 'TakeCaseController@refusePromisemoney'); //接案方拒絕付保證金
    
    //--------------------邀請--------------------------------
    Route::post('/inviteUser' , 'InviteUserController@inviteUser'); //邀請案子
    Route::post('/acceptInvite' , 'InviteUserController@acceptInvite'); //接受邀請
    Route::post('/cancelAccept' , 'InviteUserController@cancelAccept'); //取消接受邀請
    Route::post('/doubleInvite' , 'InviteUserController@doubleInvite'); //重複邀影
    Route::post('/checkInvite' , 'InviteUserController@checkInvite'); //確認邀請
    Route::post('/cancelInvite' , 'InviteUserController@cancelInvite'); //取消邀請
    // Route::post('/InvitePromisemoney' , 'InviteUserController@InvitePromisemoney'); //接案方付保證金
    //-----------------------訊息----------------------------
    Route::post('/msg','MsgController@msg'); //傳訊息*
    Route::post('/msgPerson' , 'MsgController@msgPerson'); //傳訊息
    Route::post('/showMsg','MsgController@showMsg'); //顯示訊息
    Route::post('/showDetailMsg' , 'MsgController@showDetailMsg'); //顯示訊息內容
    Route::get('/countMsg' , 'MsgController@countMsg'); //顯示訊息數量
    //------------------個人行事曆----------------------
    Route::post('/addSchedule', 'CalendarController@addSchedule'); //新增行事曆
    Route::get('/showcalendar' , 'CalendarController@showcalendar'); //顯示行事曆
    Route::post('/editSchedule' , 'CalendarController@editSchedule'); //編輯行事曆
    Route::post('/removeSchedule' , 'CalendarController@removeSchedule'); //刪除行事曆
    //---------------保證金--------------
    Route::post('/caseDefault' , 'MarginController@caseDefault'); //發案方違約
    route::post('/takerDefault' , 'MarginController@takerDefault'); //接案方違約

    Route::post('/takerObjection' , 'MarginController@takerObjection'); //接案方提出異議
    Route::post('/caseAcceptDefault' , 'MarginController@caseAcceptDefault');//發案方接受or拒絕接案方違約
    Route::post('/caseObjection' , 'MarginController@caseObjection'); //發案方有異議
    Route::post('/takerAcceptDefault' , 'MarginController@takerAcceptDefault'); //接案方接受or拒絕發案方違約
    Route::post('/CancelDefault' , 'MarginController@CancelDefault'); //取消提出異議

    Route::get('/ShowCancelReason' , 'MarginController@ShowCancelReason'); //顯示接案方違約原因
    //------------------------圖片----------------------------
    Route::get('/showImg','ImgController@showImg'); //顯示圖片
    Route::post('/uploadImg' , 'ImgController@uploadImg'); //上傳圖片
    Route::post('/delectImg' , 'ImgController@delectImg'); //刪除圖片
    //-----------------------推薦--------------------------
    Route::post('/recommendCase' , 'AlgorithmController@recommendCase'); //推薦case
    Route::get('/recommendPerson' , 'AlgorithmController@recommendPerson'); //推薦人才
    Route::post('/detailRecommendPerson' , 'AlgorithmController@detailRecommendPerson'); //推薦人才詳細
});

