<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class TheCases extends Model
{
    protected $table = 'cases';

    public $timestamps = false;

    protected $fillable = [
        'uid', 'belong_member' ,'casename','casecontent' ,'attraction','startdate' , 'enddate' ,
        'period' , 'quantity' , 'position' , 'num' , 'language_ability' ,
        'releasedate' , 'attention' , 'connect' , 'phone' , 'public' , 'status' , 'createtime' , 'updatetime' ,
        'promisemoney' , 'isopen' , 'salary' , 'peoplenum' ,
    ];

    protected $hidden = [
        'createtime', 'updatetime' ,
    ];

    public function User()
    {
        // return $this->belongsTo('App\User' , 'case_uid' , 'user_uid');
        return $this->belongsToMany('App\User' ,'user_take_case' ,'case_uid' , 'user_uid');
    }

    public function CreateMember()
    {
        return $this->belongsTo('App\member' , 'cmember' , 'user_uid','uid','uid');
    }

    public function licenses()
    {
        return $this->belongsToMany('App\License');
    }

    public function undertakeUsers() //承接者
    {
        return $this->belongsToMany('App\User' , 'user_take_case' ,'uid');
    }

    public function inviteUsers() //邀請者
    {
        return $this->belongsToMany('App\User' ,'case_invite_users' , 'uid' );
    }

    public function  CaseSaver() //儲存專案者
    {
        return $this->belongsToMany('App\User' , 'user_save_case' , 'case_uid' , 'user_uid' , 'uid' , 'uid');
    }

    // public function SaveCase()
    // {
    //     return $this->belongsToMany('App\UserSaveCase','' , 'uid' , 'case_uid' , 'uid' , 'uid');
    // }

    public function CasePosition()
    {
        return $this->hasMany('App\CasePosition' , 'case_uid' , 'uid' , 'uid' , 'uid');
    }
    
    // public function CaseLicense()
    // {
    //     return $this->
    // }

    public function TakeCase()
    {
        return $this->hasMany('App\UserTakeCase' , 'case_uid' , 'uid' , 'uid' , 'uid');
    }

    public function InviteUser()
    {
        return $this->hasMany('App\CaseInviteUser' , 'uid' , 'case_uid' , 'uid' , 'uid');
    }
    // public function recommendCase()
    // {
    //     return $this->hasMany('App\member' , 'uid' , 'uid' , 'uid' , 'uid');
    // }

    
}
