<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSaveCase extends Model
{
    protected $table = 'user_save_case';

    protected $fillable = [
        'uid' , 'user_uid' ,'case_uid' ,'belong_member', 'createtime' , 'updatetime' , 'umember' , 'cmember'
    ];

    protected $hidden = [
        'createtime', 'updatetime' 
    ];

    // public function CaseSaver()
    // {
    //     return $this->belongsToMany('App\TheCases','' , 'case_uid' , 'uid' , 'uid' , 'uid');
    // }
}
