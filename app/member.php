<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member extends Model
{
    protected $table = 'members';

    protected $fillable = [
        'uid', 'name', 'gender', 'birth', 'email', 'education', 'skill', 'profile', 'phone', 'mobilephone', 'address',
        'beforejob' , 'interest' , 'codenum',
    ];
    public $timestamps = false;

    public function company()
    {
        return $this->belongsTo(company::class);
    }

    public function LanguageAbility()
    {
        return $this->hasMany('App\UserLanguageAbility' , 'user_uid' , 'user_uid' , 'uid' , 'uid');
    }

    public function TheCases()
    {
        return $this->hasMany('App\TheCases' , 'user_uid' , 'uid','user_uid','user_uid');
    }
    
}
