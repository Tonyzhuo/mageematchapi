<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseSaveUser extends Model
{
    protected $table = 'case_save_user';

    protected $fillable = [
        'uid' , 'case_user_uid' ,'user_uid' ,'belong_member', 'createtime' , 'updatetime' , 'umember' , 'cmember'
    ];

    protected $hidden = [
        'createtime', 'updatetime' 
    ];
}
