<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//php artisan make:model workItem

class workItem extends Model
{
    protected $table = 'work_items';

    public $timestamps = false;

    protected $fillable = [
        'name', 'description', 'work_item_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'work_item_id'
    ];

    public function workItems(){
        return $this->has(workItem::class);
    }

    public function user(){
        return $this->belongsToMany(User::class,'user_work_item');
    }

    

    
}
