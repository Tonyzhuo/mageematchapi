<?php

namespace App;

use Carbon\Carbon;
use DB;

use Illuminate\Database\Eloquent\Model;

class setUID extends Model
{
    //
    private function setUid($table){
        $mytime = Carbon::now('Asia/Taipei');
        $DB = DB::table($table)->where("createtime", "<", $mytime->tomorrow())->where("createtime", ">=", $mytime->format("Y-m-d"));
        $UIDformat = ["", "ymd", "0000"];
        $UIDformat[1] = strtolower($UIDformat[1]);
        $uid = $UIDformat[0] . $mytime->format($UIDformat[1]) . (sprintf("%0" . strlen($UIDformat[2]) . "d", $DB->count() + 1) . "0");
        $count = DB::table($table)->where(["uid" => $uid])->count();
        if ($count <= 0) {
            switch($table){
                case 'main': return 'TM'.$uid; break;
                case 'daydata': return 'D'.$uid; break;
                case 'schedule': return 'S'.$uid; break;
                case 'member': return 'U'.$uid; break;
                case 'trapoint': return 'TP'.$uid; break;
                default: return $uid;
            }
        }
    }
}
