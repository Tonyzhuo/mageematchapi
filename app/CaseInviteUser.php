<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseInviteUser extends Model
{
    public $table = 'case_invite_users';

    public $timestamps = false;

    protected $fillable = [
        'uid' , 'user_uid' , 'case_uid' , 'belong_member' , 'status' , 'createtime' , 'updatetime' , 'refuse'
    ];

    public function TheCase()
    {
        return $htis->belongsTo('App\TheCases' , 'case_uid' , 'uid' , 'uid' , 'uid');
    }
}
