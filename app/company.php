<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    //
    protected $table = 'companys';

    protected $fillable=[
        'uid', 'belong_member' ,'name' , 'taxid' , 'businessphone' , 'contactphone' , 'address' , 'profile' , 'createtime' , 'updatetime'
    ];

    public function member()
    {
        return $this->hasMany(member::class);
    }
    public $timestamps = false;
}
