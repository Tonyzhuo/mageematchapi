<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class verification extends Model
{
    protected $table = 'verification';

    public $timestamps = false;

    protected $fillable=[
        'umember' , 'code' , 'count' ,
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
