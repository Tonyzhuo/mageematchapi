<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'name', 'email', 'password',
        'uid','username', 'email', 'password', 'api_token', 'sort' ,'register_token' , 'sort' , 'codenum' , 'send_email_check'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //'password',
    protected $hidden = [
         'api_token' , 'remember_token',
    ];

    public $timestamps = false;

    // public $timestamps = false;
    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function verification()
    {
        return $this->hasMany('App\verification');
    }

    public function company()
    {
        return $this->hasone('App\company');
    }

    public function member()
    {
        return $this->hasone('App\member');
    }

    public function licenses()
    {
        return $this->belongsToMany('App\License' , 'user_license')
                    ->withPivot(['status' , 'photo_front' , 'photo_backend' , 'description']);
    }

    // public function TheCases()
    // {
    //     return $this->hasMany('App\TheCases' ,'uid' , 'uid');
    // }

    public function authenticatedlicenses() //證照認證
    {
        return $this->belongsToMany('App\License' , 'user_license')
                    ->withPivot(['status' , 'photo_front' , 'photo_backend' , 'description'])
                    ->withPivot('status' ,  'pass');
    }

    public function undertakeCases()
    {
        // return $this->hasMany("App\UserTakeCase","user_uid","case_uid");
        return $this->belongsToMany('App\TheCases' ,'user_take_case' , 'user_uid' , 'case_uid',"uid","uid");
    }

    public function invitedCases()
    {
        return $this->belongsToMany('App\TheCases' , 'case_invite_users' , 'uid' , 'uid')
                    ->withPivot(['status' , 'refuse']);
    }

    public function UserSaveCase() //儲存專案
    {
        return $this->belongsToMany('App\TheCases' , 'user_save_case' , 'user_uid' , 'case_uid' ,'uid' , 'uid');
    }

    public function SaveCase()
    {
        return $this->hasOne('App\TheCases' , 'uid' , 'uid');
    }

    public function CaseSaveUser()
    {
        return $this->belongsToMany('App\TheCases' , 'case_save_user' , 'case_user_uid' , 'user_uid' , 'uid' , 'uid');
    }



    // public function workItems()
    // {
    //     return $this->belongsToMany(workItems::class);
    // }
    
    public function undertake(Thecase $case)
    {
        $this->undertakeCases()->attack($case->uid);
        
    }
    
    public function accept(Thecase $case , User $user)
    {
        $user->undertakeCases()->updateExistingPivot($case->uid , [
            'status' => 'wait_confirm3', 
        ]);
    }

    public function confirm(Thecase $case)
    {
        $user->undertakeCases()->updateExistingPivot($case->uid , [
            'status' => 'complete' 
        ]);
        $case->status = 'undertake' ;
        $case->save();
    }

    // public function invite(Thecase $case , User $user)
    // {

    // }
    
}
