<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLanguageAbility extends Model
{
    protected $table = 'user_language_ability';

    protected $fillable = [
        'id', 'uid', 'user_uid', 'language', 'ability', 'belong_member', 'umember', 'cmember', 'createtime', 'updatetime',
    ];
    public $timestamps = false;

    public function member()
    {
        return $this->belongsTo('App\member' , 'uid');
    }
    
}
