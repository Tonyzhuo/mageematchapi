<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTakeCase extends Model
{
    public $table = 'user_take_case';

    public $timestamps = false;

    protected $fillable = [
        'uid' , 'user_uid' , 'case_uid' , 'belong_member' , 'status' , 'createtime' , 'updatetime' , 'refuse'
    ];

    public function TheCase()
    {
        return $this->belongsTo('App\TheCases' , 'case_uid' , 'uid' , 'uid' , 'uid');
    }

    
}
