<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $table = 'license';

    protected $fillable=[
        'uid' , 'license_id' , 'name' , 'description'
    ];

    protected $hidden = [
        'createtime', 'updatetime', 'license_id'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsToMany('App\User' , 'user_license')
                    ->withPivot('status' , 'photo_front' , 'photo_backend' , 'description')
                    ->withTimestamps();
    }
}
