<?php
namespace App\Http\Controllers\PublicCtr;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;
use Auth;
use DB;

trait Support{
    private function SupportInsert($table , $data){
        DB::beginTransaction();
        $data=collect($data);
        $user = $this->getUser(); //取得登入者資料
        $nowtime = Carbon::now('Asia/Taipei'); //取得現在時間
        $uid = $this->setUid($table); //設定uid
        $data->forget(["uid","createtime","updatetime","nydel","cmember","umember"]);
        $data['createtime'] = $nowtime; //傳入資料補值creatime
        $data['uid'] = $uid; //傳入資料補值uid
        if(isset($user->uid)){
            $data['belong_member'] = $user->uid;
            $data['cmember'] = $user->uid; //傳入資料補值cmember
        }
        $doInsert = DB::table($table)->insert($data->all()); //新增資料
        // DB::commit();
        $result = DB::table($table)->where('uid', $uid)->get();
        return ($doInsert) ? ['table' => $table, 'state' => $doInsert, 'result' => $result] : ['table' => $table, 'state' => $doInsert]; //回傳新增結果
    }

    private function SupportUpdate($table , $data , $where){
        DB::beginTransaction();
        $Updata=collect(); //初始化比對資料
        $TheData = DB::table($table)->where($where)->first(); //取得欲修改資料
		if ($TheData) {
			$nowtime = Carbon::now("Asia/Taipei"); //取得現在時間
			$user = $this->getUser();  //取得登入者資料
            $newData = collect($data); //傳入的資料
            $oldData = collect($TheData)->all();  //資料庫的資料
            $newData->each(function ($item, $key) use ($oldData, $Updata){ //做迴圈比對
                if($item != $oldData[$key]){ //與舊資料不一樣
                    $Updata->put($key,$item); //將比對不同的資料丟入Updata
                }
            });
            if (count($Updata->all()) > 0){ //如果有不同資料做修改
                $Updata->put('updatetime', $nowtime); //設定修改時間
                if($table != 'member'){
                    $Updata->put('umember', $user->uid); //設定修改人員
                }
                DB::table($table)->where($where)->update($Updata->all()); //修改
            }
        }
        // DB::commit();
        return (count($Updata->all()) > 0) ? ['talbe' => $table, 'state' => true, 'result' => $Updata] : ['table' => $table, 'state' => false]; //回傳修改結果
    }
    
    private function SupportIsset($data , $index){
        return (isset($data[$index])) ? $data[$index]:"";
    }

    private function getUser(){
        $user = (Auth::guard('api')->user()) ? (Auth::guard('api')->user()) : "";
        return $user;
    }

    private function setUid($table){
        $mytime = Carbon::now('Asia/Taipei');
        $DB = DB::table($table)->where("createtime", "<", $mytime->tomorrow())->where("createtime", ">=", $mytime->format("Y-m-d"));
        $UIDformat = ["", "ymd", "0000"];
        $UIDformat[1] = strtolower($UIDformat[1]);
        $char = chr(rand(65, 90)) . rand(0, 9);
        $uid = $UIDformat[0] . $mytime->format($UIDformat[1]) . (sprintf("%0" . strlen($UIDformat[2]) . "d", $DB->count()) . "0" . $char);
        // $uid = $UIDformat[0] . $mytime->format($UIDformat[1]) . (sprintf("%0" . strlen($UIDformat[2]) . "d", $DB->count() + 1) . "0");
        $count = DB::table($table)->where(["uid" => $uid])->count();
        if ($count <= 0) {
            switch($table){
                case 'members': return 'ME'.$uid; break;
                case 'users': return 'U'.$uid; break;
                case 'companys': return 'C'.$uid; break;
                case 'work_items': return 'W'.$uid; break;
                case 'cases': return 'CA'.$uid; break;
                case 'resume': return 'R'.$uid; break;
                case 'user_save_case': return 'SA'.$uid; break;
                case 'user_take_case': return 'TA'.$uid; break;
                case 'calendar': return 'CAL'.$uid; break;
                case 'notice': return 'NO'.$uid; break;
                case 'case_position': return 'CP'.$uid; break;
                case 'message': return 'MS'.$uid; break;
                case 'apply_history': return 'AH'.$uid; break;
                case 'user_language_ability': return 'ULA'.$uid; break;
                case 'case_save_user': return 'CAU'.$uid; break;
                case 'calendar': return 'CD'.$uid; break;
                case 'margin': return 'M'.$uid; break;
                case 'level': return 'L'.$uid; break;
                case 'case_invite_users': return 'CI'.$uid; break;
                case 'user_card': return 'UC'.$uid; break;
                case 'img':return 'IMG'.$uid; break;
                case 'licence_code':return 'LC'.$uid; break;
                case 'credit_card_code':return 'CC'.$uid; break;
                case 'cancel_reason':return 'CR'.$uid; break;
                case 'user_license':return 'UL'.$uid; break;
                default: return $uid;
            }
        }
    }

}




?>