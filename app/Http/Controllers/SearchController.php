<?php

namespace App\Http\Controllers;

use App\TheCases;
use App\UserSaveCase;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchcase(Request $request) //搜尋專案

    {
        $page = 0;
        $pagenum = 20;
        $data = $request['name'];
        $user = Auth::guard('api')->user();
        // $case = DB::table('cases')
        //             ->where(function($query) use($data){
        //                 $query->when($data ,function($query) use($data){
        //                     $query->where('casename' , 'like' , '%'.$data.'%')
        //                     ->orwhere('category' , 'like' , '%'.$data.'%')
        //                     ->orwhere('casecontent' , 'like' , '%'.$data.'%')
        //                     ->orwhere('attraction' , 'like' , '%'.$data.'%')
        //                     ->orwhere('location' , 'like' , '%'.$data.'%')
        //                     ->orwhere('connect' , 'like' , '%'.$data.'%')
        //                     ->orwhere('attention' , 'like' , '%'.$data.'%');
        //                 });
        //             })
        //             ->where('isIssue' , 1)
        //             ->where('isopen' , 1)
        //             ->where('status', 'wait')
        //             ->orderBy('createtime', 'desc')->where('isopen', 1);   whereExists
        // $case = TheCases::with(['CreateMember' => function ($query)use($data){
        //     $query->when($data , function($query)use($data){
        //         $query->where('username' ,'like' ,'%'.$data.'%');
        //     });
        // }])->get();
        $case = TheCases::whereExists(function ($query) use ($data) {
            $query->when($data, function ($query) use ($data) {
                $query->select(DB::Raw("1"))->from('members')
                    ->where('username', 'like', '%' . $data . '%')
                    ->whereColumn('cases.cmember', '=', 'members.user_uid');
            });
        })
            ->with(['CreateMember'])
            ->orwhere(function ($query) use ($data) {
                $query->where('casename', 'like', '%' . $data . '%')
                    ->orwhere('category', 'like', '%' . $data . '%')
                    ->orwhere('casecontent', 'like', '%' . $data . '%')
                    ->orwhere('attraction', 'like', '%' . $data . '%')
                    ->orwhere('location', 'like', '%' . $data . '%')
                    ->orwhere('connect', 'like', '%' . $data . '%')
                    ->orwhere('attention', 'like', '%' . $data . '%');
            })
            ->where('isIssue', 1)
            ->where('isopen', 1)
            ->where('status', 'wait')
            ->orderBy('createtime', 'desc');
        // return $case;

        $countcase = clone $case;
        $case = $case->skip($page * $pagenum)->take($pagenum)->get();
        $countcase = $countcase->count();

        $today = Carbon::today();

        $case = $case->map(function ($item, $keys) use ($user, $today) {
            $item->uid;
            $item->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $item->uid, 'isopen' => 1])->count() > 0);
            //>0顯示天數 否則顯示true(過期)
            $item->isOverdue = ((floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->get();
            $item->category = explode(',', $item->category);
            $item->position = $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                return $item;
            });
            return $item;
        });

        if (!$case->isEmpty()) {
            return [
                'table' => 'cases',
                'state' => 'true',
                'count' => $countcase,
                'result' => $case,
            ];
        } else {
            return [
                'table' => 'cases',
                'state' => 'false',
                'result' => 'nothing',
            ];
        }
    }

    public function searchperson(Request $request) //搜尋人才

    {
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $user = Auth::guard('api')->user();
        $persondata = $request['name'];
        $person = DB::table('members')
            ->where(function ($query) use ($persondata) {
                $query->when($persondata, function ($query) use ($persondata) {
                    $query->where('username', 'like', '%' . $persondata . '%');
                });
            });
        // ->get();
        $page = 0;
        $pagenum = 20;
        $countperson = clone $person;
        $person = $person->skip($page * $pagenum)->take($pagenum)->get();
        $countperson = $countperson->count();
        if ($user != null) {
            $person = $person->map(function ($item, $keys) use($user , $avatar){
                $item->isSave = (DB::table('case_save_user')->where(['user_uid' => $item->user_uid, 'case_user_uid' => $user->uid, 'isopen' => 1])->count() > 0);
                $item->PresetResume = DB::table('resume')->where('cmember', $item->user_uid)->where('isPreset', 1)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first()->path : $avatar;
                return $item;
            });
        } else {
            $person = $person->map(function ($item, $keys) use($avatar){
                $item->PresetResume = DB::table('resume')->where('cmember', $item->user_uid)->where('isPreset', 1)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first()->path : $avatar;
                return $item;
            });
        }

        if (!$person->isEmpty()) {
            return [
                'table' => 'members',
                'state' => 'true',
                'count' => $countperson,
                'result' => $person,
            ];
        } else {
            return [
                'table' => 'cases',
                'state' => 'false',
                'result' => 'nothing',
            ];
        }
    }
}
