<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PublicCtr;
use App\User;
use App\UserTakeCase;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class TakeCaseController extends Controller
{
    use PublicCtr\Support;
    public function undertake(Request $request) //接案

    {
        // return $request->all();
        $takecasedata = $request['data']; //case_uid\resume_uid\position\message
        $user = Auth::guard('api')->user();
        $caseuid = $takecasedata['case_uid'];

        $casedata = DB::table('cases')->where('uid', $takecasedata['case_uid'])->where('isopen', 1)->where('isIssue', 1)->first();
        // dd($casedata);
        $resumeuid = DB::table('resume')->where('uid', $takecasedata['resume_uid'])->where('isopen', 1)->first();
        $name = DB::table('members')->where('user_uid', $user->uid)->first()->username;
        $doublePost = DB::table('user_take_case')->where('user_uid', $user->uid)->where('case_uid', $takecasedata['case_uid'])->where('resume_uid', $takecasedata['resume_uid'])->where('position_uid', $takecasedata['position_uid'])->where('isopen', 1)->where('status', 'accept')->where('func', 'UserTake')->get();
        $defferentPosition = DB::table('user_take_case')->where('user_uid', $user->uid)->where('case_uid', $takecasedata['case_uid'])->where('position_uid', '<>', $takecasedata['position_uid'])->where('isopen', 1)->whereIn('status', ['accept', 'money'])->where('func', 'UserTake')->get();
        $resume = DB::table('resume')->where('uid', $takecasedata['resume_uid'])->where('isopen', 1)->first();
        // return $defferentPosition;
        if (!$doublePost->isEmpty()) {
            return [
                'msg' => 'double post',
            ];
        }
        //搜尋有無與已付保證金之案子重疊
        $TakeCaseCalendar = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
            $query->where(function ($query) use ($casedata) {
                $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
                ->orWhere(function ($query) use ($casedata) {
                    $query->where('startdate', '<', $casedata->startdate)
                        ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                })
                ->orwhere(function ($query) use ($casedata) {
                    $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                        ->where('enddate', '>', $casedata->enddate);
                })
                ->orwhere(function ($query) use ($casedata) {
                    $query->where('startdate', '<', $casedata->startdate)
                        ->where('enddate', '>', $casedata->enddate);
                });
        }])
            ->whereIn('isopen', [1])
            ->whereIn('status', ['confrim'])
            ->whereIn('user_uid', [$user->uid])
            ->get();
        // return $TakeCaseCalendar;
        // $isTrueTake = $TakeCaseCalendar->filter(function ($item) {return $item->TheCase;})->count() > 0;
        // return [$isTrueTake];

        $PersonCalendar = DB::table('calendar')
            ->where('isopen', 1)
            ->where('user_uid', $user->uid)
            ->where(function ($query) use ($casedata) {
                // dd($casedata);
                $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
            ->orWhere(function ($query) use ($casedata) {
                $query->where('startdate', '<', $casedata->startdate)
                    ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
            ->orwhere(function ($query) use ($casedata) {
                $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->where('enddate', '>', $casedata->enddate);
            })
            ->orwhere(function ($query) use ($casedata) {
                $query->where('startdate', '<', $casedata->startdate)
                    ->where('enddate', '>', $casedata->enddate);
            })
            ->get()
            ->isEmpty(); //空的回傳true

        if ($TakeCaseCalendar->isEmpty()) {
            // $userinfo = $user->load(["undertakeCases" => function ($query) use ($caseuid) {
            //     return $query->where("case_uid", $caseuid)
            //     ->whereIn('status' , ['money','confirm']);
            // }]);

            $takecase = DB::table('user_take_case')->where('user_uid', $user->uid)->where('case_uid', $takecasedata['case_uid'])->where('position_uid', $takecasedata['position_uid'])->whereIn('status', ['money', 'confirm', 'complete'])->get();
            // return $takecase;
            if ($takecase->isEmpty()) {
                $tcase = $this->SupportInsert('user_take_case', [
                    'user_uid' => $user->uid,
                    'case_uid' => $takecasedata['case_uid'],
                    'case_user_uid' => $casedata->cmember,
                    'resume_uid' => $takecasedata['resume_uid'],
                    'position_uid' => $takecasedata['position_uid'],
                    'status' => 'accept',
                    'func' => 'UserTake',
                ]);
                // return $tcase['result'][0]->uid;
                $notice = $this->SupportInsert('notice', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $tcase['result'][0]->uid, //改通知內容
                    'from_user_uid' => $user->uid,
                    'to_user_uid' => $casedata->belong_member,
                    'content' => 'send a resume',
                ]);

                if ($takecasedata['message'] != null) {
                    $message = $this->SupportInsert('message', [
                        'from_user_uid' => $user->uid,
                        'to_user_uid' => $casedata->belong_member,
                        'content' => $takecasedata['message'],
                    ]);
                } else {
                    $message = '';
                }

                if (!$PersonCalendar) {
                    return [
                        $tcase,
                        'resume' => $resume,
                        $message,
                        'msg' => 'Is double on PersonCalendar',
                    ];
                } else {
                    return [
                        $tcase,
                        'resume' => $resume,
                        $message,
                    ];
                }

            } else {
                return [
                    'table' => 'user_take_case',
                    'state' => 'null',
                    'result' => 'The case is already taked', //此案已經接過
                ];
            }
        } else {
            return [
                'table' => 'calendar',
                'state' => 'false',
                'result' => 'The case is double', //與別的case重疊
                // 'aa' => 'aa'
            ];
        }

    }

    public function cancelTake(Request $request) //取消投履歷

    {
        //case_uid , resume_uid , position ,
        $data = $request['take_uid'];
        $takeCaseData = DB::table('user_take_case')->where('uid', $data)->first();
        $user = Auth::guard('api')->user();
        $CaseCmember = DB::table('cases')->where('uid', $takeCaseData->case_uid)->first();
        $cancelTake = $this->SupportUpdate('user_take_case', [
            'status' => 'cancel',
        ], ['uid' => $data]);
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'cases',
            'belong_uid' => $takeCaseData->case_uid,
            'from_user_uid' => $user->uid,
            'to_user_uid' => $CaseCmember->cmember,
            'content' => 'cancel take',
        ]);
        return [
            'cancel' => $cancelTake,
            'notice' => $notice,
        ];
    }

    public function doubletake(Request $request) //重複接案

    {
        $takecasedata = $request['data']; //case_uid\resume_uid\position\message
        $user = Auth::guard('api')->user();
        $casedata = DB::table('cases')->where('uid', $takecasedata['case_uid'])->first();

        $history = $this->SupportInsert('apply_history', [
            'user_uid' => $user->uid,
            'case_uid' => $takecasedata['case_uid'],
            'status' => 'send resume to case',
        ]);
        $case = $this->SupportInsert('user_take_case', [
            // 'user_uid' => $user->uid,
            // 'case_uid' => $takecasedata['case_uid'],
            // 'position' => $takecasedata['position'],
            // 'status' => 'accept',
            'user_uid' => $user->uid,
            'case_uid' => $takecasedata['case_uid'],
            'case_user_uid' => $casedata->cmember,
            'resume_uid' => $takecasedata['resume_uid'],
            'position_uid' => $takecasedata['position_uid'],
            'status' => 'accept',
            'func' => 'UserTake',
        ]);
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'user_take_case',
            'belong_uid' => $case['result'][0]->uid, //改通知內容
            'from_user_uid' => $user->uid,
            'to_user_uid' => $casedata->belong_member,
            'content' => 'send a resume',
        ]);
        if ($takecasedata['message'] != null) {
            $message = $this->SupportInsert('message', [
                'from_user_uid' => $user->uid,
                'to_user_uid' => $casedata->belong_member,
                'content' => $takecasedata['message'],
            ]);
        } else {
            $message = '';
        }
        $resume = DB::table('resume')->where('uid', $takecasedata['resume_uid'])->where('isopen', 1)->get();

        return [
            $case,
            $notice,
            'resime' => $resume,
            $message,
        ];
    }

    public function accept(Request $request) //發案方接受或拒絕

    {
        // {"data":{"case_uid":"" , "answer":"" , "user_uid":"","position":"" ,"take_uid":""}} case的uid/應徵我case的uid
        $takecasedata = $request['data'];
        $user = Auth::guard('api')->user();
        $CPtmp = DB::table('case_position')->where('uid', $takecasedata['position_uid'])->first();
        $casedata = DB::table('cases')->where('uid', $takecasedata['case_uid'])->first();
        $position = DB::table('case_position')->where('uid', $takecasedata['position_uid'])->first();
        $nuspace = DB::table('margin')->where('belong_uid', $takecasedata['case_uid'])->where('user_uid', $user->uid)->where('position_uid', $takecasedata['position_uid'])->where('to_user_uid', "")->first()->uid;
        $TakeCaseUid = DB::table('user_take_case')->where('case_uid', $takecasedata['case_uid'])->where('user_uid', $takecasedata['user_uid'])->where('position_uid', $takecasedata['position_uid'])->first()->uid;
        $acnum = $CPtmp->acnum;
        $num = $CPtmp->num;
        $casedata = DB::table('cases')->where('uid', $takecasedata['case_uid'])->where('isopen', 1)->where('isIssue', 1)->first();
        if ($takecasedata['answer'] == 'accept') {
            if ($acnum == $num) {
                return [
                    'msg' => 'is full',
                ];
            }

            $notice = $this->SupportInsert('notice', [
                'belong_table' => 'user_take_case',
                'belong_uid' => $TakeCaseUid, //改通知內容
                'from_user_uid' => $casedata->belong_member,
                'to_user_uid' => $takecasedata['user_uid'],
                'content' => 'accept resume',
            ]);
            $CasePosition = $this->SupportUpdate('case_position', [
                'acnum' => $acnum + 1,
            ], ['uid' => $takecasedata['position_uid']]);

            //---------
            if ($position->promisemoney == 0) {
                $case = $this->SupportUpdate('user_take_case', [
                    'status' => 'confirm',
                ], ['uid' => $takecasedata['take_uid']]);

                $this->SupportUpdate('margin', [
                    'to_user_uid' => $user->uid,
                ], [
                    'uid' => $nuspace,
                ]);

                $margin = $this->SupportInsert('margin', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $TakeCaseUid,
                    'user_uid' => $takecasedata['user_uid'],
                    'to_user_uid' => $user->uid,
                    'money' => $position->promisemoney,
                    'processing_fee' => 0, //手續費，之後在做
                    'status' => 'wait',
                    'func' => 'TakeCase',
                ]); //支付保證金

                $TakeCaseCalendarTmp = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
                    $query->where(function ($query) use ($casedata) {
                        $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                            ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                    })
                        ->orWhere(function ($query) use ($casedata) {
                            $query->where('startdate', '<', $casedata->startdate)
                                ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                        })
                        ->orwhere(function ($query) use ($casedata) {
                            $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                                ->where('enddate', '>', $casedata->enddate);
                        })
                        ->orwhere(function ($query) use ($casedata) {
                            $query->where('startdate', '<', $casedata->startdate)
                                ->where('enddate', '>', $casedata->enddate);
                        });
                }])
                    ->whereIn('isopen', [1])
                    ->whereIn('user_uid', [$takecasedata['user_uid']])
                    ->get(); //自己接的相中的案子
                $waitMoneyCase = $TakeCaseCalendarTmp->where('status', 'money');
                $waitAcceptCase = $TakeCaseCalendarTmp->where('status', 'accept');

                $waitAcceptCase->map(function ($item, $keys) {
                    $this->SupportUpdate('user_take_case', [
                        'isopen' => 0,
                    ], ['uid' => $item->uid]);
                }); //相沖的取消投履歷
                $waitMoneyCase->map(function ($item, $keys) {
                    $this->SupportUpdate('user_take_case', [
                        'status' => 'refuse',
                    ], ['uid' => $item->uid]);
                }); //相沖的case拒絕付保證金

                $notice = $this->SupportInsert('notice', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $TakeCaseUid, //改通知內容
                    'from_user_uid' => $takecasedata['user_uid'],
                    'to_user_uid' => $user->uid,
                    'content' => 'paid 0',
                ]);

            } else {
                // $case = $this->SupportUpdate('user_take_case', [
                //     'status' => 'money',
                // ], ['case_uid' => $takecasedata['case_uid'], 'user_uid' => $takecasedata['user_uid'], 'position' => $takecasedata['position'], 'func' => 'UserTake']);
                $case = $this->SupportUpdate('user_take_case', [
                    'status' => 'money',
                ], ['uid' => $takecasedata['take_uid']]);
                $CPtmp = DB::table('case_position')->where('uid', $takecasedata['position_uid'])->first();
                if ($CPtmp->acnum == $CPtmp->num) {
                    $LastTaker = DB::table('user_take_case')->where('case_uid', $takecasedata['case_uid'])->where('status', 'accept')->where('isopen', 1)->get();
                    $LastTaker->map(function ($item, $keys) {
                        $notice = $this->SupportInsert('notice', [
                            'belong_table' => 'user_take_case',
                            'belong_uid' => $item->uid, //改通知內容
                            'from_user_uid' => $item->case_user_uid,
                            'to_user_uid' => $item->user_uid,
                            'content' => 'position is full but not pay',
                        ]);
                        $this->SupportUpdate('user_take_case', [
                            'status' => 'ready',
                        ], ['uid' => $item->uid]);
                    });

                }

            }
            //111111111111

        } else {
            $case = $this->SupportUpdate('user_take_case', [
                'status' => 'refuse',
            ], ['uid' => $takecasedata['take_uid']]);

            $notice = $this->SupportInsert('notice', [
                'belong_table' => 'user_take_case',
                'belong_uid' => $TakeCaseUid, //改通知內容
                'from_user_uid' => $casedata->belong_member,
                'to_user_uid' => $takecasedata['user_uid'],
                'content' => 'refuse resume',
            ]);
        }

        return [
            $case,
        ];
    }

    public function promisemoney(Request $request) //接案方付保證金

    {
        // {"data":{"case_uid":"" , "user_uid":"","position":"","take_uid":""}} 付給哪個case的哪職位的誰
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $nowtime = Carbon::now('Asia/Taipei');
        $CPtmp = DB::table('case_position')->where('uid', $data['position_uid'])->first();
        $beforeMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        $afterMoney = $beforeMoney;
        $promisemoney = $CPtmp->promisemoney; //搜尋該職位保證金
        $userMoney = User::where('uid', $user->uid)->first()->money; //查詢使用者的剩餘金額
        // $TakeCaseUid = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('user_uid', $user->uid)->where('position_uid', $data['position_uid'])->first()->uid;
        $acnum = $CPtmp->acnum;
        $num = $CPtmp->num;
        $nuspace = DB::table('margin')->where('belong_uid', $data['case_uid'])->where('user_uid', $data['user_uid'])->where('position_uid', $data['position_uid'])->where('to_user_uid', "")->where('status', 'wait')->first()->uid;
        $casedata = DB::table('cases')->where('uid', $data['case_uid'])->where('isopen', 1)->where('isIssue', 1)->first();
        $LastTaker = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->whereIn('status', ['accept','ready'])->where('isopen', 1)->get();
        // return $TakeCaseUid;

        if ($userMoney >= $promisemoney) {
            $Money = $this->SupportUpdate('users', [
                'money' => $userMoney - $promisemoney,
            ], ['uid' => $user->uid]); //從帳戶扣錢
            $margin = $this->SupportInsert('margin', [
                'belong_table' => 'user_take_case',
                'belong_uid' => $data['take_uid'],
                'user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'money' => $promisemoney,
                'processing_fee' => 0, //手續費，之後在做
                'status' => 'wait',
                'func' => 'TakeCase',
                'position_uid' => $data['position_uid'],
                'position' => $CPtmp->name,
            ]); //支付保證金
            $this->SupportUpdate('margin', [
                'to_user_uid' => $user->uid,
            ], [
                'uid' => $nuspace,
            ]);
            $takecase = $this->SupportUpdate('user_take_case', [
                'status' => 'confirm',
            ], ['uid' => $data['take_uid']]); //付完保證金
            $LastTaker = $LastTaker->map(function ($item, $keys) {
                $notice = $this->SupportInsert('notice', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $item->uid,
                    'from_user_uid' => $item->case_user_uid,
                    'to_user_uid' => $item->user_uid,
                    'content' => 'position is full ', //職位已滿 不用等待
                ]);
            });

            $TakeCaseCalendarTmp = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
                $query->where(function ($query) use ($casedata) {
                    $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                        ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                })
                    ->orWhere(function ($query) use ($casedata) {
                        $query->where('startdate', '<', $casedata->startdate)
                            ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                    })
                    ->orwhere(function ($query) use ($casedata) {
                        $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                            ->where('enddate', '>', $casedata->enddate);
                    })
                    ->orwhere(function ($query) use ($casedata) {
                        $query->where('startdate', '<', $casedata->startdate)
                            ->where('enddate', '>', $casedata->enddate);
                    });
            }])
                ->whereIn('isopen', [1])
                ->whereIn('user_uid', [$user->uid])
                ->get(); //自己接的相中的案子
            $waitMoneyCase = $TakeCaseCalendarTmp->where('status', 'money');
            $waitAcceptCase = $TakeCaseCalendarTmp->where('status', 'accept');

            $waitAcceptCase->map(function ($item, $keys) {
                $this->SupportUpdate('user_take_case', [
                    'isopen' => 0,
                ], ['uid' => $item->uid]);
            }); //相沖的取消投履歷
            $waitMoneyCase->map(function ($item, $keys) {
                $this->SupportUpdate('user_take_case', [
                    'status' => 'refuse',
                ], ['uid' => $item->uid]);
            }); //相沖的case拒絕付保證金

            $notice = $this->SupportInsert('notice', [
                'belong_table' => 'user_take_case',
                'belong_uid' => $data['take_uid'], //改通知內容
                'from_user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'content' => 'already paid',
            ]);

        } else {
            return ['msg' => 'money is not enough'];
        }

        $afterMoney = $afterMoney - $promisemoney;

        //計算已經付保證金的人
        $countPerson = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('status', 'confirm')->get()->count();
        $TotalPerson = DB::table('case_position')->where('case_uid', $data['case_uid'])->sum('num'); //此總共案需付保證金的人

        $allPeopel = UserTakeCase::where('case_uid', $data['case_uid'])->whereIn('status', ['accept', 'check', 'money'])->get(); //接此案的所有人

        if ($countPerson == $TotalPerson) {
            $notice = $this->SupportInsert('notice', [
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => 'system',
                'to_user_uid' => $data['user_uid'],
                'content' => 'all people are pay',
            ]);

            return [
                'beforeMoney' => $beforeMoney, //付款前
                'afterMoney' => $afterMoney, //付款後
                $notice,
                $margin,
                'promisemoney' => $promisemoney,
            ];
        } else {
            return [
                'beforeMoney' => $beforeMoney, //付款前
                'afterMoney' => $afterMoney, //付款後
                $margin,
                'promisemoney' => $promisemoney,
            ];
        }

    }

    public function refusePromisemoney(Request $request) //拒絕付保證金

    {
        //{"data":{"user_uid:"","case_uid":"","position":""}}
        // 接案狀態改refuse 、
        // return $request->all();
        $data = $request['data'];
        // return $data['data'];
        $user = Auth::guard('api')->user();
        $TakeCaseUid = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('user_uid', $user->uid)->where('position', $data['position'])->first()->uid;
        $tCase = $this->SupportUpdate('user_take_case', [
            'status' => 'refuse',
        ], [
            'user_uid' => $user->uid,
            'case_uid' => $data['case_uid'],
            'position_uid' => $data['position_uid'],
        ]);
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'user_take_case',
            'belong_uid' => $TakeCaseUid, //改通知內容
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'refuse promisemoney',
        ]);
        return $tCase;
    }

    // public function defPerson(Request $request) //接受履歷後付保證金前拒絕

    // {
    //     //{"data":{"user_uid":"","case_uid":"","position":""}}
    //     $data = $request['data'];
    //     $user = Auth::guard('api')->user();
    //     $tcase = $this->SupportUpdate('user_take_case', [
    //         'status' => 'refuse',
    //     ], [
    //         'user_uid' => $data['user_uid'],
    //         'case_uid' => $data['case_uid'],
    //         'position' => $data['position'],
    //     ]);

    //     $ispay = DB::table('margin')
    //         ->where('user_uid', $data['user_uid'])
    //         ->where('case_uid', $data['case_uid'])
    //         ->where('position', $data['position'])
    //         ->get()
    //         ->isEmpty(); //空的回傳true

    //     if (!$ispay) {
    //         $margin = $this->SupportUpdate('margin', [
    //             'status' => 'return',
    //         ], [
    //             'user_uid' => $data['user_uid'],
    //             'case_uid' => $data['case_uid'],
    //             'position' => $data['position'],
    //         ]);
    //     }

    //     return $margin;
    // }

    // public function promisemoneyCaseSide(Request $request)
    // {
    //     // {"data":{"case_uid":"" , "user_uid":"","position":""}} 付給哪個case的哪職位的誰
    //     $data = $request['data']['data'];
    //     $user = Auth::guard('api')->user();
    //     $beforeMoney = DB::table('users')->where('uid' , $user->uid)->first()->money;
    //     $afterMoney = $beforeMoney;
    //     $data = $request['data']['data'];
    //     $user = Auth::guard('api')->user();
    //     $promisemoney = CasePosition::where(['name' => $data['position'], 'case_uid' => $data['case_uid']])->first()->promisemoney; //搜尋該職位保證金
    //     $userMoney = User::where('uid', $user->uid)->first()->money; //查詢使用者的剩餘金額
    //     $TakeCaseUid = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('user_uid', $data['user_uid'])->where('position', $data['position'])->first()->uid;
    //     $acnum = DB::table('case_position')->where('case_uid', $data['case_uid'])->where('name', $takecasedata['position'])->first()->acnum;
    //     $num = DB::table('case_position')->where('case_uid', $data['case_uid'])->where('name', $takecasedata['position'])->first()->num;
    //     $ispaid = DB::table('margin')
    //         ->where('user_uid', $data['user_uid'])
    //         ->where('to_user_uid', $user->uid)
    //         ->where('belong_table', 'user_take_case')
    //         ->where('belong_uid', $TakeCaseUid)
    //         ->get(); //查詢對方有無付款
    //     if ($userMoney >= $promisemoney) {
    //         if (!$ispaid->isEmpty()) //對方已付款
    //         {
    //             $Money = $this->SupportUpdate('users', [
    //                 'money' => $userMoney - $promisemoney,
    //             ], ['uid' => $user->uid]); //從帳戶扣錢
    //             $margin = $this->SupportInsert('margin', [
    //                 'belong_table' => 'user_take_case',
    //                 'belong_uid' => $TakeCaseUid,
    //                 'user_uid' => $user->uid,
    //                 'to_user_uid' => $data['user_uid'],
    //                 'money' => $promisemoney,
    //                 'processing_fee' => 0, //手續費，之後在做
    //                 'status' => 'wait',
    //                 'func' => 'TakeCase',
    //             ]); //支付保證金
    //             $takecase = $this->SupportUpdate('user_take_case', [
    //                 'status' => 'confirm',
    //             ], ['user_uid' => $data['user_uid'],
    //                 'case_uid' => $data['case_uid'],
    //                 'position' => $data['position']]); //付完保證金

    //             $notice = $this->SupportInsert('notice', [
    //                 'to_user_uid' => $user->uid,
    //                 'content' => 'other side are already paid',
    //             ]);
    //             $notice = $this->SupportInsert('notice', [
    //                 'to_user_uid' => $data['user_uid'],
    //                 'content' => 'other side are already paid',
    //             ]);

    //         } else {
    //             $Money = $this->SupportUpdate('users', [
    //                 'money' => $userMoney - $promisemoney,
    //             ], ['uid' => $user->uid]); //從帳戶扣錢
    //             $margin = $this->SupportInsert('margin', [
    //                 'belong_table' => 'user_take_case',
    //                 'belong_uid' => $TakeCaseUid,
    //                 'user_uid' => $user->uid,
    //                 'to_user_uid' => $data['user_uid'],
    //                 'money' => $promisemoney,
    //                 'processing_fee' => 0, //手續費，之後在做
    //                 'status' => 'wait',
    //                 'func' => 'TakeCase',
    //             ]); //支付保證金
    //         }
    //     } else {
    //         return ['msg' => 'money is not enough'];
    //     }

    //     $afterMoney = $afterMoney - $promisemoney;

    //     $countPerson = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('status', 'confirm')->get()->count();
    //     $TotalPerson = DB::table('case_position')->where('case_uid', $data['case_uid'])->sum('num'); //此總共案需付保證金的人

    //     // return $TotalPerson;
    //     //需付保證金的總人數
    //     $allPeopel = UserTakeCase::where('case_uid', $data['case_uid'])->get(); //接此案的所有人

    //     if ($num == $acnum) { //如果應徵滿了 其他應徵者自動變拒絕
    //         $deny = $this->SupportUpdate('user_take_case', [
    //             'status' => 'refuse',
    //         ], [
    //             'case_uid' => $takecasedata['case_uid'],
    //             'position' => $takecasedata['position'],
    //             'status' => 'accept',
    //         ]);
    //     }

    //     // return [$ispaid->isEmpty()];

    //     if ($countPerson == ($TotalPerson * 2)) {
    //         $notice = $allPeopel->map(function ($item, $keys) {
    //             $notice = $this->SupportInsert('notice', [
    //                 // 'from_user_uid' => 'system',
    //                 'to_user_uid' => $item['user_uid'],
    //                 'content' => 'all people are pay for',
    //             ]);
    //         });

    //         return [
    //             'beforeMoney' => $beforeMoney, //付款前
    //             'afterMoney' => $afterMoney,   //付款後
    //             $notice,
    //             $margin,
    //         ];
    //     } else {
    //         return [
    //             'beforeMoney' => $beforeMoney, //付款前
    //             'afterMoney' => $afterMoney,   //付款後
    //             $margin
    //         ];
    //     }

    // }
}
