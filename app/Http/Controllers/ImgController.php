<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\PublicCtr;
use Illuminate\Support\Facades\Storage;
use Validator;

class ImgController extends Controller
{
    private $LocalURL = "http://localhost/Mageematch-api/storage/pics/";
    // private $LocalURL = "http://cmt.chinchilla-zone.com/mageematch-api/storage/pics/";
    use PublicCtr\Support;
    public function showImg(Request $request)
    {
        // {"category":""}
        $category = $request['category'];
        $user = Auth::guard('api')->user();
        $img = DB::table('img')->where('belong_member' , $user->uid)->where('isopen' , 1)->where('category' , $category)->get();
        return $img;
    }

    public function uploadImg(Request $request)
    {
        // 頭像 avatar 、證照licence、景點attraction、
        // return $request->file();
        $user = Auth::guard('api')->user();
        if($request['category'] == 'avatar'){
            $userAvatar = DB::table('img')->where('belong_member' , $user->uid)->where('category' , $request['category'])->where('isopen' , 1)->first();
            if($userAvatar != null){
                $setIsopen = $this->SupportUpdate('img' , ['isopen' => 0 ] , [ 'uid' => $userAvatar->uid]);
            }
        }
        $nowtime = Carbon::now('Asia/Taipei');
        $file =  $request->file('file');
        $fileName =  Carbon::parse($nowtime)->format('Y-M-D').'-'.$request['filename'];
        $file->move(Storage_path()."//pics/",$fileName);
        $path = $this->LocalURL.$fileName;

        $Img = $this->SupportInsert('img',[
            'category' => $request['category'],
            'path' => $path,
            'isopen' => 1
        ]);

        return $Img;
    }

    public function delectImg(Request $request)
    {
        $data = $request['img_uid'];
        $user = Auth::guard('api')->user();
        $Img = $this->SupportUpdate('',[
            'isopen' => 0
        ],[
            'uid' => $data['img_uid']
        ]);
        return $Img;
    }
}
