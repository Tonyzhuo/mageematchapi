<?php

namespace App\Http\Controllers;

use App\CaseSaveUser;
use App\Http\Controllers\PublicCtr;
use App\member;
use App\resume;
use App\TheCases;
use App\User;
use App\UserSaveCase;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class publicrequestController extends Controller
{
    use PublicCtr\Support;
    public function publicrequest(Request $request)
    {
        // return $request->all();
        $public = DB::table($request['name'])
            ->where('isopen', 1)
            ->get();

        return [
            'table' => $request['name'],
            'state' => 'success',
            'result' => $public,
        ];
    }

    public function showSaveCase(Request $request) //顯示已收藏的case

    {
        //{"data":{""}}
        // return 'aa';
        // $data = $request['data'];
        // return $data;
        $user = Auth::guard('api')->user();
        $case = DB::table('user_save_case')
            ->where('user_uid', $user->uid)
            ->where('isopen', 1)
            ->orderBy('createtime', 'descs')
            ->get();
        $today = Carbon::now();
        $showCase = $case->map(function ($item, $keys) use ($user) {
            $search = DB::table('cases')->where('uid', $item->case_uid)->first();
            $search->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $item->case_uid, 'isopen' => 1])->count() > 0);
            return $search;
        });
        $showCase = $showCase->map(function ($item, $keys) use ($today) {
            $item->category = explode(',', $item->category);
            // $item->position = DB::table('case_position')->where('case_uid' , $item->case_uid)->get();
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get();
            $item->position = $item->position->map(function ($item, $keys) {
                $num = DB::table('case_position')->where('case_uid', $item->case_uid)->where('name', $item->name)->first()->num;
                $acnum = DB::table('case_position')->where('case_uid', $item->case_uid)->where('name', $item->name)->first()->acnum;
                $item->isLast = ($num == $acnum) ? "full" : $num - $acnum;
                return $item;
            });
            // $item->case = DB::table('cases')->where('uid', $item->case_uid)->get();
            $item->isOverdue = ((floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';

            return $item;
        });

        return $showCase;
        return [
            'table' => 'user_save_case',
            'state' => 'true',
            'result' => $showCase,
        ];

    }

    public function county(Request $request)
    { //縣市
        $county = DB::table('post_code')
            ->select('city_name')
            ->distinct()
            ->get();

        return [
            'table' => 'post_code',
            'state' => 'success',
            'result' => $county,
        ];
    }

    public function street(Request $request)
    { //區域
        $street = DB::table('post_code')
            ->where('city_name', $request['name'])
            ->get();
        return [
            'table' => 'post_code',
            'state' => 'success',
            'result' => $street,
        ];
    }

    public function cityArea(Request $request)
    {
        $name = $request['name'];
        $CA = DB::table('post_code')
            ->where('city_name', $name)
            ->select(DB::raw('concat(city_name , "-" , area_name) as city_area'))
            ->get();
        return $CA;
    }

    public function licenceCategory(Request $request)
    {
        $licence = DB::table('licence_code')
            ->select('category')
            ->distinct()
            ->get();

        return [
            'table' => 'licence_code',
            'status' => 'true',
            'result' => $licence,
        ];
    }

    public function licenceName(Request $request)
    {
        // {"category":"證照類別"}
        $data = $request['category'];
        $licence = DB::table('licence_code')
            ->where('category', $data)
            ->get();

        return [
            'table' => 'licence_code',
            'status' => 'true',
            'result' => $licence,
        ];
    }

    public function detail(Request $request) //顯示case詳細資料

    {
        // {"data":{"name":"","uid":""}}
        // return $request->all();
        // return 'aa';
        // return $request['data']['name'];

        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $today = Carbon::today();
        $detail = DB::table($data['name'])
            ->where('uid', $data['uid'])
            ->where('isopen', 1)
            ->first();
        // return $detail;
        $position = DB::table('case_position')
            ->where('case_uid', $data['uid'])
            ->where('isopen', 1)
            ->get();

        $detail->category = explode(',', $detail->category);
        // $detail->location = explode(',', $detail->location);
        $position = $position->map(function ($item, $keys) {
            $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
            return $item;
        });
        $detail->isOverdue = ((floor(strtotime($detail->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($detail->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
        $detail->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $detail->uid, 'isopen' => 1])->count() > 0);
        $detail->companyData = DB::table('companys')->where('belong_member', $user->uid)->first();

        // $click = $this->SupportInsert('case_click' , ['case_uid' => $data['uid']]);

        return [$detail, $position];

    }

    public function unLoginDetail(Request $request)
    {
        $data = $request['data'];
        // $user = Auth::guard('api')->user();
        $today = Carbon::today();
        $detail = DB::table($data['name'])
            ->where('uid', $data['uid'])
            ->first();
        // return $detail;
        $position = DB::table('case_position')
            ->where('case_uid', $data['uid'])
            ->where('isopen', 1)
            ->get();

        $detail->category = explode(',', $detail->category);
        // $detail->location = explode(',', $detail->location);
        $position = $position->map(function ($item, $keys) {
            $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
            return $item;
        });
        $detail->isOverdue = ((floor(strtotime($detail->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($detail->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
        // $detail->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $detail->uid, 'isopen' => 1])->count() > 0);
        // return $detail;

        return [$detail, $position];
    }

    public function detailPerson(Request $request) //顯示person詳細資料

    {
        //{"user_uid":""}
        // return $request->all();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $data = $request['user_uid'];
        // return $data;
        $user = Auth::guard('api')->user();
        $detailPerson = DB::table('members')
            ->where('user_uid', $data)
            ->first();
        $detailPerson->isSave = (CaseSaveUser::where(['cmember' => $user->uid, 'user_uid' => $detailPerson->user_uid, 'isopen' => 1])->count() > 0);

        $detailPerson->resume = DB::table('resume')->where('belong_member', $detailPerson->user_uid)->where('isopen', 1)->where('isPreset', 1)->first();
        $detailPerson->language = DB::table('user_language_ability')->where('user_uid', $data)->where('isopen', 1)->get();
        $detailPerson->img = (DB::table('img')->where('belong_member', $data)->where('category', 'avatar')->where('isopen' , 1)->first() != null) ? DB::table('img')->where('belong_member', $data)->where('category', 'avatar')->where('isopen' , 1)->first()->path : $avatar;
        // $click = $this->SupportInsert('person_click' , ['user_uid' => $data]);
        return [
            'table' => 'members',
            'status' => 'true',
            'result' => $detailPerson,
        ];
    }
    public function ShowTakeCase(Request $request)
    { //接案管理\顯示應徵的case
        // {"status":""}
        $data = $request['status'];
        $user = Auth::guard('api')->user();
        $calendar = DB::table('calendar')->where('user_uid', $user->uid)->where('isopen', 1)->get();
        $takeCase = DB::table('user_take_case')->where('user_uid', $user->uid)->whereIn('status', ['money', 'confirm'])->where('isopen', 1)->get();
        $takeCase = $takeCase->map(function ($item, $keys) {
            $item->casedate = DB::table('cases')->where('uid', $item->case_uid)->select('startdate', 'enddate', 'uid')->first();
            return $item;
        });
        if ($data == 'money') {
            $case = DB::table('user_take_case')
                ->where('user_uid', $user->uid)
                ->whereIn('status', ['check', 'money'])
                ->where('isopen', 1)
                ->orderBy('createtime', 'desc')
                ->get()
                ->groupBy('func');
        } else {
            $case = DB::table('user_take_case')
                ->where('user_uid', $user->uid)
                ->where('status', $data)
                ->where('isopen', 1)
                ->orderBy('createtime', 'desc')
                ->get()
                ->groupBy('func');
        }

        $case = $case->map(function ($item, $keys) use ($user, $calendar, $takeCase) {
            $item = $item->map(function ($item, $keys) use ($user, $calendar, $takeCase) {
                // if($item->)
                // $DefaultCase = DB::table('cancel_reason')->where('take_uid' )
                $caseData = DB::table('cases')->where('uid', $item->case_uid)->where('isopen', 1)->first();
                $item->casename = $caseData->casename;
                $item->location = $caseData->location;
                $item->startdate = $caseData->startdate;
                $item->enddate = $caseData->enddate;
                // $item->needtime_startdate = TheCases::where('uid', $item->case_uid)->first()->needtime_startdate;
                // $item->needtime_enddate = TheCases::where('uid', $item->case_uid)->first()->needtime_enddate;
                $item->totaldays = $caseData->totaldays;
                $item->promisemoney = DB::table('case_position')->where('case_uid', $item->case_uid)->where('isopen', 1)->get();
                $item->releasedate = $caseData->releasedate;
                $item->isPay = (DB::table('margin')->where('user_uid', $user->uid)->where('belong_table', 'user_take_case')->where('belong_uid', $item->uid)->count() > 0);
                $item->ownMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
                $item->Money = DB::table('case_position')->where('uid', $item->position_uid)->first()->promisemoney;
                $Calendaraa = $calendar->where('startdate', '>=', $item->startdate)->where('enddate', '>=', $item->enddate)->where('startdate', '<=', $item->enddate)->isNotEmpty();
                $Calendarbb = $calendar->where('startdate', '<=', $item->startdate)->where('enddate', '<=', $item->enddate)->where('enddate', '>=', $item->startdate)->isNotEmpty();
                $Calendarcc = $calendar->where('startdate', '<=', $item->startdate)->where('enddate', '>=', $item->enddate)->isNotEmpty();
                $Calendardd = $calendar->where('enddate', '>=', $item->startdate)->where('startdate', '>=', $item->startdate)->where('enddate', '<=', $item->enddate)->where('startdate', '<=', $item->enddate)->isNotEmpty();

                $takeCaseaa = $takeCase->where('casedate.startdate', '>=', $item->startdate)->where('casedate.enddate', '>=', $item->enddate)->where('casedate.startdate', '<=', $item->enddate)->isNotEmpty();
                $takeCasebb = $takeCase->where('casedate.startdate', '<=', $item->startdate)->where('casedate.enddate', '<=', $item->enddate)->where('casedate.enddate', '>=', $item->startdate)->isNotEmpty();
                $takeCasecc = $takeCase->where('casedate.startdate', '<=', $item->startdate)->where('casedate.enddate', '>=', $item->enddate)->isNotEmpty();
                $takeCasedd = $takeCase->where('casedate.enddate', '>=', $item->startdate)->where('casedate.startdate', '>=', $item->startdate)->where('casedate.enddate', '<=', $item->enddate)->where('casedate.startdate', '<=', $item->enddate)->isNotEmpty();
                if ($Calendaraa == 'true' || $Calendarbb == 'true' || $Calendarcc == 'true' || $Calendardd == 'true') {
                    $item->isCalendarConflict = 'true';
                } else {
                    $item->isCalendarConflict = 'false';
                }
                if ($takeCaseaa == 'true' || $takeCasebb == 'true' || $takeCasecc == 'true' || $takeCasedd == 'true') {
                    $item->isTakeCaseConflict = 'true';
                } else {
                    $item->isTakeCaseConflict = 'false';
                }
                $Conflict = ($item->isTakeCaseConflict == 'true' || $item->isCalendarConflict == 'true') ? 'true' : 'false';
                if ($Conflict == 'true') {
                    $item->isConflict = 'have conflict';
                } else {
                    $item->isConflict = 'no conflict';
                }

                return $item;
            });
            return $item;
        });

        return [
            'table' => 'user_take_case',
            'state' => 'success',
            'result' => $case,
        ];
    }

    public function ShowOwnCase(Request $request)
    { //接案管理/顯示被應徵的case
        //{"status":""}
        $data = $request['status'];
        $user = Auth::guard('api')->user();
        $today = Carbon::now();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        if ($data == 'money') {
            $OwnCase = DB::table('user_take_case')
                ->where('case_user_uid', $user->uid)
                ->whereIn('status', ['money', 'check'])
                ->where('isopen', 1)
                ->orderBy('createtime', 'desc')
                ->get();
        } else {
            $OwnCase = DB::table('user_take_case')
                ->where('case_user_uid', $user->uid)
                ->where('status', $data)
                ->where('isopen', 1)
                ->orderBy('createtime', 'desc')
                ->get();
        }

        $OwnCase = $OwnCase->map(function ($item, $keys) use ($user, $today, $avatar) {
            $caseTMP = TheCases::where('uid', $item->case_uid)->first();
            $cpTMP = DB::table('case_position')->where('uid', $item->position_uid)->first();
            $item->username = DB::table('members')->where('user_uid', $item->user_uid)->first()->username;
            $item->case = $caseTMP;
            $item->casename = $caseTMP->casename;
            $item->location = $caseTMP->location;
            $item->startdate = $caseTMP->startdate;
            $item->enddate = $caseTMP->enddate;
            $item->needtime_startdate = $caseTMP->needtime_startdate;
            $item->needtime_enddate = $caseTMP->needtime_enddate;
            $item->totaldays = $caseTMP->totaldays;
            $item->promisemoney = $cpTMP->promisemoney;
            $item->position = $cpTMP->name;
            $item->releasedate = DB::table('cases')->where('uid', $item->case_uid)->first()->releasedate;
            $item->isPay = (DB::table('margin')->where('user_uid', $user->uid)->where('belong_table', 'user_take_case')->where('belong_uid', $item->uid)->count() > 0);
            $item->isOverdue = ((floor(strtotime($item->case->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->case->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->ownMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
            $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
            // $item->user_uid=DB::table('cases')->where('uid' , $item->case_uid)->first()->cmember;
            return $item;
        });

        return [
            'table' => 'user_take_case',
            'state' => 'success',
            'result' => $OwnCase,
        ];
    }

    public function filterCase(Request $request)
    { //資料篩選
        //{"data":{"category":"","totaldays":"","scale":"","language":"","location":"","startdate":"","enddate":"","mix":"薪水間距","max":""}}
        // $tags = '歷史探索','極限挑戰','美食挖掘','購物血拼','文藝欣賞','景點觀光';
        // return $request->all();
        // dd($location);
        // $RawFindSet=collect([["location"=>$location ,"category"=>$category]);
        // ->filter(function (){
        //     return $item->CasePosition->count()>0;
        //    })->values()
        // => function($query)use($filter , $language){
        //     $query->when($language , function($query)use($language){
        //         collect(explode(",","{$language}"))->each(function($item)use($query){
        //             $query->orwhere(function($query)use($item){
        //                 $query->where('language' , "{$item}");
        //             });
        //         });
        //         // $query->where('language' , $language);
        //     });
        // }
        $filter = $request['data'];
        // return $filter;
        $page = 0;
        $pagenumber = 20;
        // return $filter;
        if ($filter['category'] != null) {
            $tags = implode(',', $filter['category']); //傳陣列
        } else {
            $tags = $filter['category'];
        }
        if ($filter['location'] != null) {
            $location = implode(',', $filter['location']);
        } else {
            $location = $filter['location'];
        }
        if ($filter['language'] != null) {
            $language = implode(',', $filter['language']);
        } else {
            $language = $filter['language'];
        }
        // ->where(function ($query) use ($filter) {
        //         if ($filter['min'] && $filter['max'] != null) {
        //             $query->whereBetween('salary', [$filter['min'], $filter['max']]);
        //         }
        //     });
        $data = TheCases::whereExists(function ($query) use ($language, $filter) {
            if ($language != null) {
                $exlanguage = collect(explode(",", "{$language}"));
                $query->select(DB::Raw("1"))->from('case_position')
                    ->whereRaw('case_position.case_uid = cases.uid and case_position.language in (\'' . $exlanguage->implode("','") . '\')');
            }
            if ($filter['min'] && $filter['max'] != null) {
                //    field  between A and B
                $query->select(DB::Raw("1"))->from('case_position')
                    ->whereBetween('salary', [$filter['min'], $filter['max']])
                    ->whereColumn('case_position.case_uid', '=', 'cases.uid');
                // ->whereRaw('case_position.case_uid = cases.uid and case_position.salary between (\'' . $filter['min'] . 'and' . $filter['max'] . '\')');
            }
        })
            ->with(['CasePosition'])
            ->where(function ($query) use ($tags) {
                $query->when($tags, function ($query) use ($tags) {
                    collect(explode(",", "{$tags}"))->each(function ($item) use ($query) {
                        $query->orWhere(function ($query) use ($item) {
                            $query->whereRaw("FIND_IN_SET('{$item}',`category`)");
                        });
                    });
                });
            })
            ->where(function ($query) use ($filter) {
                $query->where(function ($query) use ($filter) {
                    if ($filter['totaldays'] != null) {
                        if ($filter['totaldays'] == '3天以內') // 0-3
                        {
                            $query->where('totaldays', '<', 4);
                        } elseif ($filter['totaldays'] == '7天以內') //4-7
                        {
                            $query->where('totaldays', '<', 7);
                        } elseif ($filter['totaldays'] == '7天以上') //7-all
                        {
                            $query->where('totaldays', '>', 7);
                        }
                    }
                });
            })
            ->where(function ($query) use ($filter) {
                $query->when($filter['scale'], function ($query) use ($filter) {
                    if ($filter['scale'] == '小型') {
                        $query->where('peoplenum', '<', 11); // [0,11]
                    } elseif ($filter['scale'] == '中型') {
                        $query->whereBetween('peoplenum', [11, 30]); // [11,30]
                    } elseif ($filter['scale'] == '大型') {
                        preg_match("/all/g", $string);
                        $query->where('peoplenum', '>', 30); // 300-all
                    }
                });
            })
            ->where(function ($query) use ($location) {
                $query->when($location, function ($query) use ($location) {
                    collect(explode(",", "{$location}"))->each(function ($item) use ($query) {
                        // $query->orWhere(function ($query) use ($item) {
                        //     $query->whereRaw("FIND_IN_SET('{$item}',`location`)");
                        // });
                        $query->where('location', 'like', '%' . $item . '%');
                    });
                });
            })
            ->where(function ($query) use ($filter) {
                if ($filter['start'] && $filter['end'] != null) {
                    $query->whereBetween('startdate', [$filter['start'], $filter['end']])
                        ->whereBetween('enddate', [$filter['start'], $filter['end']]);
                }
            })
            ->where('isIssue', 1)
            ->where('isopen', 1)
            ->where('status', 'wait')
            ->orderBy('createtime', 'desc');
        // ->get();

        $countData = clone $data;
        $data = $data->skip($page * $pagenumber)->take($pagenumber)->get();
        $countData = $countData->count();
        $today = Carbon::today();
        $user = Auth::guard('api')->user();

        $data = $data->map(function ($item, $keys) use ($user, $today) {
            $item->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $item->uid, 'isopen' => 1])->count() > 0);
            $item->isOverdue = ((floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->get();
            $item->category = explode(',', $item->category);
            $item->position = $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                return $item;
            });
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'success',
            'count' => $countData,
            'result' => $data,
        ];
    }

    public function filterPerson(Request $request) //篩選人才

    {
        // {"data":{"gender":"","project":"","area":"","language":""}}

        $user = Auth::guard('api')->user();
        $filterdata = $request['data'];
        $page = 0;
        $pagenumber = 20;
        $currentPage = 1;

        if ($filterdata['project'] != null) {
            $project = implode(',', $filterdata['project']);
        } else {
            $project = $filterdata['project'];
        }
        if ($filterdata['area'] != null) {
            $area = implode(',', $filterdata['area']);
        } else {
            $area = $filterdata['area'];
        }
        if ($filterdata['language'] != null) {
            $language = implode(',', $filterdata['language']);
        } else {
            $language = $filterdata['language'];
        }

        $person = member::whereExists(function ($query) use ($filterdata, $language) {
            if ($language != null) {
                $exlanguage = collect(explode(",", "{$language}"));
                $query->select(DB::raw(1))
                    ->from('user_language_ability')
                    ->whereRaw('user_language_ability.user_uid = members.user_uid and user_language_ability.language in (\'' . $exlanguage->implode("','") . '\')');
            }
        })
            ->with(['LanguageAbility'])
            ->where('isMember', 1)
            ->where('isopen', 1)
            ->where(function ($query) use ($filterdata) {
                $query->when($filterdata['gender'], function ($query) use ($filterdata) {
                    $query->where('gender', $filterdata['gender']);
                });
            })
            ->where(function ($query) use ($project) {
                $query->when($project, function ($query) use ($project) {
                    collect(explode(",", "{$project}"))->each(function ($item) use ($query) {
                        $query->orWhere(function ($query) use ($item) {
                            $query->whereRaw("FIND_IN_SET('{$item}',`skill`)");
                        });
                    });
                });
            })
            ->where(function ($query) use ($area) {
                $query->when($area, function ($query) use ($area) {
                    $query->where('location', 'like', $area . '%');
                });
            })
            ->orderBy('createtime', 'desc');
        // ->get();
        $countperson = clone $person;
        $person = $person->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countperson = $countperson->count();

        return [
            'table' => 'members',
            'state' => 'true',
            'result' => $person,
            'count' => $countperson,
        ];
    }

    public function checkPerson(Request $request)
    { //顯示審核人才
        // 回傳履歷/應徵者姓名/應徵資料
        // return $request->all();
        $user = Auth::guard('api')->user();
        $today = Carbon::now();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $case = DB::table('cases')->where('cmember', $user->uid)->where('isopen', 1)->get();
        $checkPerson = DB::table('user_take_case')
            ->where('status', 'accept')
            ->where('case_user_uid', $user->uid)
            ->where('isopen', 1)
            ->orderBy('createtime', 'desc')
            ->get();
        // $checkPerson = $checkPerson->map(function($item,$keys){
        //     $item->name = DB::table('members')->where('user_uid' , $item->user_uid)->first()->username;
        //     return $item;
        // });
        // return $checkPerson;

        $checkPerson = $checkPerson->map(function ($item, $keys) use ($today, $avatar) {
            $CPtmp = DB::table('case_position')->where('uid', $item->position_uid)->first();
            $item->name = DB::table('members')->where('user_uid', $item->user_uid)->first()->username;
            $item->case = TheCases::where('uid', $item->case_uid)->first();
            $item->resume = resume::where('uid', $item->resume_uid)->first();
            $item->isOverdue = ((floor(strtotime($item->case->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->case->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
            $item->Money = $CPtmp->promisemoney;
            $item->position = $CPtmp->name;
            // dd($userinfo);
            return $item;
        });

        // return $checkPerson;
        return [
            'table' => 'user_take_case',
            'state' => 'true',
            'result' => $checkPerson,
        ];
    }

    public function showSavePerson(Request $request) //顯示收藏人才

    {
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $user = Auth::guard('api')->user();
        $person = DB::table('case_save_user')
            ->where('case_user_uid', $user->uid)
            ->where('isopen', 1)
            ->get();
        // return $person;
        $showPerson = $person->map(function ($item, $keys) use ($user, $avatar) {
            $item->user = DB::table('members')->where('user_uid', $item->user_uid)->get();
            $item->isSave = (DB::table('case_save_user')->where(['cmember' => $user->uid, 'user_uid' => $item->user_uid, 'isopen' => 1])->count() > 0);
            $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first()->path : $avatar;
            return $item;
        });
        return [
            'table' => 'case_save_user',
            'state' => 'true',
            'result' => $showPerson,
        ];
    }

    public function transaction(Request $request) //顯示交易明細

    {
        $page = 0;
        $pagenumber = 20;
        $currentPage = $request['page'];
        $user = Auth::guard('api')->user();
        $memberCollection = DB::table('users')->where('uid', $user->uid)->first();
        $beforeMoney = ($memberCollection) ? $memberCollection->money : 0;
        $afterMoney = ($memberCollection) ? $memberCollection->money : 0;
        $tmpSelect = DB::table('margin')->where('user_uid', $user->uid)->orderBy('createtime', 'desc');
        $count = clone $tmpSelect;
        $tmpSelect = $tmpSelect->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $count = $count->count();

        $output = $tmpSelect->whereIn("status", ["wait", "return"])->values();
        $input = $tmpSelect->filter(function ($item, $keys) use ($user) {
            return $item->status == 'addValue' ||
            $item->final_return == $user->uid;
        })->values();
        $OnTheTransaction = DB::table('margin')->where('user_uid', $user->uid)->where('status', 'wait')->sum('money');
        $output = $output->map(function ($item, $keys) {
            $item->to_username = DB::table('members')->where('user_uid', $item->to_user_uid)->first();
            if ($item->to_username != null) {
                $item->to_username = $item->to_username->username;
            }
            return $item;
        });
        $tmpuid = explode(",", $input->implode("user_uid", ",")); //input分割 再把user_uid 連結陣列
        $tmpDB = DB::table('members')->whereIn('user_uid', $tmpuid)->first();
        $input = $input->map(function ($item, $keys) use ($tmpDB) {
            if ($item->status == 'addValue') {
                $item->source = 'addValue';
            } else {
                $item->source = $tmpDB->username;
            }
            return $item;
        });
        $TotalOutputMoney = $output->sum('money');
        $TotalInputMoney = $input->sum('money');
        $CountOutput = $output->count();
        $CountInput = $input->count();
        $totalCount = $CountOutput + $CountInput;

        $beforeMoney = $beforeMoney + $TotalOutputMoney;
        $beforeMoney = $beforeMoney - $TotalInputMoney;

        $tmpTime = DB::table('margin')->where('user_uid', $user->uid)->get();
        $addValue = $tmpTime->whereIn('status', 'addValue')->max('createtime');
        $wait = $tmpTime->whereIn('status', 'wait')->max('createtime');
        $retStatus = $tmpTime->whereIn('status', 'return')->max('updatetime');
        $defStatus = $tmpTime->whereIn('status', 'default')->max('updatetime');
        $LastTransaction = collect([$addValue, $wait, $retStatus, $defStatus])->max();
        return [
            'output' => $output, //支出總表
            'input' => $input, //收入總表
            'TotalOutputMoney' => $TotalOutputMoney, //支出總金額
            'TotalInputMoney' => $TotalInputMoney, //收入總金額
            'OnTheTransaction' => $OnTheTransaction, //交易中總金額
            'TotalTransaction' => $totalCount, //總交易次數
            'LastTransaction' => $LastTransaction, //最後交易時間
            'beforeMoney' => $beforeMoney, //原本金額
            'afterMoney' => $afterMoney, //交易後金額
        ];
    }

    public function showBeInvite(Request $request) // 接案管理\等待中\被邀請

    {
        // {"status":""}
        // return $request->all();
        // return $request->all();
        $data = $request['status'];
        // return $publicdata;
        $user = Auth::guard('api')->user();
        // return $request['name'];
        // return $user;
        $caseInvite = DB::table('user_take_case')
            ->where('user_uid', $user->uid)
            ->where('status', 'accept')
            ->where('isopen', 1)
            ->where('func', 'InviteUser')
            ->get();

        // return ['tcase' => $caseInvite];

        $caseInvite = $caseInvite->map(function ($item, $keys) use ($user) {
            $item->casename = TheCases::where('uid', $item->case_uid)->first()->casename;
            $item->location = TheCases::where('uid', $item->case_uid)->first()->location;
            $item->startdate = TheCases::where('uid', $item->case_uid)->first()->startdate;
            $item->enddate = TheCases::where('uid', $item->case_uid)->first()->enddate;
            $item->needtime_startdate = TheCases::where('uid', $item->case_uid)->first()->needtime_startdate;
            $item->needtime_enddate = TheCases::where('uid', $item->case_uid)->first()->needtime_enddate;
            $item->totaldays = TheCases::where('uid', $item->case_uid)->first()->totaldays;
            $item->promisemoney = DB::table('case_position')->where('case_uid', $item->case_uid)->get();
            $item->releasedate = DB::table('cases')->where('uid', $item->case_uid)->first()->releasedate;
            $item->isPay = (DB::table('margin')->where('user_uid', $user->uid)->where('belong_table', 'user_take_case')->where('belong_uid', $item->uid)->count() > 0);
            $item->ownMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
            // $item->user_uid=DB::table('cases')->where('uid' , $item->case_uid)->first()->cmember;
            return $item;
        });

        return [
            'table' => 'user_take_case',
            'state' => 'success',
            'result' => $caseInvite,
        ];
    }

    public function showCacheCase(Request $request) //發案管理\未發佈

    {
        $page = 0;
        $pagenumber = 20;
        $today = Carbon::today();
        $user = Auth::guard('api')->user();
        $thecase = DB::table('cases')
        // ->with("TakeCase")
            ->where('isopen', 1)
            ->where('isIssue', 0)
            ->where('status', 'wait')
            ->where('belong_member', $user->uid)
        // ->where('releasedate', '>', $today)
            ->orderBy('createtime', 'desc');

        $countcase = clone $thecase;
        $thecase = $thecase->skip($page * $pagenumber)->take($pagenumber)->get();
        $countcase = $countcase->count();
        // return $thecase;
        $thecase = $thecase->map(function ($item, $keys) use ($user, $today) {
            $item->uid;
            $item->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $item->uid, 'isopen' => 1])->count() > 0);
            //>0顯示天數 否則顯示true(過期)
            $item->isOverdue = ((floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->position = (DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get() == null) ? 'null' : DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get();
            $item->category = explode(',', $item->category);
            if ($item->casename != null || $item->category != null || $item->casecontent != null || $item->attraction != null || $item->startdate != null || $item->enddate != null || $item->location != null || $item->releasedate != null || $item->attention != null || $item->connect != null || $item->phone != null || $item->search != null) {
                $item->isfull = '1';
            } else {
                $item->isfull = '0';
            }
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'ture',
            "count" => $countcase,
            "result" => $thecase,
        ];
    }

    public function showHistoryRecordCarryOut(Request $request)
    {
        $user = Auth::guard('api')->user();
        $case = DB::table('cases')->leftJoin('user_take_case', 'cases.uid', '=', 'user_take_case.case_uid')
            ->where('cases.status', '=', 'carryOut')
            ->where('user_take_case.user_uid', '=', $user->uid)
            ->where('user_take_case.status', '=', 'confirm')
            ->get();
        return [
            'table' => 'user_take_case',
            'status' => 'true',
            'result' => $case,
        ];

    }

    public function showHistoryRecordComplete(Request $request)
    {
        $user = Auth::guard('api')->user();
        $case = DB::table('cases')->leftJoin('user_take_case', 'cases.uid', '=', 'user_take_case.case_uid')
            ->where('cases.status', '=', 'close')
            ->where('user_take_case.user_uid', '=', $user->uid)
            ->get();
        return [
            'table' => 'user_take_case',
            'status' => 'true',
            'result' => $case,
        ];
    }

    // public function showCancelReason(Request $request)
    // {
    //     $user = Auth::guard('api')->user();
    //     $reason = DB::table('cancel_reason')->where('belong_member', $user->uid)->get();
    //     $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
    //     $reason = $reason->map(function ($item, $keys) {
    //         $item->takeCaseData = DB::table('user_take_case')->where('uid', $item->take_uid)->first();
    //         $item->resume = DB::table('resume')->where('user_uid' , $item->belpng_member)->where('isPreset' ,1)->first();
    //         $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
    //         return $item;
    //     });
    //     return [
    //         'table' => 'cancel_reason',
    //         'status' => 'true',
    //         'result' => $reason,
    //     ];
    // }

    public function hotCase(Request $request)
    {
        $case = DB::table('cases')->where('isopen', 1)->where('status', 'wait')->get();

        $case = $case->map(function ($item, $keys) {
            $takTMP = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get();
            $num = $takTMP->sum('num');
            $acnum = $takTMP->sum('acnum');
            $item->countTaker = DB::table('user_take_case')->where('case_uid', $item->uid)->where('isopen', 1)->get()->whereIn('status', ['accept', 'money', 'check', 'confirm'])->count();
            $item->isLast = $num - $acnum;
            return $item;
        });
        $case = $case->filter(function ($item, $keys) {
            return $item->isLast > 0;
        });
        $case = $case->sortByDesc('countTaker')->values()->take(3);
        //123
        return ['case' => $case];
    }

    public function getCase(Request $request)
    {
        //{"data":{"page":"","sort":""}}
        // $page = 0;
        // $pagenumber = 20;
        // $data = $request['data'];
        // $currentPage = $request['data']['page'];
        $user = Auth::guard('api')->user();
        $takeCase = DB::table('user_take_case')->where('user_uid', $user->uid)->where('isopen', 1)->orderBy('createtime', 'desc')->get();

        $takeCase = $takeCase->map(function ($item, $keys) {
            $CPtmp = DB::table('case_position')->where('uid', $item->position_uid)->first();
            if($CPtmp == null)return;
            $item->caseData = DB::table('cases')->where('uid', $item->case_uid)->first();
            $item->PromiseMoney = $CPtmp->promisemoney;
            $item->position = $CPtmp->name;
            $item->salary = $CPtmp->salary;
            if ($item->status == 'discuss') {
                $item->CancelReason = DB::table('cancel_reason')->where('take_uid', $item->uid)->where('isopen', 1)->first();
            }
            if ($item->func == 'InviteUser') {
                $item->status = 'invite-' . $item->status;
            }

            return $item;
        });
        $confirm= $takeCase->whereIn('status' ,[ 'confirm' , 'invite-confirm']);
        $wait = $takeCase->whereIn('status' ,['accept' , 'money' , 'check' ,'invite-accept' ,'invite-money','invite-check']);
        $default =$takeCase->whereIn('status' , ['takerDefault' , 'caseDefault' ,'discuss', 'invite-discuss' , 'invite-takerDefault' ,'invite-caseDefault']);
        $complete =$takeCase->whereIn('status' , ['complete' , 'invite-complete']);
        return [
            'confirm' => $confirm,
            'wait' => $wait,
            'default' => $default,
            'complete' => $complete,
        ];

    }

}
