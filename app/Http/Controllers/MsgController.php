<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PublicCtr;
use App\member;
use Auth;
use DB;
use Illuminate\Http\Request;

class MsgController extends Controller
{
    use PublicCtr\Support;
    public function msg(Request $request) //傳給發案方

    {
        //{"data":{"case_uid":"","content":""}}
        $user = Auth::guard('api')->user();
        $msg = $request['data'];
        $casedata = DB::table('cases')->where('uid', $msg['case_uid'])->first(); //case資料
        // return [$casedata];
        $sendmsg = $this->SupportInsert('message', [
            'from_user_uid' => $user->uid,
            'to_user_uid' => $casedata->belong_member,
            'content' => $msg['content'],
        ]);

        return $sendmsg;
    }

    public function msgPerson(Request $request)

    {
        //{"data":{"user_uid":"","content":""}}
        $user = Auth::guard('api')->user();
        $data = $request['data'];
        // return $data;
        
        $sendmsg = $this->SupportInsert('message',[
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => $data['content']
        ]);
        return $sendmsg;

    }

    public function showMsg(Request $request)
    {
        $user = Auth::guard('api')->user();
        $Msg = DB::table('message')->where('to_user_uid' , $user->uid)->orwhere('from_user_uid' , $user->uid)->get();
        $allMsg = $Msg->map(function($item,$keys)use($user){
            $item->from_user = DB::table('members')->where('user_uid' , $item->from_user_uid)->select('user_uid' , 'username')->first();
            $item->to_user = DB::table('members')->where('user_uid' , $item->to_user_uid)->select('user_uid' , 'username')->first();
            return $item;
        });
        // return $allMsg;
        // ->groupBy('from_user');
        // return $allMsg;
        // if($allMsg)
        $toperson = $allMsg->keyBy("to_user_uid")->keys();
        $fromuser = $allMsg->keyBy("from_user_uid")->keys();
        $personlist=collect($toperson)->unique();
        $personlist= $personlist->merge($fromuser)->filter(function ($item,$keys)use($user){
            return $item!=$user->uid;
        })->values();
        // return $personlist;
        $Msg = DB::table("members")->whereIn("user_uid",$personlist)->get();
        $Msg = $Msg->map(function($item,$keys)use($user){
            $item->isRead=(DB::table('message')->where('from_user_uid' , $item->user_uid)->where('to_user_uid' , $user->uid)->where('isRead' , 0)->get()->count())>0 ? (DB::table('message')->where('to_user_uid' , $user->uid)->where('isRead' , 0)->get()->count()) : 'false';
            return $item;
        });
        return [
            'table' => 'message',
            'status' => 'true',
            'result' => $Msg
        ];
    }

    public function showDetailMsg(Request $request)
    {
        // return 'aa';
        $data = $request['user_uid'];
        $user = Auth::guard('api')->user();
        $msg = DB::table('message')
        ->orwhere(function($query)use($user,$data){
            $query->where('from_user_uid' , $user->uid)
            ->where('to_user_uid' , $data);
        })
        ->orwhere(function($query)use($user,$data){
            $query->where('to_user_uid' , $user->uid)
            ->where('from_user_uid' , $data);
        })
        ->get();
        $msgOtherSide = DB::table('message')->where('to_user_uid' , $user->uid)->where('from_user_uid' , $data)->get();
        
        $isRead = $msgOtherSide->map(function($item,$keys){
            $this->SupportUpdate('message',[
                'isRead' => 1
            ],['uid' => $item->uid]);
        });
        
        // $isRead = $this->SupportUpdate('message',[
        //     'isRead' => 1
        // ],[
        //     'to_user_uid' => $user->uid,
        //     'from_user_uid' => $data
        // ]);

        $msg = $msg->map(function($item,$keys){
            $item->from_user_name = member::where('user_uid' , $item->from_user_uid)->first()->username;
            $item->to_user_name = member::where('user_uid' , $item->to_user_uid)->first()->username;
            return $item;
        });

        return $msg;
    }

    public function countMsg(Request $request)
    {
        $user = Auth::guard('api')->user();
        $count = DB::table('message')->where('to_user_uid' , $user->uid)->where('isRead' , 0)->get()->count();
        return $count;
    }
}
