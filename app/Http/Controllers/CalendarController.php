<?php

namespace App\Http\Controllers;
date_default_timezone_set("Asia/Taipei");

use Illuminate\Http\Request;
use App\Http\Controllers\PublicCtr;
use Auth;
use DB;
use App\User;
use App\UserTakeCase;
use App\TheCases;
use Carbon\Carbon;

class CalendarController extends Controller
{
    use PublicCtr\Support;
    public function showcalendar(Request $request) //顯示行事曆
    {
        $user = Auth::guard('api')->user();
        $calendar = DB::table('calendar')->where('user_uid' , $user->uid)->where('isopen' , 1)->get();
        $calendar = $calendar->map(function($item,$keys){
            $item->level = DB::table('template')->where('func' , 'ForLevel')->where('orders' , $item->level)->first()->value;
            return $item;
        });
        $takeCase = DB::table('user_take_case')->where('user_uid' , $user->uid)->where('status' , 'confirm')->where('isopen' , 1)->get();
        $takeCase = $takeCase->map(function($item,$keys){
            $item->level = 1;
            $item->alert = 1;
            $item->caseData = DB::table('cases')->where('uid' , $item->case_uid)->where('isopen' , 1)->first();
            $item->caseData->casePosition = DB::table('case_position')->where('uid' , $item->position_uid)->first();
            $item->allDay = 'true';
            // $item->startdate = Carbon::parse($item->startdate)->timezone('Asia/Taipei');
            return $item;
        });
        $waitting = DB::table('user_take_case')->where('user_uid' , $user->uid)->whereIn('status' , ['accept','money'])->where('isopen' , 1)->get();
        $waitting = $waitting->map(function($item,$keys){
            $item->alert = 1;
            $item->caseData = DB::table('cases')->where('uid' , $item->case_uid)->where('isopen' , 1)->first();
            $item->caseData->casePosition = DB::table('case_position')->where('uid' , $item->position_uid)->first();
            $item->allDay = 'true';
            return $item;
        });

        return [
            'calendar' => $calendar,
            'takeCase' => $takeCase,
            'waitting' => $waitting
        ];

        // return [
        //     'table' => 'calendar',
        //     'status' => 'true',
        //     'result' => $calendar,
        // ];
    }

    public function addSchedule(Request $request) //新增
    {
        // return $request->all();
        $data = $request['data'];
        $data['startdate'] = Carbon::parse($data['startdate'])->timezone('Asia/Taipei');
        $data['enddate'] = Carbon::parse($data['enddate'])->timezone('Asia/Taipei');
        // return $data;
        $user = Auth::guard('api')->user();
        // return $data['enddate'];
        $TakeCaseCalendar = UserTakeCase::with(['TheCase' => function($query)use($data , $user){
            $query->where(function($query)use($data){
                $query->whereBetween('startdate' , [$data['startdate'] , $data['enddate']])
                ->whereBetween('enddate' , [ $data['startdate'] , $data['enddate'] ]);
            })
            ->orwhere(function($query)use($data){
                $query->where('startdate' , '<' , $data['startdate'])
                ->WhereBetween('enddate' , [ $data['startdate'] , $data['enddate']]);
            })
            ->orwhere(function($query)use($data){
                $query->WhereBetween('startdate' , [ $data['startdate'] , $data['enddate']])
                ->where('enddate' , '>' , $data['enddate']);
            })
            ->orwhere(function($query)use($data){
                $query->where('startdate' , '<' , $data['startdate'])
                ->where('enddate' , '>' , $data['enddate']);
            });
        }])->whereIn('user_uid' , [$user->uid])->get();

        // return $TakeCaseCalendar;
        $isTrueTake = $TakeCaseCalendar->filter(function ($item){return $item->TheCase;})->count()>0;
        $data = collect($data);
        $data->put('user_uid' , $user->uid);
        $schedule = $this->SupportInsert('calendar' , $data);

        if(!$isTrueTake){
            return [
                'msg' => 'double with case',
                $schedule
            ];
        }else{
            return [
                $schedule,
            ];
        }
        
        
        
        
    }

    public function editSchedule(Request $request) //編輯
    {
        // {"data"{"":""... , "uid":""}}
        // return $request->all();
        $data = $request['data'];
        $data['startdate'] = Carbon::parse($data['startdate'])->timezone('Asia/Taipei');
        $data['enddate'] = Carbon::parse($data['enddate'])->timezone('Asia/Taipei');
        $user = Auth::guard('api')->user();
        $select = $data['uid'];
        $data = collect($data)->forget('uid');
        
        $schedule = $this->SupportUpdate('calendar' , $data , ['uid' => $select]);
        return $schedule;

    }

    public function removeSchedule(Request $request) //刪除
    {
        // return $request->all();
        $data = $request['uid'];
        $user = Auth::guard('api')->user();
        $schedule = $this->SupportUpdate('calendar',[
            'isopen' => 0
        ],['uid' => $data]);

        return $schedule;
    }

    public function showDetailSchedule(Request $request)
    {
        $data = $request['uid'];
        $user = Auth::guard('api')->user();
        
    }
    
}
