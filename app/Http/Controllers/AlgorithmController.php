<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class AlgorithmController extends Controller
{
    public function recommendCase(Request $request)
    {
        // {"data":{"startdate":"","enddate":"","gap":""}}
        // $page = $request['page'];
        // return $request->all();
        $data = $request['data'];
        $page = '0,5';
        $user = Auth::guard('api')->user();
        // $case = TheCases::where('isopen', 1)->get();
        $today = Carbon::today();
        $member = DB::table('members')
            ->where('members.user_uid', $user->uid)
            ->leftjoin('user_language_ability', 'members.user_uid', '=', 'user_language_ability.user_uid')
            ->first();
        $takeLocation = DB::table('user_take_case')->where('user_uid', $user->uid)->where('status', 'complete')->where('isopen', 1)->get();
        $takeLocation = $takeLocation->map(function ($item, $keys) {
            $item = DB::table('cases')->where('uid', $item->case_uid)->first()->location;
            return $item;
        });
        $takeLocation = $takeLocation->implode(","); //型態一致 變成字串
        // return $member->location;
        $memberlocation = collect(explode(",", $member->location))->map(function ($item, $keys) use ($today, $data) {
            $sql = "select count(cases.id)*2 as point , cases.uid from cases
            left join case_position on cases.uid = case_position.case_uid
            where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
            FIND_IN_SET('{$item}', cases.location) and (case_position.num - case_position.acnum) > 0 and cases.status = 'wait' and cases.isIssue = 1 and cases.isopen = 1 and cases.releasedate > '{$today}' group by uid";
            return $sql;
        });
        $memberSkill = collect(explode(",", $member->skill))->map(function ($item, $keys) use ($today, $data) {
            $sql = "select count(cases.id)*3 as point , cases.uid from cases
            left join case_position on cases.uid = case_position.case_uid
            where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
            FIND_IN_SET('{$item}', case_position.name) and (case_position.num - case_position.acnum) > 0 and cases.status = 'wait' and cases.isIssue = 1 and cases.isopen = 1 and cases.releasedate > '{$today}' group by uid";
            return $sql;
        });
        $memberInerest = collect(explode(",", $member->interest))->map(function ($item, $keys) use ($today, $data) {
            $sql = "select count(cases.id)*1 as point , cases.uid from cases
            left join case_position on cases.uid = case_position.case_uid
            where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
            FIND_IN_SET('{$item}', cases.category) and (case_position.num - case_position.acnum) > 0 and cases.status = 'wait' and cases.isIssue = 1 and cases.isopen = 1 and cases.releasedate > '{$today}' group by uid";
            return $sql;
        });
        $memberAddress = "select count(cases.id)*1 as point , cases.uid from cases
        left join case_position on cases.uid = case_position.case_uid
        where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
        location = '{$member->address}' and (case_position.num - case_position.acnum) > 0 and cases.status = 'wait' and cases.isIssue = 1 and cases.isopen = 1 and cases.releasedate > '{$today}' group by uid";
        // $memberLicence=collect(explode(",",$member->licence))->map(function($item,$keys){
        //     $sql="select count(cases.id) as point , cases.uid from cases where FIND_IN_SET('{$item}', cases.location) group by uid ";
        //     return $sql;
        // });
        $memberLanguage = collect(explode(",", $member->language))->map(function ($item, $keys) use ($today, $data) {
            $sql = "select count(cases.id)*2 as point , cases.uid from cases
            left join case_position on cases.uid = case_position.case_uid
            where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
            FIND_IN_SET('{$item}', cases.location) and (case_position.num - case_position.acnum) > 0 and cases.status = 'wait' and cases.isIssue = 1 and cases.isopen = 1 and cases.releasedate > '{$today}' group by uid";
            return $sql;
        });
        $nearByLocation = collect(explode(",", $takeLocation))->map(function ($item, $keys) use ($today, $data) {
            $sql = "select count(cases.id)*0.5 as point , cases.uid from cases
            left join case_position on cases.uid = case_position.case_uid
            where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
            FIND_IN_SET('{$item}', cases.location) and (case_position.num - case_position.acnum) > 0 and cases.status = 'wait' and cases.isIssue = 1 and cases.isopen = 1 and cases.releasedate > '{$today}' group by uid";
            return $sql;
        });

        // return $nearByLocation->implode(" union all ");

        $memberCooperation = "select count(cases.id)*3 as point , cases.uid from cases
        left join case_position on cases.uid = case_position.case_uid
        left join user_take_case on cases.uid = user_take_case.case_uid
        where cases.startdate between ('{$data['startdate']}' and '{$data['enddate']}') and cases.enddate between ('{$data['startdate']}' and '{$data['enddate']}') and
        (case_position.num - case_position.acnum) > 0 and cases.belong_member = user_take_case.case_user_uid and user_take_case.status = 'complete' and user_take_case.user_uid = '{$user->uid}' and cases.status = 'close' group By uid";
        // return $memberCooperation;
        $unionAll = collect();
        $unionAll->push($memberlocation->implode(" union all "));
        $unionAll->push($memberLanguage->implode(" union all "));
        $unionAll->push($memberSkill->implode(" union all "));
        $unionAll->push($memberInerest->implode(" union all "));
        $unionAll->push($nearByLocation->implode(" union all "));
        $unionAll->push($memberCooperation);
        $unionAll->push($memberAddress);
        $case = DB::select("
        select sum(`point`) as TotalPoint , uid from
        ({$unionAll->implode(" union all ")})x group By uid order By TotalPoint desc limit 0,5
        ");

        $caseDetail = collect($case)->map(function ($item, $keys) {
            $item->caseData = DB::table('cases')->where('uid', $item->uid)->first();
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->select("name", "num", "acnum", "language", "ability", "promisemoney", "salary", "notice")->get();
            $item->position = $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                return $item;
            });
            $item->companyData = DB::table('companys')->where('belong_member', $item->caseData->belong_member)->first();
            return $item;
        });
        // return $caseDetail;
        $caseDetail = $caseDetail->filter(function ($item, $keys) {
            return DB::table('calendar')
                ->where(function ($query) use ($item) {
                    $query->where('startdate', '>', $item->caseData->startdate)
                        ->where('enddate', '>', $item->caseData->enddate);
                })
                ->orwhere(function ($query) use ($item) {
                    $query->where('startdate', '<', $item->caseData->startdate)
                        ->where('enddate', '<', $item->caseData->enddate);
                });
        });
        // return 'aa';

        // $scheduleArray=collect();
        // // $first = $caseDetail->where('startdate',$data['startdate']);
        // $scheduleArray = $caseDetail->each(function($item,$keys)use($data){
        //     if($item->startdate == $data['startdate']){
                
        //     }else{
        //         $data['startdate'] = $data['startdate']->
        //     }
        // });

        return $scheduleArray;

        return [
            'table' => 'cases',
            'status' => 'true',
            'result' => $caseDetail,
        ];
    }

    public function recommendPerson(Request $request)
    {
        $currentPage = $request['page'];
        $page = 0;
        $pagenumber = 20;
        $user = Auth::guard('api')->user();
        $cases = DB::table('cases')->where('status', 'wait')->where('belong_member', $user->uid)->orderBy('createtime', 'desc');
        $countcase = clone $cases;
        $cases = $cases->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countcase = $countcase->count();
        $cases = $cases->map(function ($item, $keys) {
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get();
            return $item;
        });
        return $cases;
    }

    public function detailRecommendPerson(Request $request)
    {
        //{"data":{"case_uid":"","position_uid":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $case = DB::table('cases')->where('uid', $data['case_uid'])->first();
        $caseSideLocation = $case->location;

        $unionAll = collect();
            $position = DB::table('case_position')->where('uid', $data['position_uid'])->first();
            
            $caseLanguage = "select count(members.id)*2 as point ,members.uid from members
                left join user_language_ability on members.user_uid = user_language_ability.user_uid
                where '{$position->language}' = user_language_ability.language and '{$position->ability}' = user_language_ability.ability and members.isMember = 1 group by members.uid";
            
            $casesCategory = collect(explode(",", $case->category))->map(function ($item, $keys) {
                $sql = "select count(members.id)*2 as point ,members.uid from members
                left join user_language_ability on members.user_uid = user_language_ability.user_uid
                where find_in_set('{$item}',members.skill) and members.isMember = 1 group By members.uid";
                return $sql;
            });
            $casesInerest = collect(explode(",", $case->location))->map(function ($item, $keys) {
                $sql = "select count(members.id)*1 as point ,members.uid from members
                left join user_language_ability on members.user_uid = user_language_ability.user_uid
                where find_in_set('{$item}',members.interest) and members.isMember = 1 group By members.uid";
                return $sql;
            });
            $caseLocation = collect(explode(",", $case->location))->map(function ($item, $keys) {
                $sql = "select count(members.id)*2 as point ,members.uid from members
                left join user_language_ability on members.user_uid = user_language_ability.user_uid
                where find_in_set('{$item}',members.location) and members.isMember = 1 group By members.uid";
                return $sql;
            });
            $nearByLocation = collect(explode(",", $caseSideLocation))->map(function ($item, $keys) {
                $sql = "select count(members.id)*0.5 as point ,members.uid from members
                left join user_language_ability on members.user_uid = user_language_ability.user_uid
                left join user_take_case on members.user_uid = user_take_case.user_uid
                left join cases on user_take_case.case_uid = cases.uid
                where find_in_set('{$item}',cases.location) and members.isMember = 1 group By members.uid";
                return $sql;
            });
            // return $nearByLocation;
            $caseAddress = "select count(members.id)*1 as point ,members.uid from members
            left join user_language_ability on members.user_uid = user_language_ability.user_uid
            where find_in_set('{$case->location}',members.address) and members.isMember = 1 group by members.uid";

            $caseTakeTimes = "select count(members.id)*3 as point , members.uid from members
            left join user_take_case on members.user_uid = user_take_case.user_uid
            where user_take_case.case_user_uid = '{$case->belong_member}' and user_take_case.status = 'complete' group By members.uid ";

            $unionAll->push($caseLanguage);
            $unionAll->push($casesCategory->implode(" union all "));
            $unionAll->push($caseLocation->implode(" union all "));
            $unionAll->push($casesInerest->implode(" union all "));
            $unionAll->push($nearByLocation->implode(" union all "));
            // ($caseLanguage->count()>0)?$unionAll->push($caseLanguage->implode(" union all ")):"";

            $unionAll->push($caseTakeTimes);
            $pnum = $position->num * 3;
            $person = DB::select("
            select sum(`point`) as TotalPoint , uid from
            ({$unionAll->implode(" union all ")} )x group By uid order By TotalPoint desc limit 0,$pnum
            ");

            $person = collect($person)->map(function ($item, $keys) use ($avatar,$data) {
                $memberDataTMP = DB::table('members')->where('uid', $item->uid)->select('address' , 'area' , 'education' , 'email' , 'gender','interest','location','mobilephone','phone','profile','skill','uid','username','user_uid')->first();
                $caseTMP = DB::table('cases')->where('uid' , $data['case_uid'])->first();
                $positionTMP = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
                $inviteTMP = DB::table('user_take_case')->where('case_uid' , $data['case_uid'])->where('position_uid' , $data['position_uid'])->where('user_uid',$memberDataTMP->user_uid)->where('func' ,'InviteUser')->where('isopen' , 1)->first();
                $takeTMP = DB::table('user_take_case')->where('case_uid' , $data['case_uid'])->where('position_uid' , $data['position_uid'])->where('user_uid',$memberDataTMP->user_uid)->whereIn('status',['accept','money','confirm'])->where('func' ,'UserTake')->where('isopen' , 1)->first();
                $item->casename = $caseTMP->casename;
                $item->case_uid = $caseTMP->uid;
                $item->position_uid = $positionTMP->uid;
                $item->position = $positionTMP->name;
                $item->username = $memberDataTMP->username;
                $item->education = $memberDataTMP->education;
                $item->email = $memberDataTMP->email;
                $item->user_uid = $memberDataTMP->user_uid;
                $item->uid = $memberDataTMP->uid;
                $item->gender = $memberDataTMP->gender;
                $item->skill = $memberDataTMP->skill;
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->first()->path : $avatar;
                $language = DB::table('user_language_ability')->where('user_uid', $item->user_uid)->where('isopen', 1)->select("language")->get();
                $language = $language->map(function ($item, $keys) {
                    $item = $item->language;
                    return $item;
                });
                // $item->language = implode(",");
                $item->language = $language;
                $item->licence = DB::table('user_license')->where('cmember', $item->user_uid)->select('chocategory' , 'cholicens','uid')->where('isopen', 1)->get();
                if($inviteTMP != null){
                    if($inviteTMP->status == 'refuse'){
                        $item->isInvite = 'refuse';
                    }elseif ($inviteTMP->status == 'accept' ) {
                        $item->isInvite = 'already invite and wait accept';
                    }elseif ($inviteTMP->status == 'confirm') {
                        $item->isInvite = 'already match';
                    }elseif ($inviteTMP->status =='check') {
                        $item->isInvite = 'already accept and wait check';
                    }elseif ($inviteTMP->status == 'money') {
                        $item->isInvite = 'already check and wait money';
                    }
                }elseif($takeTMP!= null) {
                    $item->isInvite ='no invite but take';
                }else {
                    $item->isInvite = 'nothing';
                }
                return $item;
            });
            $person = $person->filter(function ($item, $keys) use ($case) {
                return (
                    ($case->startdate > 'calendar.startdate') && ($case->enddate > 'calendar.enddate') ||
                    ($case->startdate < 'calendar.startdate') && ($case->enddate < 'calendar.enddate') ||
                    ('calecdar' == null)
                );
            });
            if ($person->isEmpty()) {
                return ['person' => "null"];
            } else {
                return ['person' => $person];
            }

    }

}
