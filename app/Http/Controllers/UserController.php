<?php

namespace App\Http\Controllers;

use App\company;
use App\Http\Controllers\PublicCtr;
use App\member;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Mail;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Storage;

// use Request;

class UserController extends Controller
{
    use PublicCtr\Support;

    // public function testmail(Request $request){
    //     $user = Auth::guard('api')->user();
    //     $data = [
    //         'name' => 'tony',
    //         'email' => 'tony860725@gmail.com'
    //     ];
    //     Mail::to('tony860725@gmail.com')->send(new VerifyMail($data));
    //     // return [
    //     //     'status' => 'true',
    //     //     'result' => $sendMail
    //     // ];
    // }

    public function PwdReset(Request $request){
        $data = $request['pwd'];
        $data = hash::make($data);
        $user = Auth::guard('api')->user();
        $pwdResrt = $this->SupportUpdate('users',[
            'password' => $data
        ],['uid' => $user->uid]);
        // return [$pwdResrt['state']];
        if($pwdResrt['state']){
            return $pwdResrt;
        }else{
            return ['msg' => 'change fail'];
        }
    }

    public function showprofile() //顯示登入者資料

    {
        return Auth::guard("api")->user();
        $user = User::find($id);
        if (!$user) {
            return [
                'table' => 'user',
                'state' => 'fales',
                'result' => 'user not found',
            ];
        }
        return [
            'table' => 'user',
            'state' => 'ture',
            'result' => $user,
        ];
    }

    public function showmember() //顯示個人資料

    {
        $user = Auth::guard('api')->user();
        $member = member::where('email', $user->email)->first();
        $member->language = DB::table('user_language_ability')->where('user_uid' , $user->uid)->where('isopen', 1)->get();
        $member->licence = DB::table('user_license')->where('user_uid' , $user->uid)->where('isopen' , 1)->get();
        return [
            'table' => 'memberssss',
            'state' => 'ture',
            'result' => $member,
        ];
    }

    public function openpofile(Request $request) //共用資料設定

    {
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $userinfo = member::where('email', $user->email)->first();
        if ($userinfo) {
            $member = $this->SupportUpdate('members', $data, ['email' => $user->email]);
        };
        return $member;
    }

    public function setprofile(Request $request) //人才資料填寫

    {
        $newData = $request['data'];
        $language = $newData['language'];
        $newData['birth'] = Carbon::parse($newData['birth'])->timezone('Asia/Taipei');
        $user = Auth::guard('api')->user();
        $sort = DB::table('users')->where('uid', $user->uid)->first();
        $newData['location'] = collect($newData['location'])->map(function ($item, $keys) {
            $item = $item['city_area'];
            return $item;
        });

        
        $language = collect($language)->map(function ($item, $keys) use ($user) {
            $language = $this->SupportInsert('user_language_ability', [
                'ability' => $item['ability'],
                'language' => $item['language'],
                'user_uid' => $user->uid,
            ]);
            return $language;
        });

        $newData = collect($newData)->forget('language');
        $newData->put('isMember', 1);
        // return $newData;
        $newData['location'] = $newData['location']->implode(',');
        // return $newData['location'];

        $userinfo = member::where("email", $user->email)->first();
        if ($userinfo) {
            $memberesult = $this->SupportUpdate('members', $newData, ["email" => $user->email]);
            if ($sort->sort == null) {
                $useresult = $this->SupportUpdate('users', ['sort' => 'person'], ["uid" => $user->uid]);
            } else {
                $useresult = $this->SupportUpdate('users', ['sort' => 'person,company'], ["uid" => $user->uid]);
            }
        };
        return [
            $memberesult, $useresult, $language,
        ];
    }

    public function companyprofile(Request $request) //公司資料填寫

    {
        $newData = $request['data'];
        $user = Auth::guard("api")->user();
        $sort = DB::table('users')->where('uid', $user->uid)->first();
        $userinfo = member::where("email", $user->email)->first();
        if ($userinfo) {
            $companys = $this->SupportInsert('companys', $newData);
            if ($sort->sort == null) {
                $useresult = $this->SupportUpdate('users', ['sort' => 'company'], ["uid" => $user->uid]);
            } else {
                $useresult = $this->SupportUpdate('users', ['sort' => 'person,company'], ["uid" => $user->uid]);
            }
        };

        // $useresult = member::where("email" , $user->username)->first();
        return [
            $companys, $useresult,
        ];
    }
    public function changepwd(Request $request) //修改密碼

    {
        $pwd = $request['data'];
        $user = Auth::guard("api")->user();
        $checkpwd = Hash::check($pwd['newpwd'], $user->password);
        // $userinfo=member::where("email",$user->email)->first();

        if (Hash::check($pwd['oldpwd'], $user->password)) {
            if (!$checkpwd) {
                $user = $this->SupportUpdate('users', [
                    'password' => Hash::make($pwd['newpwd']),
                ], ['email' => $user->email]);
            } else {
                return [
                    'table' => 'user',
                    'state' => 'false',
                    'result' => 'The password is double.',
                ];
            };
        } else {
            return [
                'table' => 'user',
                'state' => 'false',
                'result' => 'oldpwd mistake',
            ];
        }
        return $user;

    }

    public function logout() //登出

    {
        Auth::logout();
    }

    public function updateprofile(Request $request) //編輯個人資料

    {
        $newData = $request['data'];
        $user = Auth::guard("api")->user();
        $licence = $newData['license'][0];
        $language = $newData['language'][0];
        $originalLicence = DB::table('user_license')->where('cmember' , $user->uid)->where('isopen' , 1)->get();
        $originalLanguage = DB::table('user_language_ability')->where('cmember' , $user->uid)->where('isopen' , 1)->get();
        if($originalLicence->isEmpty()){
            $AddLicence = collect($licence)->map(function($item,$keys)use($user){
                $this->SupportInsert('user_license',[
                    'user_uid' => $user->uid,
                    'chocategory' => $item['chocategory'],
                    'cholicens' => $item['cholicens'],
                ]);
            });
        }else {
            collect($licence)->map(function($item,$keys)use($user){
                $orglic = DB::table('user_license')->where('uid' , $item->uid)->get();
                if($orglic->isEmpty()){
                    $this->SupportInsert('user_license',[
                        'user_uid' => $user->uid,
                        'ability' => $item['ability'],
                        'language' => $item['language'],
                    ]);
                }else {
                    $this->SupportUpdate('user_license',[
                        'user_uid' => $user->uid,
                        'ability' => $item['ability'],
                        'language' => $item['language'],
                    ],['uid' => $item->uid]);
                }
                
            });
            // $AddLicence = collect($licence)->map(function($item,$keys)use($user){
            //     $this->SupportInsert('user_license',[
            //         'user_uid' => $user->uid,
            //         'chocategory' => $item['chocategory'],
            //         'cholicens' => $item['cholicens'],
            //     ]);
            // });
        }

        if($originalLanguage->isEmpty()){
            $AddLanguage = collect($language)->map(function($item,$keys)use($user){
                $this->SupportInsert('user_language_ability',[
                    'user_uid' => $user->uid,
                    'ability' => $item['ability'],
                    'language' => $item['language'],
                ]);
            });
        }else {
            collect($language)->map(function($item,$keys)use($user){
                $orglan = DB::table('user_language_ability')->where('uid' , $item->uid)->get();
                if($orglan->isEmpty()){
                    $this->SupportInsert('user_language_ability',[
                        'user_uid' => $user->uid,
                        'ability' => $item['ability'],
                        'language' => $item['language'],
                    ]);
                }else {
                    $this->SupportUpdate('user_language_ability',[
                        'user_uid' => $user->uid,
                        'ability' => $item['ability'],
                        'language' => $item['language'],
                    ],['uid' => $item->uid]);
                }
                
            });
            // $AddLanguage = collect($language)->map(function($item,$keys)use($user){
            //     $this->SupportInsert('user_language_ability',[
            //         'user_uid' => $user->uid,
            //         'ability' => $item['ability'],
            //         'language' => $item['language'],
            //     ]);
            // });
        }

        $MemberData = $this->SupportUpdate('members',[
            'username' => $newData['username'],
            'skill' => $newData['skill'],
            'profile' => $newData['profile'],
            'mobilephone' => $newData['mobilephone'],
            'education' => $newData['education']
        ],['user_uid' => $user->uid]);
        if($newData['username'] != $user->username){
            $UsersData = $this->SupportUpdate('users' , ['username' => $newData['username']],['uid' => $user->uid]);
        }
        
    }

    public function updateCompanyProfile(Request $request) //編輯公司資料

    {
        $newData = $request['data'];
        $user = Auth::guard("api")->user();
        $userinfo = company::where("belong_member", $user->uid)->first();
        if ($userinfo) {
            $useresult = $this->SupportUpdate('companys', $newData, ["belong_member" => $user->uid]);
            // $useresult = $this->SupportUpdate('users', [
            //     'username' => $newData['username'],
            // ], ['email' => $user->email]);
        };
        // $useresult = member::where("email" , $user->username)->first();
        return $useresult;
    }

    public function showCompany(Request $request)
    {
        $user = Auth::guard('api')->user();
        $companys = DB::table('companys')->where('belong_member', $user->uid)->first();
        return [
            'table' => 'companys',
            'status' => 'true',
            'result' => $companys,
        ];
    }

    public function showperson(Request $request) //顯示人才(findpeople)

    {
        // return $request->all();
        $page = 0;
        $pagenumber = 20;
        $user = Auth::guard('api')->user();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $currentPage = $request['page'];
        // $currentPage = Request::get('page' , 1);
        // return $currentPage;
        $person = DB::table('members')
        // ->where('public', 1)
            ->where('isMember', 1)
            ->where('user_uid', '!=', $user->uid)
            ->orderBy('createtime', 'desc');
        // ->get();

        $countPerson = clone $person;
        $person = $person->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countPerson = $countPerson->count();
        $person = $person->map(function ($item, $keys) use ($user, $avatar) {
            $item->isSave = (DB::table('case_save_user')->where(['user_uid' => $item->user_uid, 'case_user_uid' => $user->uid, 'isopen' => 1])->count() > 0);
            $item->PresetResume = DB::table('resume')->where('cmember', $item->user_uid)->where('isPreset', 1)->first();
            $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
            $item->licence = DB::table('user_license')->where('cmember', $item->user_uid)->where('isopen', 1)->get();
            return $item;
        });

        return [
            'table' => 'members',
            'state' => 'true',
            'result' => $person,
            'count' => $countPerson,
        ];
    }

    public function unLoginShowPerson(Request $request)
    {
        $page = 0;
        $pagenumber = 20;
        $currentPage = $request['page'];
        $person = DB::table('members')
            ->where('public', 1)
            ->where('isMember', 1)
            ->orderBy('createtime', 'desc');
        // ->get();

        $countPerson = clone $person;
        $person = $person->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countPerson = $countPerson->count();

        $person = $person->map(function ($item, $keys) {
            $item->PresetResume = DB::table('resume')->where('cmember', $item->user_uid)->where('isPreset', 1)->first();
            return $item;
        });

        return [
            'table' => 'members',
            'state' => 'true',
            'result' => $person,
            'count' => $countPerson,
        ];
    }

    public function authority(Request $request) //權限設定

    {
        $auth = $request['auth'];
        $user = Auth::guard("api")->user();
        $userinfo = member::where("email", $user->email)->first();
        $t = collect(['person', 'company']);

        if ($userinfo) {

            if ($auth['sort'] == 0) {
                $useresult = $this->SupportUpdate('users', ['sort' => $t->implode(",")], ["email" => $user->email]);
            } elseif ($auth['sort'] == 1) {
                $useresult = $this->SupportUpdate('users', ['sort' => $t->implode(",")], ["email" => $user->email]);
            };
        };
        return $useresult;
    }

    public function savePerson(Request $request) //收藏人才

    {
        $saveUserUid = $request['user_uid'];
        $user = Auth::guard('api')->user();
        $isSave = DB::table('case_save_user')->where('case_user_uid', $user->uid)->where('user_uid', $saveUserUid)->get();

        // return $isSave[0]->isopen;

        if ($isSave->isEmpty()) {
            $savePerson = $this->SupportInsert('case_save_user', [
                'user_uid' => $saveUserUid,
                'case_user_uid' => $user->uid,
            ]);
            return $savePerson;
        } elseif ($isSave[0]->isopen == 0) {
            $savePerson = $this->SupportUpdate('case_save_user', [
                'isopen' => 1,
            ], [
                'case_user_uid' => $user->uid,
                'user_uid' => $saveUserUid,
            ]);
            return $savePerson;
        } elseif ($isSave[0]->isopen == 1) {
            $savePerson = $this->SupportUpdate('case_save_user', [
                'isopen' => 0,
            ], [
                'case_user_uid' => $user->uid,
                'user_uid' => $saveUserUid,
            ]);
            return $savePerson;
        }

    }

    public function addValue(Request $request) //加值（目前跑流程用、之後在更改）

    {
        //{"value":""}
        $data = $request['value'];
        $user = Auth::guard('api')->user();
        $money = DB::table('users')->where('uid', $user->uid)->first()->money;
        $addValue = $this->SupportUpdate('users', [
            'money' => $money + $data,
        ], [
            'uid' => $user->uid,
        ]);
        $margin = $this->SupportInsert('margin', [
            'belong_table' => 'users',
            'belong_uid' => $user->uid,
            'user_uid' => $user->uid,
            'money' => $data,
            'status' => 'addValue',
            'func' => 'addValue',
        ]);

        return [
            $addValue,
            $margin,
        ];
    }
    public function addCard(Request $request)
    {
        //{"data":{"bank":"","location":"","branch":"","name":"","account":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $data = collect($data);
        $data->put('user_uid', $user->uid);
        $addCard = $this->SupportInsert('user_card', $data);

        return $addCard;
    }

    public function applicationRecord(Request $request) //應徵紀錄

    {
        $user = Auth::guard('api')->user();
        $record = DB::table('user_take_case')
            ->where('user_uid', $user->uid)
            ->where('isopen', 1)
        // ->select('uid', 'user_uid', 'case_uid', 'case_user_uid', 'resume_uid', 'status', 'position' )
            ->orderBy('createtime', 'desc')
            ->get();

        $record = $record->map(function ($item, $keys) {
            $item->casename = DB::table('cases')->where('uid', $item->case_uid)->first()->casename;
            $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->where('isopen', 1)->first();
            $item->ApplicationTimes = DB::table('user_take_case')->where('case_uid', $item->case_uid)->where('user_uid', $item->user_uid)->get()->count();
            return $item;
        });

        return [
            'table' => 'user_take_case',
            'status' => 'true',
            'result' => $record,
        ];
    }

    public function showCard(Request $request)
    {
        $user = Auth::guard('api')->user();
        $card = DB::table('user_card')->where('user_uid', $user->uid)->where('isopen', 1)->get();
        // return ['result' =>'11'];
        if ($card->isNotEmpty()) {
            return [
                'table' => 'user_card',
                'status' => 'true',
                'result' => $card,
            ];
        } else {
            return [
                'table' => 'user_card',
                'status' => 'true',
                'result' => 'have no card',
            ];
        }

    }

    public function removeCard(Request $request)
    {
        // return $request->all();
        // $data = $request['uid'];
        $user = Auth::guard('api')->user();
        $card = $this->SupportUpdate('user_card', [
            'isopen' => 0,
        ], [
            'user_uid' => $user->uid,
            'isopen' => 1,
        ]);
        return $card;
    }

    public function addValueRecord(Request $request)
    {
        $user = Auth::guard('api')->user();
        $record = DB::table('margin')->where('cmember', $user->uid)->where('status', 'addValue')->where('func', 'addValue')->get();
        return [
            'table' => 'margin',
            'status' => 'true',
            'result' => $record,
        ];
    }

    public function countNotice(Request $request)
    {
        $user = Auth::guard('api')->user();
        $count = DB::table('notice')->where('to_user_uid', $user->uid)->where('isRead', 0)->get()->count();
        return [
            'table' => 'notice',
            'status' => 'true',
            'result' => $count,
        ];
    }

    public function readNotice(Request $request)
    {
        //{"uid":""}
        $data = $request['uid'];
        $notice = DB::table('notice')->where('uid', $data)->first();
        // return [$notice];
        if ($notice->isRead == 0) {
            $read = $this->SupportUpdate('notice', [
                'isRead' => 1,
            ], ['uid' => $data]);
            return $read;
        }else {
            return ['msg' => 'already read'];
        }

        
    }

    public function showDetailNotice(Request $request)
    {
        //{"data":{"page":"","read":""}}
        // $data = $request['data'];
        $page = 0;
        $pagenumber = 20;
        $currentPage = $request['page'];
        $user = Auth::guard('api')->user();
        // $notice = DB::table('notice')->where('to_user_uid', $user->uid)->where('isRead' , 0)->get();
        // // return [$notice->isNotEmpty()];
        // if($data['read'] == 1){
        //     if ($notice->isNotEmpty()) {
        //         $isRead = $notice->map(function ($item, $keys) {
        //             $this->SupportUpdate('notice', [
        //                 'isRead' => 1,
        //             ], ['uid' => $item->uid]);
        //         });
        //     }
        // }

        $notice = DB::table('notice')->where('to_user_uid', $user->uid)->orderBy('createtime', 'desc');

        $countNotice = clone $notice;
        $notice = $notice->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countNotice = $countNotice->count();

        $notice = $notice->map(function ($item, $keys) use ($user) {
            // return $item;
            $item->form_user_name = (DB::table('members')->where('user_uid', $item->from_user_uid)->first()) != null ? (DB::table('members')->where('user_uid', $item->from_user_uid)->first()->username) : 'system';
            $item->to_user_name = DB::table('users')->where('uid',$item->to_user_uid)->first()->username;
            if ($item->belong_table == 'cases') {
                $item->casename = DB::table('cases')->where('uid', $item->belong_uid)->where('isopen' , 1)->first()->casename;
            } elseif ($item->belong_table == 'user_take_case') {
                $takeCaseUid = DB::table('user_take_case')->where('uid', $item->belong_uid)->first();
                // return $takeCaseUid;
                $item->casename = DB::table('cases')->where('uid', $takeCaseUid->case_uid)->where('isopen' , 1)->first()->casename;
                $item->position = DB::table('case_position')->where('uid' , $takeCaseUid->position_uid)->first()->name;
            }
            if ($item->belong_table == 'cases') {
                $cdate = DB::table('cases')->where('uid', $item->belong_uid)->where('isopen' , 1)->first();
                $item->startdate = $cdate->startdate;
                $item->enddate = $cdate->enddate;
            } elseif ($item->belong_table == 'user_take_case') {
                $caseUID = DB::table('user_take_case')->where('uid', $item->belong_uid)->first();
                $tdate = DB::table('cases')->where('uid', $caseUID->case_uid)->where('isopen' , 1)->first();
                $item->startdate = $tdate->startdate;
                $item->enddate = $tdate->enddate;
            }
            return $item;
        });
        return [
            $notice,
            $countNotice,
        ];
    }

}
