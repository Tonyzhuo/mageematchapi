<?php

namespace App\Http\Controllers;

use App\CasePosition;
use App\Http\Controllers\PublicCtr;
use App\User;
use App\UserTakeCase;
use Auth;
use DB;
use Illuminate\Http\Request;

class InviteUserController extends Controller
{
    use PublicCtr\Support;
    public function inviteUser(Request $request) //邀請

    {
        //{"data":{"user_uid":"被邀請的人","case_uid":"邀請的案子","position_uid":"邀請的職位","message":"邀請的訊息"}}
        $user = Auth::guard('api')->user();
        $data = $request['data'];
        $casedata = DB::table('cases')->where('uid', $data['case_uid'])->first();
        $takecase = DB::table('user_take_case')->where('user_uid', $data['user_uid'])->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->whereIn('status', ['accept','money', 'confirm', 'complete'])->get();
        $CPtmp = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
        if($CPtmp->num == $CPtmp->acnum){
            return ['msg' => 'is full'];
        }

        $TakeCaseCalendar = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
            $query->where(function ($query) use ($casedata) {
                $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
                ->orWhere(function ($query) use ($casedata) {
                    $query->where('startdate', '<', $casedata->startdate)
                        ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                })
                ->orwhere(function ($query) use ($casedata) {
                    $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                        ->where('enddate', '>', $casedata->enddate);
                })
                ->orwhere(function ($query) use ($casedata) {
                    $query->where('startdate', '<', $casedata->startdate)
                        ->where('enddate', '>', $casedata->enddate);
                });
        }])
            ->whereIn('isopen', [1])
            ->whereIn('status', ['confrim'])
            ->whereIn('user_uid', [$data['user_uid']])
            ->get();

        if ($TakeCaseCalendar->isEmpty()) {
            if ($takecase->isEmpty()) {
                $inviteUser = $this->SupportInsert('user_take_case', [ //邀請
                    'user_uid' => $data['user_uid'],
                    'case_uid' => $data['case_uid'],
                    'case_user_uid' => $user->uid,
                    'status' => 'accept',
                    'position_uid' => $data['position_uid'],
                    'func' => 'InviteUser',
                ]);
                $notice = $this->SupportInsert('notice', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $inviteUser['result'][0]->uid,
                    'from_user_uid' => $user->uid,
                    'to_user_uid' => $data['user_uid'],
                    'content' => 'case invite you',
                ]);
                if ($data['message'] != null) {
                    $msg = $this->SupportInsert('message', [ //傳訊息
                        'from_user_uid' => $user->uid,
                        'to_user_uid' => $data['user_uid'],
                        'content' => $data['message'],
                    ]);
                }
                return [
                    $inviteUser,
                    $notice,
                ];
            } else {
                return [
                    'table' => 'user_take_case',
                    'status' => 'false',
                    'msg' => 'The case is already taked',
                ];
            }
        } else {
            return [
                'table' => 'user_take_case',
                'status' => 'false',
                'msg' => 'The person has another case',
            ];
        }

    }

    public function doubleInvite(Request $request) //重複邀影

    {
        $data = $request['data']; //case_uid\position_uid\message\user_uid
        $user = Auth::guard('api')->user();
        $casedata = DB::table('cases')->where('uid', $user->uid)->first();


        $case = $this->SupportInsert('user_take_case', [
            'user_uid' => $data['user_uid'],
            'case_uid' => $data['case_uid'],
            'case_user_uid' => $user->uid,
            'resume_uid' => $data['resume_uid'],
            'position_uid' => $data['position_uid'],
            'status' => 'accept',
            'func' => 'InviteUser',
        ]);
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'user_take_case',
            'belong_uid' => $$inviteUser['result'][0]->uid,
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'case invite you',
        ]);
        if ($data['message'] != null) {
            $message = $this->SupportInsert('message', [
                'from_user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'content' => $data['message'],
            ]);
        } else {
            $message = '';
        }
        $resume = DB::table('resume')->where('uid', $data['resume_uid'])->where('isopen', 1)->get();

        return [
            $case,
            $notice,
            'resime' => $resume,
            $message,
        ];
    }

    public function acceptInvite(Request $request) //接受邀請
    // {"data":{"case_uid":"","answer":"","position_uid":"","take_uid":""}}

    {
        $user = Auth::guard('api')->user();
        $data = $request['data'];
        $acnum = DB::table('case_position')->where('uid', $data['position_uid'])->first()->acnum;
        $num = DB::table('case_position')->where('uid', $data['position_uid'])->first()->num;
        $caseCmemberUid = DB::table('cases')->where('uid', $data['case_uid'])->where('isopen', 1)->first()->cmember;
        // return $caseCmemberUid;
        $TakeCaseUid = DB::table('user_take_case')->where('uid' , $data['take_uid'])->where('isopen', 1)->first()->uid;
        $nuspace = DB::table('margin')->where('belong_uid', $data['case_uid'])->where('user_uid' , $caseCmemberUid)->where('position_uid', $data['position_uid'])->where('to_user_uid', "")->where('status' , 'wait')->first()->uid;
        // $aa = DB::table('margin')->where('belong_uid' , $data['case_uid'])->where('position' , $data['position'])->get();
        // return $aa;
        $getInviteUserName = DB::table('members')->where('user_uid', $user->uid)->first()->username;
        $position = DB::table('case_position')->where('uid', $data['position_uid'])->where('isopen', 1)->first();

        $casedata = DB::table('cases')->where('uid', $data['case_uid'])->where('isopen', 1)->where('isIssue', 1)->first();

        $InviteUserCalendar = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
            $query->where(function ($query) use ($casedata) {
                $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->whereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
                ->orWhere(function ($query) use ($casedata) {
                    $query->where('startdate', '<', $casedata->startdate)
                        ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                })
                ->orwhere(function ($query) use ($casedata) {
                    $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                        ->where('enddate', '>', $casedata->enddate);
                })
                ->orwhere(function ($query) use ($casedata) {
                    $query->where('startdate', '<', $casedata->startdate)
                        ->where('enddate', '>', $casedata->enddate);
                });
        }])
            ->whereIn('isopen', [1])
            ->whereIn('status', ['confrim'])
            ->whereIn('user_uid', [$user->uid])
            ->get();
        $isTrueTake = $InviteUserCalendar->filter(function ($item) {return $item->TheCase;})->count() > 0;
        // return [$isTrueTake];
        if ($isTrueTake) {
            return ['msg' => 'is double'];
        }

        $PersonCalendar = DB::table('calendar')
            ->where('user_uid', $user->uid)
            ->where(function ($query) use ($casedata) {
                // dd($casedata);
                $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
            ->orWhere(function ($query) use ($casedata) {
                $query->where('startdate', '<', $casedata->startdate)
                    ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
            })
            ->orwhere(function ($query) use ($casedata) {
                $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                    ->where('enddate', '>', $casedata->enddate);
            })
            ->orwhere(function ($query) use ($casedata) {
                $query->where('startdate', '<', $casedata->startdate)
                    ->where('enddate', '>', $casedata->enddate);
            })
            ->get()
            ->isEmpty(); //空的回傳true

        if ($data['answer'] == 'accept') {
            // return ['aa'];
            // if (!$isTrueTake) {
            // $inviteUser = $this->SupportUpdate('user_take_case', [
            //     'status' => 'check',
            // ], [
            //     'user_uid' => $user->uid,
            //     'case_uid' => $data['case_uid'],
            //     'position' => $data['position'],
            //     'func' => 'InviteUser',
            // ]);
            $inviteUser = $this->SupportUpdate('user_take_case', [
                'status' => 'check',
            ], ['uid' => $data['take_uid']]);
            $notice = $this->SupportInsert('notice', [
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $user->uid,
                'to_user_uid' => $caseCmemberUid,
                'content' => 'accept invite',
            ]);
            
            if ($position->promisemoney == 0) {
                $case = $this->SupportUpdate('user_take_case', [
                    'status' => 'confirm',
                ], ['uid' => $data['take_uid']]);

                $this->SupportUpdate('margin', [
                    'to_user_uid' => $user->uid,
                ], [
                    'uid' => $nuspace,
                ]);
                $margin = $this->SupportInsert('margin', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $TakeCaseUid,
                    'user_uid' => $user->uid,
                    'to_user_uid' => $user->uid,
                    'money' => $position->promisemoney,
                    'processing_fee' => 0, //手續費，之後在做
                    'status' => 'wait',
                    'func' => 'TakeCase',
                ]);
                $CasePosition = $this->SupportUpdate('case_position', [
                    'acnum' => $acnum + 1,
                ], ['uid' => $data['position_uid']]);


                $TakeCaseCalendarTmp = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
                    $query->where(function ($query) use ($casedata) {
                        $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                            ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                    })
                        ->orWhere(function ($query) use ($casedata) {
                            $query->where('startdate', '<', $casedata->startdate)
                                ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                        })
                        ->orwhere(function ($query) use ($casedata) {
                            $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                                ->where('enddate', '>', $casedata->enddate);
                        })
                        ->orwhere(function ($query) use ($casedata) {
                            $query->where('startdate', '<', $casedata->startdate)
                                ->where('enddate', '>', $casedata->enddate);
                        });
                }])
                    ->whereIn('isopen', [1])
                    ->whereIn('user_uid', [$user->uid])
                    ->get(); //自己接的相中的案子
                $waitMoneyCase = $TakeCaseCalendarTmp->where('status', 'money');
                $waitAcceptCase = $TakeCaseCalendarTmp->where('status', 'accept');

                $waitAcceptCase->map(function ($item, $keys) {
                    $this->SupportUpdate('user_take_case', [
                        'isopen' => 0,
                    ], ['uid' => $item->uid]);
                }); //相沖的取消投履歷
                $waitMoneyCase->map(function ($item, $keys) {
                    $this->SupportUpdate('user_take_case', [
                        'status' => 'refuse',
                    ], ['uid' => $item->uid]);
                }); //相沖的case拒絕付保證金

                $noticeInvite = $this->SupportInsert('notice', [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $TakeCaseUid, //改通知內容
                    'from_user_uid' => $caseCmemberUid,
                    'to_user_uid' => $user->uid,
                    'content' => 'paid 0',
                ]);
                

                if (!$PersonCalendar) {
                    return [
                        $inviteUser,
                        $notice,
                        'msg' => 'Is double on PersonCalenda',
                    ];
                } else {
                    return [
                        $inviteUser,
                        $notice,
                    ];
                }
            }
            return [
                'inviteUser' => $inviteUser,
                'notice' => $notice,
            ];

        } else {
            //拒絕邀請
            $inviteUser = $this->SupportUpdate('user_take_case', [
                'status' => 'refuse',
            ], ['uid' => $data['take_uid']]);

            $notice = $this->SupportInsert('notice', [
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $user->uid,
                'to_user_uid' => $caseCmemberUid,
                'content' => 'refuse invite',
            ]);
            return [
                'inviteUser' => $inviteUser,
                'notice' => $notice,
            ];
        }

    }

    public function doubleAccept(Request $request)
    {
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $acnum = DB::table('case_position')->where('uid', $data['position_uid'])->first()->acnum;
        $num = DB::table('case_position')->where('uid', $data['position_uid'])->first()->num;
        $caseCmemberUid = DB::table('cases')->where('uid', $data['case_uid'])->first()->cmember;
        $TakeCaseUid = DB::table('user_take_case')->where('user_uid', $user->uid)->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->where('isopen', 1)->first()->uid;
        $nuspace = DB::table('margin')->where('belong_uid', $data['case_uid'])->where('user_uid', $caseCmemberUid)->where('position_uid', $data['position_uid'])->where('to_user_uid', "")->first()->uid;
        $getInviteUserName = DB::table('members')->where('user_uid', $user->uid)->first()->username;
        $position = DB::table('case_position')->where('uid', $data['position_uid'])->where('isopen', 1)->first();
        $casedata = DB::table('cases')->where('uid', $data['case_uid'])->where('isopen', 1)->where('isIssue', 1)->first();

        $inviteUser = $this->SupportUpdate('user_take_case', [
            'status' => 'check',
        ], ['uid' => $data['take_uid']]);
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'cases',
            'belong_uid' => $data['case_uid'],
            'from_user_uid' => $user->uid,
            'to_user_uid' => $caseCmemberUid,
            'content' => 'accept invite',
        ]);
        if ($position->promisemoney == 0) {
            $case = $this->SupportUpdate('user_take_case', [
                'status' => 'confirm',
            ], ['case_uid' => $data['case_uid'], 'user_uid' => $user->uid, 'position_uid' => $data['position_uid'], 'func' => 'InviteUser']);
            $this->SupportUpdate('margin', [
                'to_user_uid' => $user->uid,
            ], [
                'uid' => $nuspace,
            ]);

            $margin = $this->SupportInsert('margin', [
                'belong_table' => 'user_take_case',
                'belong_uid' => $TakeCaseUid,
                'user_uid' => $user->uid,
                'to_user_uid' => $user->uid,
                'money' => $position->promisemoney,
                'processing_fee' => 0, //手續費，之後在做
                'status' => 'wait',
                'func' => 'TakeCase',
            ]);
            $TakeCaseCalendarTmp = UserTakeCase::with(['TheCase' => function ($query) use ($user, $casedata) {
                $query->where(function ($query) use ($casedata) {
                    $query->whereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                        ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                })
                    ->orWhere(function ($query) use ($casedata) {
                        $query->where('startdate', '<', $casedata->startdate)
                            ->WhereBetween('enddate', [$casedata->startdate, $casedata->enddate]);
                    })
                    ->orwhere(function ($query) use ($casedata) {
                        $query->WhereBetween('startdate', [$casedata->startdate, $casedata->enddate])
                            ->where('enddate', '>', $casedata->enddate);
                    })
                    ->orwhere(function ($query) use ($casedata) {
                        $query->where('startdate', '<', $casedata->startdate)
                            ->where('enddate', '>', $casedata->enddate);
                    });
            }])
                ->whereIn('isopen', [1])
                ->whereIn('user_uid', [$user->uid])
                ->get(); //自己接的相中的案子
            $waitMoneyCase = $TakeCaseCalendarTmp->where('status', 'money');
            $waitAcceptCase = $TakeCaseCalendarTmp->where('status', 'accept');

            $waitAcceptCase->map(function ($item, $keys) {
                $this->SupportUpdate('user_take_case', [
                    'isopen' => 0,
                ], ['uid' => $item->uid]);
            }); //相沖的取消投履歷
            $waitMoneyCase->map(function ($item, $keys) {
                $this->SupportUpdate('user_take_case', [
                    'status' => 'refuse',
                ], ['uid' => $item->uid]);
            }); //相沖的case拒絕付保證金

            $noticeInvite = $this->SupportInsert('notice', [
                'belong_table' => 'user_take_case',
                'belong_uid' => $TakeCaseUid, //改通知內容
                'from_user_uid' => $caseCmemberUid,
                'to_user_uid' => $user->uid,
                'content' => 'paid 0',
            ]);
            if (!$PersonCalendar) {
                return [
                    $inviteUser,
                    $notice,
                    'msg' => 'Is double on PersonCalenda',
                ];
            } else {
                return [
                    $inviteUser,
                    $notice,
                ];
            }
        } else {
            $case = $this->SupportUpdate('user_take_case', [
                'status' => 'money',
            ], ['case_uid' => $data['case_uid'], 'user_uid' => $data['user_uid'], 'position_uid' => $data['position_uid'], 'func' => 'UserTake']);
            return $case;
        }

    }

    public function checkInvite(Request $request) //確認邀請

    {
        // {"data":{"case_uid":"" , "user_uid":"接案方的user_uid","position_uid":"","take_uid":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $CPtmp = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
        $checkInvite = $this->SupportUpdate('user_take_case', [
            'status' => 'money',
        ], ['uid' => $data['take_uid']]);
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'user_take_case',
            'belong_uid' => $data['take_uid'],
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'check invite',
        ]);
        $CasePosition = $this->SupportUpdate('case_position', [
            'acnum' => $CPtmp->acnum + 1,
        ], ['uid' => $data['position_uid']]);
        $AfterCPtmp = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
        if($AfterCPtmp->num == $AfterCPtmp->acnum)  {
            $LastTaker = DB::table('user_take_case')->where('case_uid' , $data['case_uid'])->where('status' , 'wait')->where('isopen' , 1)->get();
            $LastTaker = $LastTaker->map(function($item,$keys){
                $this->SupportInsert('notice', [
                    'belong_table' => 'cases',
                    'belong_uid' => $data['case_uid'],
                    'from_user_uid' => $item->case_user_uid,
                    'to_user_uid' => $item->user->uid,
                    'content' => 'position is full but not pay',
                ]);
            });
        }
        return [
            'check' => $checkInvite,
            'notice' => $notice,
        ];

    }

    public function cancelAccept(Request $request){ //接案方取消接受
        //{"data":{"take_uid":"接案uid","user_uid":"發案方uid","position_uid":"職位uid","case_uid":"案子uid"}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $takeInfo = $this->SupportUpdate('user_take_case',[
            'status' => 'refuse'
        ],['uid' => $data['take_uid']]);
        // return [ 'aa'=> $takeInfo['state']];
        if($takeInfo['state'] == false){
            return['msg' => $takeInfo['state']];
        } 
        $this->SupportInsert('notice', [
            'belong_table' => 'cases',
            'belong_uid' => $data['case_uid'],
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'position is full but not pay',
        ]);
        return ['msg' => $takeInfo['state']];

        
    }

    public function cancelInvite(Request $request) //取消邀請人才

    {
        //{"data":{"case_uid":"","user_uid":"","position":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $cancelInvite = $this->SupportUpdate('user_take_case', [
            'isopen' => 0,
        ], [
            'case_uid' => $data['case_uid'],
            'user_uid' => $data['user_uid'],
            'position_uid' => $data['position_uid'],
            'func' => 'InviteUser',
        ]);
        return $cancelInvite;
    }
//     public function InvitePromisemoneyCaseSide(Request $request) //發案方付保證金

//     {
    //         // {"data":{"case_uid":"" , "user_uid":"","position":""}} 付給哪個case的哪職位的誰
    //         $data = $request['data'];
    //         $user = Auth::guard('api')->user();
    //         $promisemoney = CasePosition::where(['name' => $data['position'], 'case_uid' => $data['case_uid']])->first()->promisemoney; //搜尋該職位保證金
    //         $userMoney = User::where('uid', $user->uid)->first()->money; //查詢使用者的剩餘金額
    //         $InviteUserUid = DB::table('case_invite_users')->where('case_uid', $data['case_uid'])->where('user_uid', $data['user_uid'])->where('position', $data['position'])->first()->uid;
    //         $ispaid = DB::table('margin')
    //             ->where('user_uid', $data['user_uid'])
    //             ->where('to_user_uid', $user->uid)
    //             ->where('belong_table', 'case_invite_users')
    //             ->where('belong_uid', $InviteUserUid)
    //             ->get(); //查詢對方有無付款
    //         if ($userMoney >= $promisemoney) {
    //             if (!$ispaid->isEmpty()) //對方已付款
    //             {
    //                 $Money = $this->SupportUpdate('users', [
    //                     'money' => $userMoney - $promisemoney,
    //                 ], ['uid' => $user->uid]); //從帳戶扣錢
    //                 $margin = $this->SupportInsert('margin', [
    //                     'belong_table' => 'case_invite_users',
    //                     'belong_uid' => $InviteUserUid,
    //                     'user_uid' => $user->uid,
    //                     'to_user_uid' => $data['user_uid'],
    //                     'money' => $promisemoney,
    //                     'processing_fee' => 0, //手續費，之後在做
    //                     'status' => 'wait',
    //                     'func' => 'InviteUser',
    //                 ]); //支付保證金
    //                 $InviteUser = $this->SupportUpdate('case_invite_users', [
    //                     'status' => 'confirm',
    //                 ], ['user_uid' => $data['user_uid'],
    //                     'case_uid' => $data['case_uid'],
    //                     'position' => $data['position']]); //付完保證金

//                 $notice = $this->SupportInsert('notice', [
    //                     'to_user_uid' => $user->uid,
    //                     'content' => 'other side are already paid',
    //                 ]);
    //                 $notice = $this->SupportInsert('notice', [
    //                     'to_user_uid' => $data['user_uid'],
    //                     'content' => 'other side are already paid',
    //                 ]);

//             } else {
    //                 $Money = $this->SupportUpdate('users', [
    //                     'money' => $userMoney - $promisemoney,
    //                 ], ['uid' => $user->uid]); //從帳戶扣錢
    //                 $margin = $this->SupportInsert('margin', [
    //                     'belong_table' => 'case_invite_users',
    //                     'belong_uid' => $InviteUserUid,
    //                     'user_uid' => $user->uid,
    //                     'to_user_uid' => $data['user_uid'],
    //                     'money' => $promisemoney,
    //                     'processing_fee' => 0, //手續費，之後在做
    //                     'status' => 'wait',
    //                     'func' => 'InviteUser',
    //                 ]); //支付保證金
    //             }
    //         } else {
    //             return ['msg' => 'money is not enough'];
    //         }

//         $countPerson = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('status', 'confirm')->get()->count();
    //         $TotalPerson = DB::table('case_position')->where('case_uid', $data['case_uid'])->sum('num'); //此總共案需付保證金的人

// // return $TotalPerson;
    //         //需付保證金的總人數
    //         $allPeopel = CaseInviteUser::where('case_uid', $data['case_uid'])->get(); //接此案的所有人

// // return [$ispaid->isEmpty()];

//         if ($countPerson == ($TotalPerson * 2)) {
    //             $notice = $allPeopel->map(function ($item, $keys) {
    //                 $notice = $this->SupportInsert('notice', [
    //                     // 'from_user_uid' => 'system',
    //                     'to_user_uid' => $item['user_uid'],
    //                     'content' => 'all people are pay for',
    //                 ]);
    //             });

//             return [
    //                 $notice,
    //                 $margin,
    //             ];
    //         } else {
    //             return $margin;
    //         }

//     }
}
