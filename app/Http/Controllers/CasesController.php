<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PublicCtr;
use App\UserSaveCase;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

// use Request;

class CasesController extends Controller
{
    use PublicCtr\Support;

    public function showcase(Request $request) //顯示所有case

    {
        // use Request;
        $page = 0;
        $pagenumber = 20;
        $today = Carbon::today();
        $user = Auth::guard('api')->user();
        $currentPage = $request['page'];
        $calendar = DB::table('calendar')->where('user_uid', $user->uid)->where('isopen', 1)->get();
        // return $calendar->where('startdate' , '>=' , '2018-12-30')->where('enddate' , '<=' , '2019-01-01' );
        $takeCase = DB::table('user_take_case')->where('user_uid', $user->uid)->whereIn('status', ['money', 'confirm'])->where('isopen', 1)->get();
        $takeCase = $takeCase->map(function ($item, $keys) {
            $item->casedate = DB::table('cases')->where('uid', $item->case_uid)->select('startdate', 'enddate', 'uid')->first();
            return $item;
        });

        $thecase = DB::table('cases')
        // ->with("TakeCase")
            ->where('isopen', 1)
            ->where('isIssue', 1)
            ->where('status', 'wait')
            ->where('search', 1)
            ->where('releasedate', '>', $today)
            ->orderBy('createtime', 'desc');

        $countcase = clone $thecase;
        $thecase = $thecase->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countcase = $countcase->count();

        $thecase = $thecase->map(function ($item, $keys) use ($user, $today, $calendar, $takeCase) {
            $item->uid;
            $item->isSave = (UserSaveCase::where(['cmember' => $user->uid, "case_uid" => $item->uid, 'isopen' => 1])->count() > 0);
            //>0顯示天數 否則顯示true(過期)
            $item->isOverdue = ((floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get();
            $item->category = explode(',', $item->category);
            $item->position = $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                return $item;
            });
            $item->companyData = DB::table('companys')->where('belong_member', $user->uid)->first();

            $Calendaraa = $calendar->where('startdate', '>=', $item->startdate)->where('enddate', '>=', $item->enddate)->where('startdate', '<=', $item->enddate)->isNotEmpty();
            $Calendarbb = $calendar->where('startdate', '<=', $item->startdate)->where('enddate', '<=', $item->enddate)->where('enddate', '>=', $item->startdate)->isNotEmpty();
            $Calendarcc = $calendar->where('startdate', '<=', $item->startdate)->where('enddate', '>=', $item->enddate)->isNotEmpty();
            $Calendardd = $calendar->where('enddate', '>=', $item->startdate)->where('startdate', '>=', $item->startdate)->where('enddate', '<=', $item->enddate)->where('startdate', '<=', $item->enddate)->isNotEmpty();

            $takeCaseaa = $takeCase->where('casedate.startdate', '>=', $item->startdate)->where('casedate.enddate', '>=', $item->enddate)->where('casedate.startdate', '<=', $item->enddate)->isNotEmpty();
            $takeCasebb = $takeCase->where('casedate.startdate', '<=', $item->startdate)->where('casedate.enddate', '<=', $item->enddate)->where('casedate.enddate', '>=', $item->startdate)->isNotEmpty();
            $takeCasecc = $takeCase->where('casedate.startdate', '<=', $item->startdate)->where('casedate.enddate', '>=', $item->enddate)->isNotEmpty();
            $takeCasedd = $takeCase->where('casedate.enddate', '>=', $item->startdate)->where('casedate.startdate', '>=', $item->startdate)->where('casedate.enddate', '<=', $item->enddate)->where('casedate.startdate', '<=', $item->enddate)->isNotEmpty();
            if ($Calendaraa == 'true' || $Calendarbb == 'true' || $Calendarcc == 'true' || $Calendardd == 'true') {
                $item->isCalendarConflict = 'true';
            } else {
                $item->isCalendarConflict = 'false';
            }
            if ($takeCaseaa == 'true' || $takeCasebb == 'true' || $takeCasecc == 'true' || $takeCasedd == 'true') {
                $item->isTakeCaseConflict = 'true';
            } else {
                $item->isTakeCaseConflict = 'false';
            }
            $Conflict = ($item->isTakeCaseConflict == 'true' || $item->isCalendarConflict == 'true') ? 'true' : 'false';
            $allReadtTake = ($takeCase->where('casedate.uid', '=', $item->uid)->first() == null) ? 'flase' : 'true';

            if ($allReadtTake == 'true') {
                $item->isConflict = 'allReadyTake';
            } elseif ($Conflict == 'true') {
                $item->isConflict = 'have conflict';
            } else {
                $item->isConflict = 'no conflict';
            }
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'ture',
            "count" => $countcase,
            "result" => $thecase,
            // 'page' => $currentPage
        ];
    }

    public function unLoginShowCase(Request $request)
    {
        $page = 0;
        $pagenumber = 20;
        $today = Carbon::today();
        // return $request['page'];
        $currentPage = $request['page'];

        $thecase = DB::table('cases')
        // ->with("TakeCase")
            ->where('isopen', 1)
            ->where('isIssue', 1)
            ->where('status', 'wait')
            ->where('search', 1)
            ->where('releasedate', '>', $today)
            ->orderBy('createtime', 'desc');

        $countcase = clone $thecase;
        $thecase = $thecase->skip($page + (($currentPage - 1) * $pagenumber))->take($pagenumber)->get();
        $countcase = $countcase->count();

        $thecase = $thecase->map(function ($item, $keys) use ($today) {
            $item->uid;
            //>0顯示天數 否則顯示true(過期)
            $item->isOverdue = ((floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->get();
            $item->category = explode(',', $item->category);
            $item->position = $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                return $item;
            });
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'ture',
            "count" => $countcase,
            "result" => $thecase,
        ];
    }

    public function addcase(Request $request) //新增專案(發案玩先扣保證金)

    {
        // return $request->all();
        $user = Auth::guard('api')->user();
        $casedata = $request['case'];
        $casedata['startdate'] = Carbon::parse($casedata['startdate'])->timezone('Asia/Taipei');
        $casedata['enddate'] = Carbon::parse($casedata['enddate'])->timezone('Asia/Taipei');
        $casedata['releasedate'] = Carbon::parse($casedata['releasedate'])->timezone('Asia/Taipei');
        $casedata['category'] = collect($casedata['category'])->map(function ($item, $keys) {
            $item = $item['name'];
            return $item;
        });
        $casedata['category'] = $casedata['category']->implode(",");

        $casedata['location'] = collect($casedata['location'])->map(function ($item, $keys) {
            $item = $item['city_area'];
            return $item;
        });
        $casedata['location'] = $casedata['location']->implode(",");

        $coldata = collect($casedata);
        if ($coldata['phonepart'] != null) {
            $coldata['phone'] = $coldata['phone'] . "#" . $coldata['phonepart'];
            $coldata->forget('phonepart');
        } else {
            $coldata->forget('phonepart');
        }

        $coldata->put('totaldays', floor((strtotime($coldata['enddate']) - strtotime($coldata['startdate'])) / 3600 / 24) + 1);
        // $coldata->put('needtime_startdate', $coldata['needtime']['startdate']);
        // $coldata->put('needtime_enddate', $coldata['needtime']['enddate']);
        // $coldata->forget('needtime');
        $coldata->put('status', 'wait');
        $choosejob = $coldata['choosejob'];
        $allPromiseMoney = collect($choosejob)->sum('promisemoney');
        $ownMoney = DB::table('users')->where('uid', $user->uid)->first()->money;

        if ($ownMoney < $allPromiseMoney) {
            return [
                'table' => 'cases',
                'status' => 'false',
                'result' => 'Money not enough',
            ];
        } else {
            $coldata->forget('choosejob');

            $case = $this->SupportInsert('cases', $coldata); //新增專案
            $choosejob = collect($choosejob)->map(function ($item, $keys) use ($case) {
                $item['case_uid'] = $case['result'][0]->uid;
                return $item;
            });

            $choosejob = $choosejob->map(function ($item, $keys) use ($user) { //新增需求職位
                $choosejob = $this->SupportInsert('case_position', [
                    'name' => $item['name'],
                    'num' => $item['num'],
                    'language' => $item['language'],
                    'ability' => $item['ability'],
                    'case_uid' => $item['case_uid'],
                    'promisemoney' => $item['promisemoney'],
                    'salary' => $item['salary'],
                    'notice' => $item['notice'],
                ]);
                for ($i = 0; $i < $item['num']; $i++) {
                    $margin = $this->SupportInsert('margin', [
                        'belong_table' => 'cases',
                        'belong_uid' => $item['case_uid'],
                        'user_uid' => $user->uid,
                        'money' => $item['promisemoney'],
                        'processing_fee' => 0,
                        'status' => 'wait',
                        'func' => 'addCase',
                        'position' => $item['name'],
                        'position_uid' => $choosejob['result'][0]->uid,
                    ]);
                    // echo [$margin];
                    $userMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
                    $this->SupportUpdate('users', [
                        'money' => $userMoney - $item['promisemoney'],
                    ], [
                        'uid' => $user->uid,
                    ]);
                }

                return [$choosejob, $margin];
            });
            return [
                $case, //case大項
                $choosejob, //職位、人數
                // $margin
                // $totlaPromiseMoney //扣帳戶的錢
            ];
        }

    }

    public function cacheCase(Request $request) //暫存專案

    {
        $user = Auth::guard('api')->user();
        $casedata = $request['case'];
        $casedata['startdate'] = Carbon::parse($casedata['startdate'])->timezone('Asia/Taipei');
        $casedata['enddate'] = Carbon::parse($casedata['enddate'])->timezone('Asia/Taipei');
        $casedata['releasedate'] = Carbon::parse($casedata['releasedate'])->timezone('Asia/Taipei');
        $userMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        // dd($casedata['category'][0]);
        $casedata['category'] = collect($casedata['category'])->map(function ($item, $keys) {
            $item = $item['name'];
            return $item;
        });
        $casedata['category'] = $casedata['category']->implode(",");

        $casedata['location'] = collect($casedata['location'])->map(function ($item, $keys) {
            $item = $item['city_area'];
            return $item;
        });
        $casedata['location'] = $casedata['location']->implode(",");

        $coldata = collect($casedata);
        if ($coldata['phonepart'] != null) {
            $coldata['phone'] = $coldata['phone'] . "#" . $coldata['phonepart'];
            $coldata->forget('phonepart');
        } else {
            $coldata->forget('phonepart');
        }

        $coldata->put('totaldays', floor((strtotime($coldata['enddate']) - strtotime($coldata['startdate'])) / 3600 / 24) + 1);

        $choosejob = $coldata['choosejob'];
        $allPromiseMoney = collect($choosejob)->sum('promisemoney');
        $ownMoney = DB::table('users')->where('uid', $user->uid)->first()->money;

        $coldata->put('isIssue', 0);
        $coldata->put('status', 'wait');
        $coldata->forget('choosejob');
        $case = $this->SupportInsert('cases', $coldata);
        $choosejob = collect($choosejob)->map(function ($item, $keys) use ($case) {
            // $name = DB::table('position_code')->where('id' , $item['name'])->first()->position_name;
            // $item['name'] = $name;
            $item['case_uid'] = $case['result'][0]->uid;
            return $item;
        });
        $choosejob = $choosejob->map(function ($item, $keys) use ($user, $userMoney) { //新增需求職位
            if ($item['name'] != null) {
                $choosejob = $this->SupportInsert('case_position', [
                    'name' => $item['name'],
                    'num' => $item['num'],
                    'language' => $item['language'],
                    'ability' => $item['ability'],
                    'case_uid' => $item['case_uid'],
                    'promisemoney' => $item['promisemoney'],
                    'salary' => $item['salary'],
                    'notice' => $item['notice'],
                ]);
                return $choosejob;
            }
        });
        return [
            $case,
            $choosejob,
        ];

    }

    public function issueCase(Request $request)
    {
        // return $request->all();
        $data = $request['case_uid'];
        $user = Auth::guard('api')->user();
        $userMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        $casedata = DB::table('cases')->where('uid', $data)->first();
        $isPosition = DB::table('case_position')->where('case_uid', $data)->where('isopen', 1)->get();
        // return $isPosition->sum('promisemoney');
        // return [$isPosition];
        if ($casedata->casename != null || $casedata->category != null || $casedata->content != null || $casedata->attraction != null || $casedata->startdate != null || $casedata->enddate != null || $casedata->location != null || $casedata->releasedate != null || $casedata->attentiln != null || $casedata->connect != null || $casedata->phone != null || $casedata->search != null) {
            if ($isPosition->isNotEmpty()) {
                $position = $isPosition->map(function ($item, $keys) use ($user, $userMoney) {
                    for ($i = 0; $i < $item->num; $i++) {
                        $this->SupportInsert('margin', [
                            'belong_table' => 'cases',
                            'belong_uid' => $item->case_uid,
                            'user_uid' => $user->uid,
                            'money' => $item->promisemoney,
                            'processing_fee' => 0,
                            'status' => 'wait',
                            'func' => 'addCase',
                            'position' => $item->name,
                            'position' => $item->uid,
                        ]);
                        $this->SupportUpdate('users', [
                            'money' => $userMoney - $item->promisemoney,
                        ], [
                            'uid' => $user->uid,
                        ]);
                    }
                    return $item;
                });
                $case = $this->SupportUpdate('cases', [
                    'isIssue' => 1,
                ], [
                    'uid' => $data,
                ]);

                return [
                    'case' => $case,
                    'position' => $position,
                    'totalMoney' => $isPosition->sum('promisemoney'),
                ];
            } else {
                return ['msg' => 'pls fill in the position'];
            }
        } else {
            return ['msg' => 'pls fill data'];
        }

    }

    public function pushcase() //發案管理\已發佈

    {
        $user = Auth::guard("api")->user();
        $today = Carbon::now();
        $owncase = DB::table('cases')
            ->where('isopen', 1)
            ->where('isIssue', 1)
            ->where('status', 'wait')
            ->where('cmember', $user->uid)
            ->where('releasedate', '>', $today)
            ->orderBy('createtime', 'desc')
            ->get();

        $owncase = $owncase->map(function ($item, $keys) use ($today, $user) {
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->select('uid', 'case_uid', 'name', 'num', 'acnum', 'language', 'ability', 'promisemoney', 'salary', 'notice')->get();
            $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                $item->application = DB::table('user_take_case')->where('case_uid', $item->case_uid)->where('position_uid', $item->uid)
                    ->where(function ($query) {
                        $query->where('status', ['accept', 'money', 'confirm']);
                    })
                    ->select('user_uid', 'case_uid', 'resume_uid', 'status', 'position_uid')
                    ->orderBy('updatetime', 'asc')
                    ->get();
                $item->userTake = DB::table('user_take_case')->where('case_uid', $item->case_uid)->where('position_uid', $item->uid)->where('func', 'UserTake')->get()->count();
                $item->caseInvite = DB::table('user_take_case')->where('case_uid', $item->case_uid)->where('position_uid', $item->uid)->where('func', 'InviteUser')->get()->count();
                return $item;
            });
            $item->companyData = DB::table('companys')->where('belong_member', $user->uid)->first();
            $item->isOverdue = ((floor((strtotime($item->releasedate) - strtotime($today)) / 3600 / 24)) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            $tmp = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1);
            $item->TotalNum = $tmp->sum('num');
            $item->TotalLastNum = $item->TotalNum - $tmp->sum('acnum');
            $item->waitAccept = DB::table('user_take_case')->where('case_uid', $item->uid)->where('isopen', 1)->where('status', 'accept')->get()->count();
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'ture',
            'result' => $owncase,
        ];
    }

    public function showComleteCase(Request $request)
    {
        $user = Auth::guard("api")->user();
        $today = Carbon::now();
        $owncase = DB::table('cases')
            ->where('isopen', 1)
            ->where('isIssue', 1)
            ->where('status', 'close')
            ->where('releasedate', '>', $today)
            ->orderBy('createtime', 'desc')
            ->get();

        $owncase = $owncase->map(function ($item, $keys) use ($today) {
            $item->position = DB::table('case_position')->where('case_uid', $item->uid)->where('isopen', 1)->select('case_uid', 'name', 'num', 'acnum', 'language', 'ability', 'promisemoney', 'salary', 'notice')->get();
            $item->position->map(function ($item, $keys) {
                $item->isLast = ($item->num == $item->acnum) ? "full" : ($item->num - $item->acnum);
                $item->application = DB::table('user_take_case')->where('case_uid', $item->case_uid)->where('position_uid', $item->uid)
                    ->where(function ($query) {
                        $query->where('status', ['accept', 'money', 'confirm']);
                    })
                    ->select('user_uid', 'case_uid', 'resume_uid', 'status', 'position', 'position_uid')
                    ->orderBy('updatetime', 'asc')
                    ->get();
                return $item;
            });
            $item->isOverdue = ((floor((strtotime($item->releasedate) - strtotime($today)) / 3600 / 24)) > 0) ? (floor(strtotime($item->releasedate) - strtotime($today)) / 3600 / 24) : 'true';
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'ture',
            'result' => $owncase,
        ];
    }

    public function showCaseTaker(Request $request) // 發案管理\已發佈\進行中\來應試的人

    {
        // return ["hello"];
        //{"data":{"case_uid":"","position":""}}
        $data = $request['data'];
        // return $data;
        $user = Auth::guard('api')->user();
        $taker = DB::table('user_take_case')
            ->where('case_uid', $data['case_uid'])
            ->where('position_uid', $data['position_uid'])
            ->where('isopen', 1)
            ->whereIn('status', ['accept', 'money', 'confirm'])
            ->get();

        $taker = $taker->map(function ($item, $keys) {
            $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
            return $item;
        })->groupBy('status');

        return [
            'table' => 'user_take_case',
            'status' => 'true',
            'result' => $taker,
        ];

    }

    public function showCaseInvite(Request $request) // 發案管理\已發佈\進行中\已邀請的人

    {
        // {"data":{"case_uid":"","position":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $today = Carbon::now();
        $inviter = DB::table('user_take_case')
            ->where('case_uid', $data['case_uid'])
            ->where('position_uid', $data['position_uid'])
            ->where('isopen', 1)
            ->whereIn('status', ['accept', 'money', 'confirm'])
            ->where('func', 'InviteUser')
            ->get()
            ->groupBy('status');
        $inviter = $inviter->map(function ($item, $keys) {
            $item = $item->map(function ($item, $keys) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                return $item;
            });
            return $item;
        });

        return [
            'table' => 'cases',
            'state' => 'ture',
            'result' => $inviter,
        ];
    }
    public function showCaseRefuse(Request $request) //發案管理\已發佈\進行中\被拒絕的人

    {
        //{"data":{"case_uid":"","position":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        if ($data['position_uid'] == null) {
            $TakeRefuse = DB::table('user_take_case')
                ->where('case_uid', $data['case_uid'])
                ->where('isopen', 1)
                ->where('func', 'UserTake')
                ->where('status', 'refuse')
                ->get();

            $InviteRefuse = DB::table('user_take_case')
                ->where('case_uid', $data['case_uid'])
                ->where('isopen', 1)
                ->where('func', 'InviteUser')
                ->where('status', 'refuse')
                ->get();

            $TakeRefuse = $TakeRefuse->map(function ($item, $keys) use ($avatar) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                return $item;
            });

            $InviteRefuse = $InviteRefuse->map(function ($item, $keys) use ($avatar) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $item->resume = DB::table('resume')->where('user_uid', $item->belong_member)->where('isPreset', 1)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                return $item;
            });
            return [
                'table' => 'cases',
                'state' => 'ture',
                'TakeRefuse' => $TakeRefuse,
                'InviteRefuse' => $InviteRefuse,
            ];
        } else {
            $TakeRefuse = DB::table('user_take_case')
                ->where('case_uid', $data['case_uid'])
                ->where('position_uid', $data['position_uid'])
                ->where('isopen', 1)
                ->where('func', 'UserTake')
                ->where('status', 'refuse')
                ->get();

            $InviteRefuse = DB::table('user_take_case')
                ->where('case_uid', $data['case_uid'])
                ->where('position_uid', $data['position_uid'])
                ->where('isopen', 1)
                ->where('func', 'InviteUser')
                ->where('status', 'refuse')
                ->get();

            $TakeRefuse = $TakeRefuse->map(function ($item, $keys) use ($avatar) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                return $item;
            });

            $InviteRefuse = $InviteRefuse->map(function ($item, $keys) use ($avatar) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $item->resume = DB::table('resume')->where('user_uid', $item->belpng_member)->where('isPreset', 1)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                return $item;
            });
            return [
                'table' => 'cases',
                'state' => 'ture',
                'TakeRefuse' => $TakeRefuse,
                'InviteRefuse' => $InviteRefuse,
            ];
        }

    }

    public function showAllStatus(Request $request)
    {
        //{"data":{"case_uid":"","position":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        if ($data['position_uid'] == null) {
            $allStatus = DB::table('user_take_case')
                ->where('case_uid', $data['case_uid'])
                ->where('isopen', 1)
                ->get();
            // return $allStatus;
            $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
            // return $allStatus;
            $allStatus = $allStatus->map(function ($item, $keys) use ($avatar) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                if ($item->func == 'InviteUser') {
                    $item->resume = DB::table('resume')->where('belong_member', $item->user_uid)->where('isPreset', 1)->first();
                } else {
                    $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->first();
                }
                $item->Money = DB::table('case_position')->where('uid', $item->position_uid)->first()->promisemoney;
                $item->position = DB::table('case_position')->where('uid', $item->position_uid)->first()->name;
                return $item;
            })->groupBy('status');

            return [
                'table' => 'user_take_case',
                'status' => 'true',
                'result' => $allStatus,
            ];
        } else {
            $allStatus = DB::table('user_take_case')
                ->where('case_uid', $data['case_uid'])
                ->where('position_uid', $data['position_uid'])
                ->where('isopen', 1)
                ->get();
            $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
            // return $allStatus;
            $allStatus = $allStatus->map(function ($item, $keys) use ($avatar) {
                $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                if ($item->func == 'InviteUser') {
                    $item->resume = DB::table('resume')->where('belong_member', $item->user_uid)->where('isPreset', 1)->first();
                } else {
                    $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->first();
                }
                $item->Money = DB::table('case_position')->where('uid', $item->position_uid)->first()->promisemoney;
                $item->position = DB::table('case_position')->where('uid', $item->position_uid)->first()->name;
                return $item;
            })->groupBy('status');

            return [
                'table' => 'user_take_case',
                'status' => 'true',
                'result' => $allStatus,
            ];
        }

    }

    public function showRefuse(Request $request) //人才概況\已拒絕

    {
        //{"data":{"case_uid":"","position":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $Status = DB::table('user_take_case')
            ->where('case_uid', $data['case_uid'])
            ->where('position_uid', $data['position_uid'])
            ->where('isopen', 1)
            ->where('status', 'refuse')
            ->get();
        // return $allStatus;
        $Status = $Status->map(function ($item, $keys) {
            $item->user = DB::table('members')->where('user_uid', $item->user_uid)->first();
            $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->first();
            return $item;
        })->groupBy('status');

        return [
            'table' => 'user_take_case',
            'status' => 'true',
            'result' => $allStatus,
        ];
    }

    public function savecase(Request $request) //收藏專案

    {
        // return $request->all();
        $caseuid = $request['data'];
        // return $caseuid;
        $user = Auth::guard('api')->user();
        $userinfo = $user->load(["UserSaveCase" => function ($query) use ($caseuid) {
            return $query->where("case_uid", $caseuid);
        }]);
        // return ['test'=>$userinfo];

        $savecase = $userinfo->UserSaveCase->isEmpty();
        // return [$savecase];
        $isopen = DB::table('user_save_case')
            ->where('user_uid', $user->uid)
            ->where('case_uid', $caseuid)
            ->first();
        // return $isopen;
        // return $isopen;
        if ($isopen != null) {
            $isopeninfo = $isopen->isopen;
        }

        // return $isopeninfo;

        if ($savecase) {
            $case = $this->SupportInsert('user_save_case', [
                'user_uid' => $user->uid,
                'case_uid' => $caseuid,
            ]);
            return $case;
        } elseif ($isopeninfo == 0) {
            $case = $this->SupportUpdate('user_save_case', [
                'isopen' => 1,
            ], ['user_uid' => $user->uid, 'case_uid' => $caseuid]);
            return $case;
        } elseif ($isopeninfo == 1) {
            $case = $this->SupportUpdate('user_save_case', [
                'isopen' => 0,
            ], ['user_uid' => $user->uid, 'case_uid' => $caseuid]);
            return $case;
        }

    }

    public function updatecase(Request $request) //編輯專案

    {
        // case\case_position
        $user = Auth::guard('api')->user();
        $casedata = $request['case'];
        $uid = $casedata['case_uid'];
        $casedata['startdate'] = Carbon::parse($casedata['startdate'])->timezone('Asia/Taipei');
        $casedata['enddate'] = Carbon::parse($casedata['enddate'])->timezone('Asia/Taipei');
        $casedata['releasedate'] = Carbon::parse($casedata['releasedate'])->timezone('Asia/Taipei');
        if ($casedata['category'] != null) {
            $casedata['category'] = collect($casedata['category'])->map(function ($item, $keys) {
                $item = $item;
                return $item;
            });
            $casedata['category'] = $casedata['category']->implode(",");
        }
        if ($casedata['location'] != null) {
            $casedata['location'] = collect($casedata['location'])->map(function ($item, $keys) {
                $item = $item;
                return $item;
            });
            $casedata['location'] = $casedata['location']->implode(",");
        }

        if ($casedata['phonepart'] != null) {
            $casedata['phone'] = $casedata['phone'] . "#" . $coldata['phonepart'];
        }

        // collect($casedata)->put('totaldays', floor((strtotime($casedata['enddate']) - strtotime($casedata['startdate'])) / 3600 / 24));
        // $coldata->put('needtime_startdate', $coldata['needtime']['startdate']);
        // $coldata->put('needtime_enddate', $coldata['needtime']['enddate']);
        // $coldata->forget('needtime');


        $case = $this->SupportUpdate('cases',[
            'casename' => $casedata['casename'],
            'attention' =>  $casedata['attention'],
            'attraction' => $casedata['attraction'],
            'casecontent' => $casedata['casecontent'],
            'category'=> $casedata['category'],
            'connect'=> $casedata['connect'],
            'enddate'=> $casedata['enddate'],
            'location'=> $casedata['location'],
            'peoplenum'=> $casedata['peoplenum'],
            'phone'=> $casedata['phone'],
            'releasedate'=> $casedata['releasedate'],
            'search'=> $casedata['search'],
            'startdate'=> $casedata['startdate'],
            'totaldays' => (floor(strtotime($casedata['enddate']) - strtotime($casedata['startdate']))) +1,
        ] , ['uid'=> $uid]);

        return $case;

    }

    public function editPosition(Request $request) //修改職位需求

    {
        //***不允許修改職位名稱跟保證金、不允許修改小於已應徵上的人數*/
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $userMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        // $editMoney = $data['promisemoney'];
        $positionData = DB::table('case_position')->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->first();
        $positionmoney = $positionData->promisemoney;
        $positionNum = $positionData->num;
        $positionAcNum = $positionData->acnum;
        // $position = collect($position);
        $position = collect($data);
        $data = collect($data);
        $caseData = DB::table('cases')->where('uid', $data['case_uid'])->first();
        $alreadyTaker = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->whereIn('status', ['money', 'confirm'])->get();
        $data->forget('case_uid');
        $data->forget('name');
        $data->forget('position_uid');
        // return [$position,$data];

        // $marginUid = DB::table('margin')->where('belong_uid', $position['case_uid'])->where('position', $position['name'])->where('to_user_uid', "")->first()->uid;
        // return $marginUid;
        if ($positionAcNum > $data['num']) {
            return [
                'table' => 'case_position',
                'status' => 'false',
                'result' => 'can not less than ',
            ];
        } else {
            // return $data;
            $editPosition = $this->SupportUpdate('case_position', [
                'num' => $data['num'],
                'language' => $data['language'],
                'ability' => $data['ability'],
                'salary' => $data['salary'],
                'notice' => $data['notice'],
            ], [
                'case_uid' => $position['case_uid'],
                'name' => $position['name'],
            ]);
            if ($alreadyTaker->isNotEmpty()) {
                $notice = $alreadyTaker->map(function ($item, $keys) use ($user, $position) {
                    $notice = $this->SupportInsert('notice', [
                        'belong_table' => 'cases',
                        'belong_uid' => $position['case_uid'],
                        'from_user_uid' => $user->uid,
                        'to_user_uid' => $item->user_uid,
                        'content' => 'change the content',
                    ]);
                });
            }

            if ($positionNum > $data['num']) { //需求減少
                for ($i = 0; $i < ($positionNum - $data['num']); $i++) {
                    $marginUid = DB::table('margin')->where('belong_uid', $position['case_uid'])->where('position_uid', $position['position_uid'])->where('to_user_uid', '')->where('final_return', '')->where('status', 'wait')->first()->uid;
                    $margin = $this->SupportUpdate('margin', [
                        'status' => 'return',
                        'final_return' => $user->uid,
                    ], [
                        'uid' => $marginUid,
                    ]);
                }
                $userPay = $this->SupportUpdate('users', [
                    'money' => $userMoney + ($positionmoney * ($positionNum - $data['num'])),
                ], [
                    'uid' => $user->uid,
                ]);
                return [
                    $editPosition,
                    $margin,
                    $userPay,
                ];

            } elseif ($data['num'] > $positionNum) { //需求增加
                for ($i = 0; $i < ($data['num'] - $positionNum); $i++) {
                    $margin = $this->SupportInsert('margin', [
                        'belong_table' => 'cases',
                        'belong_uid' => $position['case_uid'],
                        'user_uid' => $user->uid,
                        'money' => $positionmoney,
                        'status' => 'wait',
                        'func' => 'TakeCase',
                        'position' => $position['name'],
                        'position_uid' => $position['position_uid'],
                    ]);
                }
                $userPay = $this->SupportUpdate('users', [
                    'money' => $userMoney - ($positionmoney * ($data['num'] - $positionNum)),
                ], [
                    'uid' => $user->uid,
                ]);

                return [
                    $editPosition,
                    $margin,
                    $userPay,
                ];
            } else {
                return $editPosition;
            }

        }
    }

    public function addPosition(Request $request) //增加職位需求

    {
        // return $request->all();
        //{"data":{"case_uid":"","name":"","num":"","language":"","ability":"","promisemoney":"","salary":"","notice":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $userOwnMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        $allPositionMoney = $data['num'] * $data['promisemoney'];

        if ($userOwnMoney < $allPositionMoney) {
            return [
                'table' => 'margin',
                'status' => 'false',
                'result' => 'promisemoney not enough',
            ];
        } else {
            $addPosition = $this->SupportInsert('case_position', $data);
            $positionNum = DB::table('case_position')->where('case_uid', $data['case_uid'])->where('name', $data['name'])->first()->num;
            for ($i = 0; $i < $positionNum; $i++) {
                $this->SupportInsert('margin', [
                    'belong_table' => 'case',
                    'belong_uid' => $data['case_uid'],
                    'user_uid' => $user->uid,
                    'money' => $data['promisemoney'],
                    'status' => 'wait',
                    'func' => 'TakeCase',
                    'position' => $data['name'],
                    'position_uid' => $addPosition[0]['result']->uid,
                ]);
                $userMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
                $this->SupportUpdate('users', [
                    'money' => $userMoney - $data['promisemoney'],
                ], [
                    'uid' => $user->uid,
                ]);
            }
            return $addPosition;
        }

    }

    public function removePosition(Request $request) //刪除職位需求

    {
        //position\case_uid
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        // $taker = DB::table('user_take_case')->where('case_uid' , $data['case_uid'])->where('isopen' , 1)->where('')->get();
        $positionMoney = DB::table('case_position')->where('uid', $data['position_uid'])->first()->promisemoney;
        $positionNum = DB::table('case_position')->where('uid', $data['position_uid'])->first()->num;
        $positionAcNum = DB::table('case_position')->where('uid', $data['position_uid'])->first()->acnum;
        $allPositionMoney = $positionMoney * $positionNum;
        $userMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        if ($positionAcNum != 0) {
            return [
                'table' => 'case_position',
                'status' => 'false',
                'result' => 'have taker',
            ];
        } else {
            $remove = $this->SupportUpdate('case_position', [
                'isopen' => 0,
            ], [
                'case_uid' => $data['case_uid'],
                'name' => $data['position'],
            ]);
            $this->SupportUpdate('margin', [
                'status' => 'return',
            ], [
                'belong_uid' => $data['case_uid'],
                'position' => $data['position'],
            ]);
            $this->SupportUpdate('users', [
                'money' => $userMoney + $allPositionMoney,
            ], [
                'uid' => $user->uid,
            ]);

            return $remove;
        }

    }

    public function cancelTaker(Request $request) //發案管理\審核人才\等待中 取消按鈕

    {
        // {"take_uid":"TA開頭的uid"}
        $data = $request['take_uid'];
        $cancelTaker = $this->SupportUpdate('user_take_case', [
            'status' => 'refuse',
        ], ['uid' => $data]);
        return $cancelTaker;
    }

    public function copyCase(Request $request)
    {
        // {"data":{"case_uid":"","times":""}}
        $data = $request['data'];
        $case = DB::table('cases')->where('uid', $data['case_uid'])
            ->select('casename', 'category', 'casecontent', 'attraction', 'startdate', 'enddate', 'location', 'releasedate', 'attention', 'connect', 'phone', 'search', 'peoplenum', 'isopen', 'totaldays', 'status')
            ->first();
        $position = DB::table('case_position')->where('case_uid', $data['case_uid'])
            ->select('case_uid', 'name', 'num', 'language', 'ability', 'promisemoney', 'salary', 'notice', 'isopen')
            ->get();
        // return $position;
        for ($i = 0; $i < $data['times']; $i++) {
            $copyCase = $this->SupportInsert('cases', $case);
            $position->map(function ($item, $keys) {
                $this->SupportInsert('case_position', $item);
            });
        }

        // return $copyCase;
        // $copyPosition = $this->SupportInsert('case_position' ,)
    }

    public function alternate(Request $request)
    {
        //{"data":{"case_uid":"","position":""}}
        $data = $request['data'];
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $position = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->where('status', 'ready')->where('isopen', 1)->get();
        $position = $position->map(function ($item, $keys) use($avatar,$data){
            $memberDataTMP = DB::table('members')->where('user_uid', $item->user_uid)->select('address' , 'area' , 'education' , 'email' , 'gender','interest','location','mobilephone','phone','profile','skill','uid','username','user_uid')->first();
            $caseTMP = DB::table('cases')->where('uid' , $data['case_uid'])->first();
            $positionTMP = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
            $item->casename = $caseTMP->casename;
            $item->case_uid = $caseTMP->uid;
            $item->position_uid = $positionTMP->uid;
            $item->position = $positionTMP->name;
            $item->username = $memberDataTMP->username;
            $item->education = $memberDataTMP->education;
            $item->email = $memberDataTMP->email;
            $item->user_uid = $memberDataTMP->user_uid;
            $item->uid = $memberDataTMP->uid;
            $item->gender = $memberDataTMP->gender;
            $item->skill = $memberDataTMP->skill;
            $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
            if ($item->func == 'UserTake') {
                $item->resume = DB::table('resume')->where('uid', $item->resume_uid)->first();
            } else {
                $item->resume = DB::table('resume')->where('belong_member', $item->resume_uid)->first();
            }
            $language = DB::table('user_language_ability')->where('user_uid', $item->user_uid)->where('isopen', 1)->select("language")->get();
            $language = $language->map(function ($item, $keys) {
                $item = $item->language;
                return $item;
            });
            $item->language = $language;
            $item->licence = DB::table('user_license')->where('cmember', $item->user_uid)->select('chocategory' , 'cholicens','uid')->where('isopen', 1)->get();
            return $item;
        });
        return [
            'person' => $position,
        ];
    }

    public function showDefault(Request $request)
    {
        //{"data":{"case_uid":"","position_uid":""}}
        $data = $request['data'];

        if ($data['position_uid'] == null) {
            $TAKEtmp = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->get();
            $takeDefault = $TAKEtmp->where('status', 'takerDefault');
            $takeDefault = $takeDefault->map(function ($item, $keys) {
                $RStmp = DB::table('cancel_reason')->where('take_uid', $item->uid)->first();
                $item->cancelReascon = $RStmp->content;
                $item->status = $RStmp->status;
            });
            $caseDefault = $TAKEtmp->where('status', 'caseDefault');
            $caseDefault = $caseDefault->map(function ($item, $keys) {
                $RStmp = DB::table('cancel_reason')->where('take_uid', $item->uid)->first();
                $item->cancelReascon = $RStmp->content;
                $item->status = $RStmp->status;
            });
            return [
                'takerDefault' => $takeDefault,
                'caseDefault' => $caseDefault,
            ];
        } else {
            $TAKEtmp = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->get();
            $takeDefault = $TAKEtmp->where('status', 'takerDefault');
            $takeDefault = $takeDefault->map(function ($item, $keys) {
                $RStmp = DB::table('cancel_reason')->where('take_uid', $item->uid)->first();
                $item->cancelReascon = $RStmp->content;
                $item->status = $RStmp->status;
            });
            $caseDefault = $TAKEtmp->where('status', 'caseDefault');
            $caseDefault = $caseDefault->map(function ($item, $keys) {
                $RStmp = DB::table('cancel_reason')->where('take_uid', $item->uid)->first();
                $item->cancelReascon = $RStmp->content;
                $item->status = $RStmp->status;
            });
            return [
                'takerDefault' => $takeDefault,
                'caseDefault' => $caseDefault,
            ];
        }
    }

    public function complete(Request $request)
    {
        //{"case_uid":""}
        $user = Auth::guard('api')->user();
        $data = $request['case_uid'];
        $CaseData = DB::table('cases')->where('uid', $data)->first();
        $TakeCase = DB::table('user_take_case')->where('case_uid', $data)->where('isopen', 1)->get();
        $nowday = Carbon::now();
        $totalday = floor((strtotime($nowday) - strtotime($CaseData->enddate)) / 3600 / 24);
        if ($totalday < 7) {
            return ['msg' => 'need 7 days to complete case'];
        }
        if ($TakeCase->whereIn('status', ['discuss', 'accept', 'money', 'confirm'])->isNotEmpty()) {
            return ['msg' => 'have discuss'];
        } else {
            $caseComplete = $this->SupportUpdate('cases', [
                'status' => 'complete',
            ], ['uid' => $data]);
            $TakeCase->where('status', 'confirm');
            $TakeCase = $TakeCase->map(function ($item, $keys) {
                //歸還錢、接案狀態改為complete、margin最後歸還者改為自己
                $PromiseMoney = DB::table('case_position')->where('uid', $item->position_uid)->first()->promisemoney;
                $UserOwnMoney = DB::table('users')->where('uid', $item->cmember)->first()->money;
                //歸還錢
                $this->SupportUpdate('users', [
                    'money' => $UserOwnMoney + $PromiseMoney,
                ], ['uid' => $item->cmember]);
                //接案狀態更改
                $this->SupportUpdate('user_take_case', [
                    'status' => 'complete',
                ], ['uid' => $item->uid]);
                //margin最後歸還者改為自己
                $this->SupportUpdate('margin', [
                    'status' => 'return',
                    'final_return' => $item->cmember,
                    'return_money' => $PromiseMoney,
                ], ['belong_uid' => $item->uid]);
            });

            $AddCasePromiseMoney = DB::table('case_position')->where('case_uid', $data)->where('isopen', 1)->get()->sum('promisemoney');
            $CaseOwnMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
            $margin = DB::table('margin')->where('belong_uid', $data)->where('func', 'addCase')->get();
            //發案者歸還保證金
            $this->SupportUpdate('users', [
                'money' => $CaseOwnMoney + $AddCasePromiseMoney,
            ], ['uid' => $user->uid]);
            //margin最後歸還改為自己
            $margin->map(function ($item, $keys) use ($user) {
                $this->SupportUpdate('margin', [
                    'status' => 'return',
                    'final_return' => $user->uid,
                    'return_money' => $item->money,
                ], ['uid' => $item->uid]);
            });
        }
    }

    public function removeCase(Request $request)
    {
        //{"case_uid":""}
        $data = $request['case_uid'];
        $user = Auth::guard('api')->user();
        $CaseTaker = DB::table('user_take_case')->where('case_uid', $data)->whereIn('status', ['money', 'confirm', 'complete', 'ready', 'discuss', 'accept'])->where('isopen', 1)->get();
        if ($CaseTaker->isNotEmpty()) {
            return ['msg' => 'have taker'];
        } else {
            //case isopen改為0、押金歸還
            $this->SupportUpdate('cases', [
                'isopen' => 0,
            ], ['uid' => $data]);

            $margin = DB::table('margin')->where('belong_uid', $data)->where('func', 'addCase')->get();
            $margin->map(function ($item, $keys) {
                $UserOwnMoney = DB::table('users')->where('uid', $user->uid)->first();
                //保證金歸還
                $this->SupportUpdate('margin', [
                    'status' => 'return',
                    'final_return' => $user->uid,
                    'return_money' => $item->money,
                ], ['uid' => $item->uid]);
                //保證金加回帳戶
                $this->SupportUpdate('users', [
                    'money' => $UserOwnMoney + $item->money,
                ], ['uid' => $user->uid]);
            });
        }
    }

    public function ShowHistoryRecordTaker(Request $request)
    {
        // {"data":{"case_uid":"","position_uid":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $UserTakeCaseTMP = DB::table('user_take_case')->where('case_uid', $data['case_uid'])->get();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        if ($data['position_uid'] == null) {
            $Taker = $UserTakeCaseTMP;
            $Taker = $Taker->map(function ($item, $keys) use ($avatar) {
                $MemberData = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $CPtmp = DB::table('case_position')->where('uid' , $item->position_uid)->first();
                $item->TakerData = $MemberData;
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                $item->position = $CPtmp->name;
                $item->licence = DB::table('user_license')->where('user_uid' , $item->user_uid)->where('isopen' , 1)->get();
                if($item->func == 'UserTake'){
                    $item->resume = DB::table('resume')->where('uid' , $item->resume_uid)->first();
                }else {
                    $item->resume = DB::table('resume')->where('belong_member' , $item->user_uid)->where('isPreset' , 1)->first();
                }
                return $item;
            });
        } else {
            $Taker = $UserTakeCaseTMP->where('position_uid', $data['position_uid']);
            $Taker = $Taker->map(function ($item, $keys) use ($avatar) {
                $MemberData = DB::table('members')->where('user_uid', $item->user_uid)->first();
                $CPtmp = DB::table('case_position')->where('uid' , $item->position_uid)->first();
                $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
                $item->TakerData = $MemberData;
                $item->position = $CPtmp->name;
                $item->licence = DB::table('user_license')->where('user_uid' , $item->user_uid)->where('isopen' , 1)->get();
                return $item;
            });
        }

        return $Taker;
    }
}
