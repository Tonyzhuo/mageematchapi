<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\License;
use Auth;
use Carbon\Carbon;
use App\Http\Controllers\PublicCtr;
// use Symfony\Component\Process\Process;
// use Symfony\Component\Process\Exception\ProcessFailedException;

class LicenseController extends Controller
{
    use PublicCtr\Support;
    public function updateCode(Request $request)
    {
        //{"data":{"name":"","category":"","level":""}}
        // $command2 = escapeshellcmd('/Users/zhuoyijia/crawlers/browserControl.py');
        // $output2 = shell_exec($command2);
        // echo $output2;
        // return ;
        $data = $request['data'];
        $licence = $this->SupportInsert('licence_code' , [
            'name' => $data['name'],
            'category' => $data['category'],
            'level' => $data['level']
            ]);
        return $licence;
    }
}
