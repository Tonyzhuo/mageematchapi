<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PublicCtr;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use PublicCtr\Support;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('auth:api');
    }

    public function login(Request $request)
    {
        $pwd = User::where(["email" => $request['email']])->first();
        $members = DB::table('members')->where('user_uid',$pwd->uid)->first();
        $img = DB::table('img')->where('cmember', $pwd->uid)->where('category', 'avatar')->where('isopen', 1)->get();
        if ($pwd) {
            // dd(Hash::check($request['password'],$pwd->password));
            if (Hash::check(($request['password']), $pwd->password)) { //（輸入的值,比對的值）
                if (User::where(['send_email_check' => '1'])) {
                    if($members->phone == null){
                        $this->SupportInsert('notice', [
                            'belong_table' => 'members',
                            'from_user_uid' => 'system',
                            'to_user_uid' => $pwd->uid,
                            'content' => 'pls fill the phone',
                        ]);
                    }
                    if($pwd->sort == null){
                        $this->SupportInsert('notice', [
                            'belong_table' => 'users',
                            'from_user_uid' => 'system',
                            'to_user_uid' => $pwd->uid,
                            'content' => 'pls choose the side',
                        ]);
                    }
                    if ($img->isEmpty()) {
                        $notice = $this->SupportInsert('notice', [
                            'belong_table' => 'img',
                            'from_user_uid' => 'system',
                            'to_user_uid' => $pwd->uid,
                            'content' => 'pls add pic',
                        ]);
                    }
                    return [
                        "state" => "success",
                        'api_token' => $pwd->api_token,
                        'uid' => $pwd->uid,
                        'username' => $pwd->username,
                    ];
                } else {
                    return [
                        "state" => "error",
                        "msg" => "email no open",
                    ];
                };
            } else {
                return [
                    "state" => "error",
                    'msg' => 'password mistake',
                ];
            }
        } else {
            return [
                'stste' => 'error',
                'msg' => 'account mistake',
            ];
        };
    }

}
