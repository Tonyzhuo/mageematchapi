<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PublicCtr;
use App\Mail\VerifyMail;
use App\member;
use App\User;
use App\verification;
use Auth;
use DB;
use Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use Response;
use Carbon\Carbon;

class RegisterController extends Controller
{
    use PublicCtr\Support;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:255|unique:users',
    //         'password' => 'required|string|min:6|confirmed',
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    

    public function testRegister(Request $request)
    {
        //{"data":{"username":"","email":"","password":"","gender":""}}
        $data = $request['data'];
        $user = $this->SupportInsert('users', [
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => Hash::make($data['email'] . $data['email'] . (str_random(50) . time()) . str_random()),
            'sort' => 'person',
            'isopen' => 1,
            'money' => 5000,
        ]);
        $member = $this->SupportInsert('members', [
            'user_uid' => $user['result'][0]->uid,
            'email' => $data['email'],
            'username' => $data['username'],
            'gender' => $data['gender'],
            'education' => '亞東技術學院',
            'isopen' => 1,
            'isMember' => 1,
        ]);
        $resume = $this->SupportInsert('resume', [
            'user_uid' => $user['result'][0]->uid,
            'resumename' => '我要應徵',
            'education' => '亞東技術學院',
            'profile' => '個性活潑外向,是個好學生',
            'servercontent' => '當個乖學生',
            'resumejob' => '學生',
        ]);
        return [
            'user' => $user,
            'member' => $member,
        ];
    }

    protected function member(Request $request) //會員註冊

    {
        // DB::beginTransaction();
        // $data = $request['data'];
        // $CheckEmailDouble = DB::table('users')->where('email', $data['email'])->first();
        // $code = range(0,9);
        // shuffle($code);
        // $result = array_slice($code,0,6);
        // $result = collect($result)->implode('');
        // // return $result;
        // if (!$CheckEmailDouble) {
        //     $user = User::create([
        //         'username' => $data['username'],
        //         'email' => $data['email'],
        //         'password' => hash::make($data['password']),
        //         // 'api_token' => Hash::make($data['email'] . $data['email'] . (str_random(50) . time()) . str_random()),
        //         'send_email_check' => 0,
        //         'codenum' => $result
        //     ]);
        //     Mail::to($user->email)->send(new VerifyMail($user));
        //     DB::commit();
        //     return [
        //         'table' => 'users',
        //         'status' => 'true',
        //         'result' => $user,
        //     ];
        // }else{
        //     return [
        //         'table' => 'users',
        //         'status' => 'true',
        //         'result' => 'email is double',
        //     ];
        // }
        try {
            $data = $request['data'];
            $user = $this->createMember($data);
            return [
                'table' => 'user',
                'state' => 'true',
                'result' => $user,
            ];
        } catch (\Exception $e) {
            // DB::rollback();
            // dd($e);
            return response()->json([
                'msg' => $e->getMessage(),
                "state" => "no create account",
            ]);

        }
    }

    protected function createMember(array $data)
    {
        $repeat = DB::table('users')->where('email', $data['email'])->first();
        if (!$repeat) {
            $userdata = $this->SupportInsert("users", [
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                // 'api_token' => Hash::make($data['email'].$data['password'].(str_random(50) . time()). str_random()),
                'codenum' => '0000',
                'register_token' => '',
            ]);
        } else {
            return [
                'table' => 'users',
                'state' => 'false',
                'result' => 'email repeat',
            ];
        }

        $memberdata = $this->SupportInsert("members", [
            'user_uid' => $userdata['result'][0]->uid,
            'username' => $data['username'],
            'email' => $data['email'],
        ]);

        // return $userdata;

        if ($userdata['state']) {
            $userresult = $userdata;
        }
        if ($memberdata['state']) {
            $memresult = $memberdata;
        }
        return ['user' => $userresult, 'member' => $memresult];
    }

    public function RegisterMember(Request $request)
    {
        
        DB::beginTransaction();
        $data = $request['data'];
        $CheckEmailDouble = DB::table('users')->where('email', $data['email'])->first();
        $code = range(0,9);
        shuffle($code);
        $result = array_slice($code,0,6);
        $result = collect($result)->implode('');
        // return $result;
        if (!$CheckEmailDouble) {
            $user = User::create([
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => hash::make($data['password']),
                // 'api_token' => Hash::make($data['email'] . $data['email'] . (str_random(50) . time()) . str_random()),
                'send_email_check' => 0,
                'codenum' => $result
            ]);
            Mail::to($user->email)->send(new VerifyMail($user));
            DB::commit();
            return [
                'table' => 'users',
                'status' => 'true',
                'result' => $user,
            ];
        }else{
            return [
                'table' => 'users',
                'status' => 'true',
                'result' => 'email is double',
            ];
        }

    }

    public function verification(Request $request) //認證驗證碼

    {
        $info = $request['code'];
        $user = DB::table('users')->where('email' , $info['email'])->first();
        $nowTime = Carbon::now();
        
        // return [strtotime($user->createtime) , strtotime($nowTime) , (strtotime($nowTime) - strtotime($user->createtime))/60];
        if((strtotime($nowTime) - strtotime($user->createtime)) > 60){
            return ['msg' => 'time is up'];
        }
        if ($info['checknumber'] == $user->codenum) {
            // $userapi = $this->SupportUpdate('users' ,[
            //     'api_token' => Hash::make($info['email'].$info['email'].(str_random(50) . time()). str_random()),
            // ],['email'=>$info['email'],"codenum"=>$info['checknumber']]);

            $userapi = DB::table('users')
                ->where('email', $info['email'])
            // ->where('codenum' , $$info['checknumber'])
                ->update([
                    'api_token' => Hash::make($info['email'] . $info['email'] . (str_random(50) . time()) . str_random()),
                    'money' => 5000,
                ]);

        } else {
            return [
                'table' => 'users',
                'state' => 'fale',
                'result' => 'code mistake or time is up',
            ];
        }
        $userinfo = DB::table('users')->where('email', $info['email'])->get();

        return [
            'table' => 'users',
            'state' => 'true',
            'result' => $userinfo,
        ];
    }

    public function testRegisterMember(Request $request)
    {
        //{"data":{"times":"","pwd":""}}
        $data = $request['data'];
        for ($i=3; $i < $data['times']; $i++) { 
            $user = $this->SupportInsert('users', [
                'username' => 'test'.$i,
                'email' => 'test'.$i.'@magee.com',
                'password' => Hash::make($data['pwd']),
                'api_token' => Hash::make($data['pwd'] . $data['pwd'] . (str_random(50) . time()) . str_random()),
                'sort' => 'person',
                'isopen' => 1,
                'money' => 5000,
            ]);
            $member = $this->SupportInsert('members', [
                'user_uid' => $user['result'][0]->uid,
                'email' => 'test'.$i.'@magee.com',
                'username' => 'test'.$i,
                'gender' => 'male',
                'education' => '亞東技術學院',
                'isopen' => 1,
                'isMember' => 1,
            ]);
            $resume = $this->SupportInsert('resume', [
                'user_uid' => $user['result'][0]->uid,
                'resumename' => '第一份履歷',
                'education' => '亞東技術學院',
                'profile' => '個性活潑外向,是個好學生',
                'servercontent' => '當個乖學生',
                'resumejob' => '學生',
            ]);
        }
        
        
    }

}
