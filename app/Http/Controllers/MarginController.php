<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PublicCtr;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class MarginController extends Controller
{
    use PublicCtr\Support;

    public function CancelDefault(Request $request)//取消違約
    //{"data":{"default_uid":"CR開頭","take_uid":""}
    {
        $data = $request['default_uid'];
        $user = Auth::guard('api')->user();
        $CancelDefault = $this->SupportUpdate('cancel_reason',[
            'isopen' => 0,
            'status' => 'cancel'
        ],['uid' => $data]);
        $takeCaseStatus = $this->SupportUpdate('',[
            'status' => 'confirm'
        ],['uid' => $data['take_uid']]);

        return [
            'CancelDefault' => $CancelDefault,
            'takeCaseStatus' => $takeCaseStatus
        ];
    }

    public function ShowCancelReason(Request $request)
    {
        //{"":""}
        $user = Auth::guard('api')->user();
        $avatar = "http://localhost/Mageematch-api/storage/pics/avatar.jpg";
        $takeInfo =DB::table('user_take_case')->where('case_user_uid' , $user->uid)->where('status' , 'discuss')->get();
        $takeInfo = $takeInfo->map(function($item,$keys)use($avatar){
            $caseTMP = DB::table('cases')->where('uid' , $item->case_uid)->first();
            $userTMP = DB::table('users')->where('uid' , $item->belong_member)->first();
            $positionTMP = DB::table('case_position')->where('uid' , $item->position_uid)->first();
            $item->cancelReason = DB::table('cancel_reason')->where('take_uid' , $item->uid)->where('isopen' , 1)->first();
            $item->casename = $caseTMP->casename;
            $item->case_uid = $caseTMP->uid;
            $item->ProposeDefaultName = $userTMP->username;
            $item->position= $positionTMP->name;
            $item->img = (DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first() != null) ? DB::table('img')->where('belong_member', $item->user_uid)->where('category', 'avatar')->where('isopen', 1)->first()->path : $avatar;
            if($item->func == 'UserTake'){
                $item->resume = DB::table('resume')->where('uid' , $item->resume_uid)->first();
            }else {
                $item->resume = DB::table('resume')->where('cmember' , $item->user_uid)->where('isPreset' , 1)->first();
            }
            return $item;
        });

        return [
            'table' => 'cancel_reason',
            'status' => 'true',
            'result' => $takeInfo
        ];
        
    }

    public function caseDefault(Request $request) //發案方違約

    {
        //{"data":{"user_uid":"被違約者","case_uid":"被違約者的case","position":"被違約者的職位","take_uid":"","stillneed":""}}
        //24小時內違約不用扣住保證金
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $today = Carbon::now();
        $CPtmp = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
        // $TakeCaseUid = DB::table('user_take_case')->where('user_uid', $data['user_uid'])->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->first()->uid;
        // $TakeCaseUid = DB::table('user_take_case')->where('uid' , $data['take_uid'])->first()->uid;
        $casePositionData = DB::table('case_position')->where('uid', $data['position_uid'])->first();
        //發案方違約金
        $casePromiseMoney = $CPtmp->promisemoney;
        //接案方違約金
        $takerPromiseMoney = $CPtmp->promisemoney;
//-------
        //發案方的錢
        $caseOwnMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        //接案方的錢
        $takerOwnMoney = DB::table('users')->where('uid', $data['user_uid'])->first()->money;
//-----
        $EditCasePosition = $this->SupportUpdate('case_position', [
            'acnum' => $CPtmp->acnum - 1,
            'num' => $CPtmp->num - 1,
        ], ['uid' => $data['position_uid']]);

        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'cases',
            'belong_uid' => $data['case_uid'],
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'default promisemoney',
        ]);
        $takerSideMoney = $this->SupportUpdate('users', [
            'money' => $takerOwnMoney + $casePromiseMoney + $takerPromiseMoney,
        ], [
            'uid' => $data['user_uid'],
        ]);
        //接案狀態更改為違約
        $takeCaseStatus = $this->SupportUpdate('user_take_case', [
            'status' => 'caseDefault',
        ], ['uid' => $data['take_uid']]);
        //接案方付錢狀態更改為歸還
        $marginStatus = $this->SupportUpdate('margin', [
            'status' => 'return',
            'final_return' => $data['user_uid'],
        ], [
            'belong_uid' => $data['take_uid'],
        ]);
        //發案方最後歸還者改為自己
        $marginFinalReturn = $this->SupportUpdate('margin', [
            'status' => 'return',
            'final_return' => $data['user_uid'],
        ], [
            'belong_uid' => $data['case_uid'],
            'user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'position_uid' => $data['position_uid'],
            'status' => 'wait'
        ]);
        if($data['stillneed'] == 1){
            //職位+1
            $CPtmp = DB::table('case_position')->where('uid' , $data['position_uid'])->first();
            $this->SupportUpdate('case_position',[
                'num' => $CPtmp->num +1
            ],['uid' => $data['position_uid']]);
            //扣帳戶錢
            $caseOwnMoney = DB::table('users')->where('uid' , $user->uid)->first()->money;
            $this->SupportUpdate('users',[
                'money' => $caseOwnMoney - $CPtmp->promisemoney
            ],['uid' => $user->uid]);
            //margin新增付錢紀錄
            $this->SupportInsert('margin',[
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'user_uid' => $user->uid,
                'money' => $CPtmp->promisemoney,
                'status' => 'wait',
                'func' => 'addCase',
                'position_uid' => $data['position_uid'],
                'position' => $CPtmp->name
            ]);
        }
        // $DefaultReason = $this->SupportInsert('cancel_reason',[
        //     'take_uid' => $data['take_uid'],
        //     'content' => $data['content'],
        //     'status' => 'default',
        // ]);

        return [
            'takerSideMoney' => $takerSideMoney,
            'takeCaseStatus' => $takeCaseStatus,
            'marginStatus' => $marginStatus,
            'marginFinalReturn' => $marginFinalReturn,
            'EditCasePosition' => $EditCasePosition,
        ];

    }
    public function takerDefault(Request $request)
    {
        //{"data":{"user_uid":"被違約者","case_uid":"被違約者的case","position_uid":"被違約者的職位","take_uid":"","content":""}}
        //24小時內違約不用扣住保證金
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $today = Carbon::now();
        $TakeCaseUid = DB::table('user_take_case')->where('user_uid', $user->uid)->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->first()->uid;
        $casePositionData = DB::table('case_position')->where('uid', $data['position_uid'])->first();
        //發案方違約金
        $casePromiseMoney = DB::table('margin')->where('user_uid', $data['user_uid'])->where('belong_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->first()->money;
        //接案方違約金
        $takerPromiseMoney = DB::table('margin')->where('user_uid', $user->uid)->where('belong_uid', $TakeCaseUid)->first()->money;
//-------
        //發案方的錢
        $caseOwnMoney = DB::table('users')->where('uid', $data['user_uid'])->first()->money;
        //接案方的錢
        $takerOwnMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
//-----
        $EditCasePosition = $this->SupportUpdate('case_position', [
            'acnum' => $casePositionData->acnum - 1,
            'num' => $casePositionData->num - 1,
        ], ['uid' => $casePositionData->uid]);

        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'cases',
            'belong_uid' => $data['case_uid'],
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'default promisemoney',
        ]);
        $takerSideMoney = $this->SupportUpdate('users', [
            'money' => $caseOwnMoney + $casePromiseMoney + $takerPromiseMoney,
        ], [
            'uid' => $data['user_uid'],
        ]);
        //接案狀態更改為違約
        $takeCaseStatus = $this->SupportUpdate('user_take_case', [
            'status' => 'takerDefault',
        ], ['uid' => $data['take_uid']]);
        //接案方付錢狀態更改為歸還
        $marginStatus = $this->SupportUpdate('margin', [
            'status' => 'return',
            'final_return' => $data['user_uid'],
        ], [
            'belong_uid' => $TakeCaseUid,
        ]);
        //發案方最後歸還者改為自己
        $marginFinalReturn = $this->SupportUpdate('margin', [
            'status' => 'return',
            'final_return' => $data['user_uid'],
        ], [
            'belong_uid' => $data['case_uid'],
            'user_uid' => $data['user_uid'],
            'to_user_uid' => $user->uid,
            'position_uid' => $data['position_uid'],
        ]);
        $DefaultReason = $this->SupportInsert('cancel_reason',[
            'take_uid' => $data['take_uid'],
            'content' => $data['content'],
            'status' => 'default',
        ]);

        return [
            'takerSideMoney' => $takerSideMoney,
            'takeCaseStatus' => $takeCaseStatus,
            'marginStatus' => $marginStatus,
            'marginFinalReturn' => $marginFinalReturn,
            'EditCasePosition' => $EditCasePosition,
        ];
    }

    public function takerObjection(Request $request) //接案提出方異議

    {
        //{"data":{"user_uid":"被違約者","case_uid":"被違約者的case","position_uid":"被違約者的職位","take_uid":"","content":""}}
        // return 'aa';
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $takeCaseUid = DB::table('user_take_case')->where('user_uid', $user->uid)->where('case_uid', $data['case_uid'])->where('position_uid', $data['position_uid'])->first();
        $cancelReason = $this->SupportInsert('cancel_reason', [
            'take_uid' => $takeCaseUid->uid,
            'content' => $data['content'],
            'status' => 'wait',
        ]);
        
        $take = $this->SupportUpdate('user_take_case',[
            'status' => 'discuss'
        ],['uid' => $takeCaseUid->uid]);
        
        $notice = $this->SupportInsert('notice', [
            'belong_table' => 'cases',
            'belong_uid' => $data['case_uid'],
            'from_user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'content' => 'taker default objection',
        ]);
        return $cancelReason;
        // DB::rollback();
    }

    public function caseAcceptDefault(Request $request) //發案方接受或拒絕接案方的違約

    {
        //{"data":{"user_uid":"","case_uid":"","position_uid":"","answer":"","reason_uid":"","take_uid":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $today = Carbon::now();
        $TakeCaseTMP = DB::table('user_take_case')->where('uid' , $data['take_uid'])->first();
        $casePositionData = DB::table('case_position')->where('uid', $data['position_uid'])->first();
        //發案方違約金
        $casePromiseMoney = $casePositionData->promisemoney;
        //接案方違約金
        $takerPromiseMoney =  $casePositionData->promisemoney;
//------------------
        //發案方的錢
        $caseOwnMoney = DB::table('users')->where('uid', $user->uid)->first()->money;
        //接案方的錢
        $takerOwnMoney = DB::table('users')->where('uid', $data['user_uid'])->first()->money;

        if ($data['answer'] == 'accept') {

            $this->SupportUpdate('case_position', [
                'acnum' => $casePositionData->acnum - 1,
                'num' => $casePositionData->num - 1,
            ], ['uid' => $casePositionData->uid]);

            $notice = $this->SupportInsert('notice', [ //通知接受違約
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'content' => 'default accept',
            ]);

            $reason = $this->SupportUpdate('cancel_reason', [ //違約狀態改接受
                'status' => 'accpet',
            ], ['uid' => $data['reason_uid']]);
            $casenotice = $this->SupportInsert('notice', [
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $data['user_uid'],
                'to_user_uid' => $user->uid,
                'content' => 'promisemoney return',
            ]);
            $takenotice = $this->SupportInsert('notice', [
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'content' => 'promisemoney return',
            ]);
            //雙方保證金退給被違約者
            $caseSideMoney = $this->SupportUpdate('users', [
                'money' => $caseOwnMoney + $casePromiseMoney,
            ], [
                'uid' => $user->uid,
            ]);
            $takeSideMoney = $this->SupportUpdate('users', [
                'money' => $takerOwnMoney + $takerPromiseMoney,
            ], ['uid' => $data['user_uid']]);
            //接案狀態更改為違約
            $takeCaseStatus = $this->SupportUpdate('user_take_case', [
                'status' => 'takerDefault',
            ], ['uid' => $data['take_uid']]);
            //接案方付錢狀態更改為歸還
            $marginStatus = $this->SupportUpdate('margin', [
                'status' => 'return',
                'final_return' => $data['user_uid'],
            ], [
                'belong_uid' => $data['take_uid'],
            ]);
            //發案方最後歸還者改為自己
            $marginFinalReturn = $this->SupportUpdate('margin', [
                'status' => 'return',
                'final_return' => $user->uid,
            ], [
                'belong_uid' => $data['case_uid'],
                'user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'position_uid' => $data['position_uid'],
                'status' => 'wait'
            ]);
            
            return [
                $caseSideMoney,
                $takeCaseStatus,
                $marginStatus,
                $marginFinalReturn,
            ];
        } else {
            $this->SupportUpdate('case_position', [
                'acnum' => $casePositionData->acnum - 1,
                'num' => $casePositionData->num - 1,
            ], ['uid' => $casePositionData->uid]);

            $notice = $this->SupportInsert('notice', [ //通知拒絕違約
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'content' => 'default reject',
            ]);

            $reason = $this->SupportUpdate('cancel_reason', [ //違約狀態改違約
                'status' => 'default',
            ], ['uid' => $data['reason_uid']]);

            $casenotice = $this->SupportInsert('notice', [
                'belong_table' => 'cases',
                'belong_uid' => $data['case_uid'],
                'from_user_uid' => $data['user_uid'],
                'to_user_uid' => $user->uid,
                'content' => 'promisemoney return',
            ]);
            //雙方保證金退給被違約者
            $caseSideMoney = $this->SupportUpdate('users', [
                'money' => $caseOwnMoney + $casePromiseMoney + $takerPromiseMoney,
            ], [
                'uid' => $user->uid,
            ]);
            //接案狀態更改為違約
            $takeCaseStatus = $this->SupportUpdate('user_take_case', [
                'status' => 'takerDefault',
            ], ['uid' => $data['take_uid']]);
            //接案方付錢狀態更改為歸還
            $marginStatus = $this->SupportUpdate('margin', [
                'status' => 'return',
                'final_return' => $user->uid,
            ], [
                'belong_uid' => $data['take_uid'],
            ]);
            //發案方最後歸還者改為自己
            $marginFinalReturn = $this->SupportUpdate('margin', [
                'status' => 'return',
                'final_return' => $user->uid,
            ], [
                'belong_uid' => $data['case_uid'],
                'user_uid' => $user->uid,
                'to_user_uid' => $data['user_uid'],
                'position_uid' => $data['position_uid'],
                'status' => 'wait'
            ]);
            
            return [
                $caseSideMoney,
                $takeCaseStatus,
                $marginStatus,
                $marginFinalReturn,
            ];
        }

    }

    public function caseObjection(Request $request) //發案方有異議

    {
        //{"data":{"user_uid":"","case_uid":"","position":""}}
        $data = $request['data'];
        $user = Auth::guard('api')->user();
        $TakeCaseUid = DB::table('user_take_case')->where('user_uid', $data['user_uid'])->where('case_uid', $data['case_uid'])->where('position', $data['position'])->first()->uid;
        $caseMargin = $this->SupportUpdate('margin', [
            'status' => 'check',
        ], [
            'belong_talbe' => 'case',
            'belong_uid' => $data['case_uid'],
            'user_uid' => $user->uid,
            'to_user_uid' => $data['user_uid'],
            'position' => $data['position'],
        ]);
        $takerMargin = $this->SupportUpdate('margin', [
            'status' => 'check',
        ], [
            'belong_uid' => $TakeCaseUid,
        ]);
        // $takeCaseStatus = $this->SupportUpdate('user_take_case',[
        //     'status' => 'check'
        // ],[
        //     'uid' => $TakeCaseUid
        // ]);

        return [
            $caseMargin,
            $takerMargin,
            'msg' => 'Pls wait for Artificial check',
        ];
    }
    

    public function tenday()
    {
        $user = Auth::guard("api")->user();

        $userday = DB::table('margin')->where('user_uid', $user->uid)->where('status', 'complete')->get();
        $money = DB::table('users')->where('uid', $user->uid)->first()->money;
        $case = DB::table('cases')->where('belong_member', $user->uid)->where('status' , 'wait')->where('isopen', 1)->where('isIssue', 1)->get();
        $deadLine = DB::table('user_take_case')->where('status', 'money')->get();
        $defaultObjection = DB::table('cancel_reason')->where('cmember' , $user->uid)->where('isopen' , 1)->get();

        $nowday = Carbon::now();

        $back = $userday->map(function ($item, $keys) use ($nowday, $user) {
            $totalday = floor((strtotime($nowday) - strtotime($item->updatetime)) / 3600 / 24);
            if ($totalday >= 7) {
                $margin = $this->SupportUpdate('margin', [
                    'isreturn' => 1,
                    'status' => 'return',
                    'return_money' => $item->money,
                    'final_return' => $item->to_user_uid,
                ], [
                    'belong_table' => 'user_take_case',
                    'belong_uid' => $item->belong_uid,
                    'user_uid' => $user->uid,
                ]);
                $caseuid = DB::table('user_take_case')->where('case_uid', $item->belong_uid)->first();
                $casename = DB::table('cases')->where('uid', $caseuid)->first()->casename;
                $notice = $this->SupportInsert('notice', [
                    'belong_table' => 'cases',
                    'belong_uid' => $data['case_uid'],
                    'from_user_uid' => 'system',
                    'to_user_uid' => $user->uid,
                    'content' => 'promisemoney return',
                ]); //通知

                $userMoney = $this->SupportUpdate('users', [
                    'money' => $money + $item->money,
                ], [
                    'uid' => $user->uid,
                ]); //錢歸還
            }
        });

        $carryOut = $case->map(function ($item, $keys) use ($nowday) {
            if ($nowday >= $item->releasedate && $item->releasedate != null) {
                $this->SupportUpdate('cases', [
                    'status' => 'carryOut',
                ], ['uid' => $item->uid]);
            }
        });

        $deadLine = $deadLine->map(function ($item, $keys) use ($nowday) {
            $oneDay = floor((strtotime($nowday) - strtotime($item->updatetime)) / 3600 / 24);
            $positionData = DB::table('case_position')->where('uid' , $item->position_uid)->first();
            if ($oneDay >= 1) {
                $this->SupportUpdate('user_take_case', [
                    'status' => 'ready'
                ], ['uid' => $item->uid]);
                $this->SupportUpdate('case_position', [
                    'acnum' => $positionData->acnum - 1,
                ], ['uid' => $positionData->uid]);
            }
        });

        // $defaultObjection = $defaultObjection->map(function(){
        //     $totalday = floor((strtotime($nowday) - strtotime($item->createtime)) / 3600 / 24);
        //     if($totalday >= 1){
                
        //     }
        // });

    }

}
