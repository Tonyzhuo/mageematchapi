<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PublicCtr;
use App\resume;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    use PublicCtr\Support;
    public function addResume(Request $request) //新增履歷

    {
        $user = Auth::guard('api')->user();
        $dataresume = $request['data'];
        // return $dataresume['resumename'];
        $resumeName = DB::table('resume')
            ->where('resumename', $dataresume['resumename'])
            ->where('cmember', $user->uid)
            ->where('isopen' ,1)
            ->first();
        // return [$resumeName];
        if ($resumeName == null) {
            $resume = $this->SupportInsert('resume', $dataresume);
        } else {
            return [
                'table' => 'resume',
                'state' => 'false',
                'result' => 'resumeName is double',
            ];
        }

        return $resume;
    }

    public function updateresume(Request $request) //修改履歷

    {
        $newresume = $request['resume'];
        $select = $newresume['uid'];
        // return ['resume' => $newresume, 'select' => $select];
        if (Auth::check()) {
            $resume = $this->SupportUpdate('resume', $newresume, ['uid' => $select]);
        }
        return $resume;
    }

    public function showresume() //顯示所有履歷

    {
        $user = Auth::guard("api")->user();
        // $resumeinfo = resume::where("belong_member", $user->uid)
        // ->orwhere('user_uid' , $user->uid)
        // ->where('isopen', 1)
        // ->get();
        // return $user;
        $resumeinfo = DB::table('resume')->where(function($query)use($user){
            $query->where('user_uid' , $user->uid)
            ->orwhere("belong_member", $user->uid);
        })->where('isopen' ,1)->get();
        return [
            'table' => 'resume',
            'state' => 'true',
            'result' => $resumeinfo,
        ];
    }

    public function deleteresume(Request $request) //刪除履歷

    {
        $user = Auth::guard('api')->user();
        // return $request->all();
        // return DB::table('resume')->where('cmember' , $user->uid)->where('isopen' , 1)->get();
        $select = $request['name'];
        foreach ($select as $values) {
            $resume = $this->SupportUpdate('resume', [
                'isopen' => 0,
            ], ['uid' => $values]);
        };

        // $resumeinfo = resume::where("belong_member", $user->uid)->where('isopen', 1)->get();

        return [
            'table' => 'resume',
            'stste' => 'true',
            'result' => $resume,
        ];
    }

    public function PresetResume(Request $request)
    {
        //{"resume_uid":""}
        // return $request->all();
        $user = Auth::guard('api')->user();
        $data = $request['resume_uid'];
        $allResume = DB::table('resume')->where('belong_member' , $user->uid)->where('isPreset' , 1)->get();
        // $allResume = DB::table('resume')->where('belong_member' , $user)
        // return [$isPreset];
        if($allResume->isEmpty())
        {
            $PResume = $this->SupportUpdate('resume', [
                'isPreset' => 1,
            ], [
                'uid' => $data,
            ]);
            return $PResume;
        }else
        {
            $allResume = $allResume->map(function($item,$keys)use($user){
                $OtherResume = $this->SupportUpdate('resume', [
                    'isPreset' => 0,
                ], [
                    'uid' => $item->uid,
                ]);
                return $OtherResume;
            });
            
            $PResume = $this->SupportUpdate('resume', [
                'isPreset' => 1,
            ], [
                'uid' => $data,
            ]);
            return [
                $allResume,
                $PResume,
            ];
        }
    }

}
