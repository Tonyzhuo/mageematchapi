<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PublicCtr;
use Illuminate\Http\Request;
use DB;
class testController extends Controller
{
    use PublicCtr\Support;
    // public function test(Request $request)
    // {
    //     // return $request->all();
    //     $data = $request['data']['name'];
    //     // return $data;
    //     collect($data)->map(function($item,$keys) use($data){
    //         $credit = $this->SupportInsert('credit_card_code', [
    //             'name' => $item,
    //         ]);
    //         return $credit;
    //     });

    // }
    public function test(Request $request){
        $data = $request['data'];
        $BeforeUser = DB::table('users')->where('uid' , $data)->first();
        $AfterUser = DB::table('users')->where('uid' , $data)->update(['money' => 50000]);
        return [$BeforeUser , $AfterUser];
    }

    public function testNotice(Request $request)
    {
        $Members = DB::table('members')->where('isMember' , 1)->where('isopen' , 1)->get();
        for ($i=0; $i < 10; $i++) { 
            for ($j=0; $j < 20; $j++) { 
                $Members->map(function($item,$keys)use($i,$j){
                    $this->SupportInsert('notice',[
                        'belong_table' => 'test',
                        'belong_uid' => 'test',
                        'from_user_uid' => 'system',
                        'to_user_uid' => $item->user_uid,
                        'content' => 'test'.$i.'-'.$j,
                        'isRead' => 1
                    ]);
                });
            }
        }
    }

}
