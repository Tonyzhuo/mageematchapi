<?php

namespace App\Http\Middleware;

use App\Http\Controllers\MarginController;
use Auth;
use Closure;

class CheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $user = Auth::guard($guards[0])->check();
        // dd($user);
        if ($user) {
            $matgin = new MarginController;
            $matgin->tenday();
            // dd("D");
            return $next($request);
        } else {
            return new AuthenticationException('Unauthenticated.', $guards);
        }
        // dd($guards);

    }
}
