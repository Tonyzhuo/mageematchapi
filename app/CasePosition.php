<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasePosition extends Model
{
    protected $table = 'case_position';

    protected $fillable=[
        'id' , 'uid' , 'case_uid' , 'name' , 'num' , 'language' , 'ability' , 'promisemoney' ,
        'salary' , 'belong_member' , 'cmember' , 'umember' , 'createtime' , 'updatetime' , 'isopen'
    ];
    public $timestamps = false;

    public function TheCase()
    {
        return $this->belongsTo('App\TheCase' , 'case_uid' , 'uid');
    }

    
}
