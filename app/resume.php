<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class resume extends Model
{
    protected $table = 'resume';

    protected $fillable=[
        'uid' ,'belong_member' ,'name' , 'education' , 'mobilephone' , 'profile' , 'servercontent' , 'job' ,
        'licensename' ,'interest' , 'createtime' , 'updatetime' ,'umember' , 'cmember'
    ];
    public $timestamps = false;

    public function resume()
    {
        return $this->belongsTo(User::class);
    }
}
